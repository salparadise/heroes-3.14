#include "Headers/MusicPlayer.hpp"


MusicPlayer::MusicPlayer() : _music() , _themes() , _volume(100.f)
{

	
	_themes[Menu] = "Resources/sounds/menu.wav";
	_themes[Map] = "Resources/sounds/map.ogg";
	_sounds["moving"] = std::shared_ptr<sf::Music> (new sf::Music());
    _sounds["moving"]->openFromFile("Resources/sounds/moving.wav");


	_sounds["attack"] = std::shared_ptr<sf::Music> (new sf::Music());
    _sounds["attack"]->openFromFile("Resources/sounds/attack.wav");
	
	
	
}
/**
*	Odtwarzacz muzyki. play(nazwa) - gra, stop- zatrzymuje
*
*/

void MusicPlayer::play(MusicPlayer::ID theme)
{
	
	_music.stop();
	std::string filename = _themes[theme];
	if (!_music.openFromFile(filename))
		return;
	_music.setVolume(_volume);
	_music.setLoop(true);
	_music.play();
}

void MusicPlayer::playSound(std::string sound)
{
	_sounds[sound]->setLoop(false);
	_sounds[sound]->setVolume(_volume);
	_sounds[sound]->play();
	
}



void MusicPlayer::stop()
{
	_music.stop();
}

void MusicPlayer::setPaused(bool paused)
{
	if (paused)
		_music.pause();
	else
		_music.play();
}
