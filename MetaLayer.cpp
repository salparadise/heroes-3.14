/*
 * MetaLayer.cpp
 *
 *  Created on: 25 lis 2013
 *      Author: hawker
 */
#include "Headers/MetaLayer.hpp"

MetaLayer::MetaLayer(std::shared_ptr<Game> g):Layer(g),rect(sf::IntRect(0, 0, 255, 210)) {
	_isActive = false;

	//_sfgui = std::shared_ptr<sfg::SFGUI>(&(_game->getConfig().sfgui));
}

bool MetaLayer::processEvent(sf::Event& e){

	if(_game->getGameState()==MetaGame::META){
						if(e.type == sf::Event::KeyPressed){
						switch(e.key.code){
							case sf::Keyboard::Key::S :
						//	save();
							break;
							case sf::Keyboard::Key::L :
						//	load();
							break;
							case sf::Keyboard::Key::H :
							//	help();
							break;
							case sf::Keyboard::Key::Escape:
								clickedOk();

								break;
							default:clickedMenu(); break;
							}
						return false;
						}
						else {
							_desktop.HandleEvent(e);
							return false;
						}
					}

	else if( e.type == sf::Event::KeyPressed && e.key.code==sf::Keyboard::Key::Escape) {

						_game->setGameState(MetaGame::META);
						return false;
			}
	return true;


}

void MetaLayer::update(sf::Time dt){
	if(_gui != nullptr) {			_desktop.Update(0.f);
		}

		if( _gui == nullptr) {
			createMessage();
		}
}

void MetaLayer::draw(sf::RenderTarget& window, sf::RenderStates states) const{

	_wWidth=window.getSize().x;
		_wHeight=window.getSize().y;
	if(_gui != nullptr) {
		window.draw(*_gui);

		}

}

void MetaLayer::createMessage() {
	_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND);
	auto out = sfg::Button::Create("Powrot do gry");
	auto menu = sfg::Button::Create("Main Menu");
	out->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaLayer::clickedOk, this));
	menu->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaLayer::clickedMenu, this));

	auto box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );
	box->Pack(out, false);
	box->Pack(menu, false);

	_gui->Add(box);
	_gui->SetPosition(
			sf::Vector2f(
				_wWidth/2	 - _gui->GetAllocation().width / 2.f,
				_wHeight/2 - _gui->GetAllocation().height / 2.f
		)

			);
	_desktop.Add(_gui);
}

void MetaLayer::clickedOk() {
	_desktop.RemoveAll();
	_gui = nullptr;
	_game->setGameState(MetaGame::GAME);
									SetIsActive(false);
}

void MetaLayer::clickedMenu(){
	_gui = nullptr;
	_desktop.RemoveAll();
	_game->setGameState(MetaGame::GAME);
	SetIsActive(false);
	_game->end();
}
