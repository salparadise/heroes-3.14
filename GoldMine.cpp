#include "Headers/GoldMine.hpp"

GoldMine::GoldMine() {
	_textureId = 14;
	_income = 100;
}

GoldMine::~GoldMine() {

}

void GoldMine::updateResources() {
	if(_owner != nullptr) {
		_owner->updateGold(_income);
	}
}
