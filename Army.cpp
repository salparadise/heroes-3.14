/**
 * @file Army.cpp
 *
 * @date 10.11.2013
 * @version 1.0
 *
 */

#include "Headers/Army.hpp"
#include "Headers/Player.hpp"
#include "Headers/Field.hpp"
#include "Headers/Building.hpp"
#include "Headers/Town.hpp"
#include "Headers/PathFinding.hpp"

Army::Army(std::shared_ptr<Player> player, std::shared_ptr<Field> currentField, int hoursOfSleep) : _lineOfSight(200)
{
	//nadaje informacje obiektowi Army na jakim Polu sie znajduje
	//oraz nadaje Polu informacje jaka Armia sie na nim znajduje
	_currentField = currentField;
	_owner = player;
	if(!_owner->isHumanPlayer())
		_stateMachine = new ScriptedStateMachine<Army>(this);
	_id = _numberOfObjects-1;
}
Army::Army(std::shared_ptr<Player> player, int move ,std::shared_ptr<Field> currentField, int hoursOfSleep) : Army(player, currentField, hoursOfSleep)
{
	_move=move;
}


Army::Army(std::shared_ptr<Player> player) {
	_owner = player;
}

std::shared_ptr<Army> Army::attack(std::shared_ptr<Army> enemy){
	bool koniec=false;
	int iter = 0;

	do{
		int ourAtack=0;	//obrazenia od atakujacego
		int Damage=0;
		++iter;
		for(int i=1; i<=10; i++){
			if(shared_from_this()->getCreatureAmount(i)==0) continue;
			for(int j=0; j<shared_from_this()->getCreatureAmount(i); j++){
				Damage= getCreature(i)->getDamage();
				ourAtack+= Damage;
			}
		}
		//liczenie ataku dla przeciwnika
		int enemyAtack=0;
		Damage=0;
		for(int i=1; i<=10; i++){
			if(enemy->getCreatureAmount(i)==0) continue;
			for(int j=0; j< enemy->getCreatureAmount(i); j++){
				Damage= enemy->getCreature(i)->getDamage();
				enemyAtack+= Damage;
			}
		}

		//obliczenie ilosci wojsk, potrzebne do rozdzielenia obrazen
		int ourAmount=0, enemyAmount=0;
		for(int i=1; i<=10; i++){
			ourAmount+=shared_from_this()->getCreatureAmount(i);
		}

		for(int i=1; i<=10; i++){
			enemyAmount+=enemy->getCreatureAmount(i);
		}

		//obliczenie strat
		//dla atakujacego
		int health;
		double damagePerCreature;
		int lose=0;
		for(int i=1;i<=10;i++){
			if(getCreatureAmount(i)==0) continue;
			health=getCreatureAmount(i)*(getCreature(i)->getMaxHealth());
			damagePerCreature=((double)getCreatureAmount(i))/((double)ourAmount);
			lose=(enemyAtack*damagePerCreature)/(getCreature(i)->getMaxHealth());
			shared_from_this()->updateCreature(i, -lose);
		}
		lose=0;
		//dla broniacego
		for(int i=1;i<=10;i++){
			if(getCreatureAmount(i)==0) continue;
			health=enemy->getCreatureAmount(i)*(enemy->getCreature(i)->getMaxHealth());
			damagePerCreature=((double)enemy->getCreatureAmount(i))/((double)enemyAmount);
			lose=(ourAtack*damagePerCreature)/(enemy->getCreature(i)->getMaxHealth());
			enemy->updateCreature(i, -lose);
		}
		for(int i=1; i<=10; i++){
			enemyAmount+=enemy->getCreatureAmount(i);
		}

		for(int i=1; i<=10; i++){
			ourAmount+=shared_from_this()->getCreatureAmount(i);
		}

		if(enemyAmount <= 0 || ourAmount <= 0) {
			if(enemyAmount <= 0 && ourAmount <=0) return nullptr;
			else if(enemyAmount <= 0) return shared_from_this();
			else return enemy;
		}

		if(iter > 200) {
			if(enemyAmount == ourAmount <= 0) return nullptr;
			else if(enemyAmount < ourAmount) return shared_from_this();
			else return enemy;
		}
	}while(!koniec);

	return nullptr;
}

void Army::updateCreature(int lv, int amount){
	_creatures[lv-1]=_creatures[lv-1]+amount;
	if(_creatures[lv-1]<0)_creatures[lv-1]=0;
	_moveD = shared_from_this()->getMinRange();
}

void Army::mergeArmy(const std::shared_ptr<Army>& targetArmy) {
	for(int i = 1; i <= 10; ++i)
		_creatures[i - 1] += targetArmy->getCreatureAmount(i);

	if(targetArmy->getMoveD() < _moveD)
		_moveD = targetArmy->getMoveD();

	_owner->removeArmy(targetArmy);
	targetArmy->getCurrentField()->removeArmy();
}

void Army::claimBuilding(const std::shared_ptr<Building>& targetBuilding)
{
	targetBuilding->getOwner()->removeBuilding(targetBuilding);
	targetBuilding->changeOwner(_owner);
	_owner->addBuilding(targetBuilding);
}

void Army::claimTown(const std::shared_ptr<Town>& targetTown)
{
	targetTown->getOwner()->removeTown(targetTown);
	targetTown->changeOwner(_owner);
	targetTown->initDefense();
	_owner->addTown(targetTown);
}

int Army::getMinRange() const {
	int min = 9999;

	for(int i = 0; i < 10; ++i) {

		if(_creatures[i] != 0 && shared_from_this()->getCreature(i + 1)->getRange() < min) {
			min = shared_from_this()->getCreature(i + 1)->getRange();
		}
	}

	if(min == 9999) return 0;
	return min;
}

void Army::update()
{
	if(!_owner->isHumanPlayer())
	{
		do
		{
			_stateMachine->update();
		}
		while(_moveD > 20);
	}
}

std::shared_ptr<Creature> Army::getCreature(int i) const {
	if(_owner != nullptr && _owner->getRace() != nullptr && _owner->getRace()->getCreature(i) != nullptr) {
		return _owner->getRace()->getCreature(i);
	}
	return 0;
}

bool Army::operator==(const Army& rhsArmy) const
{
	if(shared_from_this()->getCurrentField()->getPosition() == rhsArmy.getCurrentField()->getPosition() && shared_from_this()->_id == rhsArmy._id)
		return true;
	else
		return false;
}

void Army::move(std::shared_ptr<Field> newField, int cost)
{
	_moveD -= cost;
	_owner->updateArmyPosition(shared_from_this(), newField);
}

std::shared_ptr<Army> Army::enemyInRange() const
{
	std::shared_ptr<Army> closestArmy = nullptr;
	int minDistance = 1000;
	for(auto player : _owner->getPlayers())
	{
		//w tym miejscu uznaje, ze wszyscy gracze graja przeciwko wszystkim graczom
		if(player->getName() == _owner->getName())
			continue;
		for_each(player->getArmiesVector().begin(), player->getArmiesVector().end(),
			[&](const std::shared_ptr<Army> army) {
				double a = fabs(shared_from_this()->getCurrentField()->getPosition().x - army->getCurrentField()->getPosition().x);
				double b = fabs(shared_from_this()->getCurrentField()->getPosition().y - army->getCurrentField()->getPosition().y);
				double distance = sqrt( pow(a, 2) + pow(b, 2) );
				if(distance <= _lineOfSight && distance < minDistance)
				{
					closestArmy = army;
					minDistance = distance;
				}
		} );//koniec funkcji lambda, petli for_each
	}
	for(const auto army : _owner->getNeutralPlayer()->getArmiesVector())
	{
		double a = fabs(shared_from_this()->getCurrentField()->getPosition().x - army->getCurrentField()->getPosition().x);
		double b = fabs(shared_from_this()->getCurrentField()->getPosition().y - army->getCurrentField()->getPosition().y);
		double distance = sqrt( pow(a, 2) + pow(b, 2) );
		if(distance <= _lineOfSight && distance < minDistance)
		{
			closestArmy = army;
			minDistance = distance;
		}
	}
	return closestArmy;
}

std::shared_ptr<Army> Army::myArmyInMoveRange() const
{
	std::shared_ptr<Army> myArmy = nullptr;
	int minDistance = 1000;

	for(auto army : _owner->getArmiesVector())
	{
		if(shared_from_this() == army)
			continue;

		std::map<int, std::shared_ptr<Field> > path;
		int distance = 0;
		path = PathFinding().findPathAStar(_currentField, army->getCurrentField(), _moveD);

		for(auto step : path)
		{
			if(step.second == _currentField)
				continue;	// Pierwsze pole nie jest wliczane do kosztu

			if(step.second != nullptr)
				distance += step.second->getCost();
		}
		if(distance <= _moveD && distance < minDistance)
		{
			myArmy = army;
			minDistance = distance;
		}
	}

	return myArmy;
}

std::shared_ptr<Building> Army::buildingInRange() const
{
	std::shared_ptr<Building> closestBuilding = nullptr;
	int minDistance = 1000;
	for(auto player : _owner->getPlayers())
	{
		//w tym miejscu uznaje, ze wszyscy gracze graja przeciwko wszystkim graczom
		if(player->getName() == _owner->getName())
			continue;
		for(const auto building : player->getBuildingsVector())
		{
			double a = fabs(shared_from_this()->getCurrentField()->getPosition().x - building->getCurrentField()->getPosition().x);
			double b = fabs(shared_from_this()->getCurrentField()->getPosition().y - building->getCurrentField()->getPosition().y);
			double distance = sqrt( pow(a, 2) + pow(b, 2) );
			if(distance <= _lineOfSight && distance < minDistance)
			{
				closestBuilding = building;
				minDistance = distance;
			}
		}
	}
	for(const auto building : _owner->getNeutralPlayer()->getBuildingsVector())
	{
		double a = fabs(shared_from_this()->getCurrentField()->getPosition().x - building->getCurrentField()->getPosition().x);
		double b = fabs(shared_from_this()->getCurrentField()->getPosition().y - building->getCurrentField()->getPosition().y);
		double distance = sqrt( pow(a, 2) + pow(b, 2) );
		if(distance <= _lineOfSight && distance < minDistance)
		{
			closestBuilding = building;
			minDistance = distance;
		}
	}
	return closestBuilding;
}

std::shared_ptr<Town> Army::townInRange() const
{
	std::shared_ptr<Town> closestTown = nullptr;
	int minDistance = 1000;
	for(auto player : _owner->getPlayers())
	{
		//w tym miejscu uznaje, ze wszyscy gracze graja przeciwko wszystkim graczom
		if(player->getName() == _owner->getName())
			continue;
		for(const auto town : player->getTownsVector())
		{
			double a = fabs(shared_from_this()->getCurrentField()->getPosition().x - town->getCurrentField()->getPosition().x);
			double b = fabs(shared_from_this()->getCurrentField()->getPosition().y - town->getCurrentField()->getPosition().y);
			double distance = sqrt( pow(a, 2) + pow(b, 2) );
			if(distance <= _lineOfSight && distance < minDistance)
			{
				closestTown = town;
				minDistance = distance;
			}
		}
	}
	for(const auto town : _owner->getNeutralPlayer()->getTownsVector())
	{
		double a = fabs(shared_from_this()->getCurrentField()->getPosition().x - town->getCurrentField()->getPosition().x);
		double b = fabs(shared_from_this()->getCurrentField()->getPosition().y - town->getCurrentField()->getPosition().y);
		double distance = sqrt( pow(a, 2) + pow(b, 2) );
		if(distance <= _lineOfSight && distance < minDistance)
		{
			closestTown = town;
			minDistance = distance;
		}
	}
	return closestTown;
}

bool Army::isEnemyStronger(const std::shared_ptr<Army>& enemyArmy) const
{
	if(enemyArmy == nullptr)
	{
		return false;
	}
	int thisArmyCreaturesAmount = 0, enemyArmyCreaturesAmount = 0, friendArmyCreaturesAmount = 0;
	std::shared_ptr<Army> friendArmy = myArmyInMoveRange();
	for(int i=1; i<=10; i++)
	{
		thisArmyCreaturesAmount += shared_from_this()->getCreatureAmount(i);
		enemyArmyCreaturesAmount += enemyArmy->getCreatureAmount(i);
		if(friendArmy != nullptr)
			friendArmyCreaturesAmount += friendArmy->getCreaturesAmount(i);
	}

	if(thisArmyCreaturesAmount+friendArmyCreaturesAmount < enemyArmyCreaturesAmount)
		return true;
	else
		return false;
}

bool Army::goTo(const std::shared_ptr<Field>& targetField)
{
	if(_currentField == targetField)
		return true;
	std::map<int, std::shared_ptr<Field> > path;
	path = PathFinding().findPathAStar(_currentField, targetField, _moveD);
	for(auto step = path.begin(); step != path.end(); ++step)
	{
		if(step->second == _currentField)
			continue;	// Pierwsze pole nie jest wliczane do kosztu

		if(step->first <= _moveD)
			shared_from_this()->move(step->second, step->first);
		else
			break;
	}
	if(_currentField == targetField)
		return true;
	else
		return false;
}

bool Army::goTo(const std::shared_ptr<Object>& target)
{
	const std::shared_ptr<Field>& targetField = target->getCurrentField();
	return goTo(targetField);
}

bool Army::goToNeighbourFieldTo(const std::shared_ptr<Field>& targetField)
{
	std::map<int, std::shared_ptr<Field> > path;

	path = PathFinding().findPathAStar(_currentField, targetField, _moveD);
	if (!path.empty())
	{
	    auto iter = path.end();
	    --iter;
	    path.erase(iter);//powinno wyjsc na to samo co 'path.erase(path.rbegin());', 'std::prev( path.end() )' - ale nie zawsze wychodzi
	}
	auto iter = path.end();
	--iter;
	const std::shared_ptr<Field>& prevTargetField = iter->second;
	if(_currentField == prevTargetField)
		return true;

	for(auto step = path.begin(); step != path.end(); ++step)
	{
		if(step->second == _currentField)
			continue;	// Pierwsze pole nie jest wliczane do kosztu

		if(step->first <= _moveD)
			shared_from_this()->move(step->second, step->first);
		else
			break;
	}
	if(_currentField == prevTargetField)
		return true;
	else
		return false;
}

void Army::goAfter(const std::shared_ptr<Army>& enemyArmy)
{
	const std::shared_ptr<Field>& targetField = enemyArmy->getCurrentField();

	if(goToNeighbourFieldTo(targetField))//-1
	{
		auto winner = shared_from_this()->attack(enemyArmy);

		if(winner == shared_from_this())
		{
			//jesli ta armia wygrala
			enemyArmy->getOwner()->removeArmy(enemyArmy);
			targetField->removeArmy();
		}
		else if(winner == enemyArmy)
		{
			//jesli zwyciezyla armia przeciwna
			shared_from_this()->getOwner()->removeArmy(shared_from_this());
			_currentField->removeArmy();
		}
		else
		{
			//remis: przegraly obie armie
			enemyArmy->getOwner()->removeArmy(enemyArmy);
			targetField->removeArmy();
			shared_from_this()->getOwner()->removeArmy(shared_from_this());
			_currentField->removeArmy();
		}
	}
}

void Army::flee(const std::shared_ptr<Army>& enemyArmy)
{
	const auto enemyArmyPosition = enemyArmy->getCurrentField()->getPosition();
	const auto thisArmyPosition = _currentField->getPosition();
	auto neighbourFields = _currentField->getNeighbourFields();
	const auto nFBegin = neighbourFields.begin();

	neighbourFields.erase(nFBegin+4);//usun _currentField
	if(enemyArmyPosition.x >= thisArmyPosition.x)
	{
		neighbourFields.erase(nFBegin+7);
		if(enemyArmyPosition.y >= thisArmyPosition.y)
		{
			neighbourFields.erase(nFBegin+8);
			neighbourFields.erase(nFBegin+5);
		}
		else if(enemyArmyPosition.y < thisArmyPosition.y)
		{
			neighbourFields.erase(nFBegin+6);
			neighbourFields.erase(nFBegin+3);
		}
	}
	else if(enemyArmyPosition.x < thisArmyPosition.x)
	{
		neighbourFields.erase(nFBegin+1);
		if(enemyArmyPosition.y >= thisArmyPosition.y)
		{
			neighbourFields.erase(nFBegin+2);
			neighbourFields.erase(nFBegin+5);
		}
		else if(enemyArmyPosition.y < thisArmyPosition.y)
		{
			neighbourFields.erase(nFBegin);
			neighbourFields.erase(nFBegin+3);
		}
	}

	std::shared_ptr<Field> cheapestField = nullptr;
	int minCost = 1000;
	for(auto step : neighbourFields)
	{
		if(step->getCost() == 666)
			continue;
		if(step->getCost() < minCost)
		{
			minCost = step->getCost();
			cheapestField = step;
		}
	}

	if(cheapestField == nullptr)
	{
//		std::cout << "nie znaleziono optymalnej drogi ucieczki; Army::wander()\n";
		shared_from_this()->wander();
	}
	else
		shared_from_this()->move(cheapestField, cheapestField->getCost());
}

std::shared_ptr<Town> Army::selectClosestTown() const
{
	int pathCost = 0, minCost = 10000;
	std::shared_ptr<Town> targetTown = nullptr;
	std::map<int, std::shared_ptr<Field> > path;
	//wybieram miasto, ktore jest najblizej aktualnej armii
	for(auto town : _owner->getTownsVector())
	{
		path = PathFinding().findPathAStar(_currentField, town->getCurrentField(), _moveD);
		for(auto step = path.begin(); step != path.end(); ++step)
		{
			if(step->second == _currentField)
				continue;

			pathCost += step->first;
		}
		if(pathCost < minCost)
		{
			minCost = pathCost;
			targetTown = town;
		}
	}
	return targetTown;
}

bool Army::mergeWithArmy(const std::shared_ptr<Army>& targetArmy)
{
	if( shared_from_this()->goToNeighbourFieldTo(targetArmy->getCurrentField()) )
	{
		shared_from_this()->mergeArmy(targetArmy);
		return true;
	}
	else
		return false;
}

bool Army::claimThatBuilding(const std::shared_ptr<Building>& targetBuilding)
{
	if( shared_from_this()->goTo(targetBuilding->getCurrentField()) )
	{
		shared_from_this()->claimBuilding(targetBuilding);
		return true;
	}
	else
		return false;
}

bool Army::claimThatTown(const std::shared_ptr<Town>& targetTown)
{
	if( goTo(targetTown->getCurrentField()) )
	{
		auto winner = shared_from_this()->attack(targetTown->getArmy());

		if(winner == shared_from_this())
			claimTown(targetTown);
		else
		{
			shared_from_this()->getOwner()->removeArmy(shared_from_this());
			_currentField->removeArmy();
		}
		return true;
	}
	else
		return false;
}

void Army::setArmyToTheTown(const std::shared_ptr<Army>& army)
{
	_owner->setArmyToTheTown(army);
}

void Army::setArmyToTheSpottedBuilding(const std::shared_ptr<Army>& army)
{
	_owner->setArmyToTheSpottedBuilding(army);
}

void Army::resetArmyToTheTown()
{
	_owner->resetArmyToTheTown();
}
void Army::resetArmyToTheSpottedBuilding()
{
	_owner->resetArmyToTheSpottedBuilding();
}

const std::shared_ptr<Army>& Army::getArmyToTheTown() const
{
	return _owner->getArmyToTheTown();
}

const std::shared_ptr<Army>& Army::getArmyToTheSpottedBuilding() const
{
	return _owner->getArmyToTheSpottedBuilding();
}

void Army::wander()
{
	int randomField;
	do
		randomField = rand()%9;
	while(randomField == 4 || _currentField->getNeighbourFields()[randomField] == nullptr || _currentField->getNeighbourFields()[randomField]->getCost() == 666 || _currentField->getNeighbourFields()[randomField]->isFreeArmy() == false);

	std::map<int, std::shared_ptr<Field> > path;
	path = PathFinding().findPathAStar(_currentField, _currentField->getNeighbourFields()[randomField], _moveD);
	auto step = path.begin();
	++step;
	shared_from_this()->move(step->second, step->first);
}

void Army::buyTroops()
{
	auto buildingId = rand()%3+1;
	int number = (int)( std::floor(_owner->getGold()/_owner->getRace()->getCreature(buildingId)->getCost()) ),
		totalCost = number * _owner->getRace()->getCreature(buildingId)->getCost();

	if(_owner->getTownsVector().empty())
		return;

	if(totalCost <= _owner->getGold() && number >= 6)
	{
		// kupuje w pierwszym, domyslnym miescie
		auto field = _owner->getTownsVector()[0]->getCurrentField();

		if(field->isFreeArmy())
		{
			auto army = std::make_shared<Army>(_owner, field, 0);
			field->addArmy(army);
			army->updateCreature(buildingId, number);
			if(!_owner->isHumanPlayer())
				if(luabind::type(_owner->getLuabindStates()) == LUA_TTABLE)
					army->getFSM()->setStartState(_owner->getLuabindStates()["State_Wander"]);
			_owner->addArmy(army);
		}
		else
		{
			field->getArmy()->updateCreature(buildingId, number);
		}

		_owner->updateGold(-totalCost);
	}
}

int Army::getNumberOfArmies() const
{
	return _owner->getArmiesVector().size();
}

bool Army::isTownDefencesStronger(const std::shared_ptr<Town>& enemyTown) const
{
	return isEnemyStronger(enemyTown->getArmy());
}
