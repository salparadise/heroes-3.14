/*
 * Player.cpp
 *
 *  Created on: 16 lis 2013
 *      Author: hawker
 */
#include <cmath>

#include "Headers/Player.hpp"
#include "Headers/Army.hpp"
#include "Headers/Field.hpp"

void Player::setDefaultKeys(){
	mKeyBinding[sf::Keyboard::Left]="MapLeft";
	mKeyBinding[sf::Keyboard::Right]="MapRight";
	mKeyBinding[sf::Keyboard::Up]="MapUp";
	mKeyBinding[sf::Keyboard::Down]="MapDown";
	mKeyBinding[sf::Keyboard::E]="E";
	mKeyBinding[sf::Keyboard::K]="K";
}

std::string Player::handleInput(sf::Event &e) {
	for(auto& pair : mKeyBinding) {
		if (e.key.code==(pair.first)) return pair.second;
	}
	return 0;
}

Player::Player(std::string name, std::shared_ptr<Race> race, const std::vector<std::shared_ptr<Player> >& players, bool humanPlayer, const std::shared_ptr<Player>& neutralPlayer, luabind::object& states, int gold, int wood, int stone, int food)
				: _race(race), _name(name), _sight(5), _gold(gold), _wood(wood), _stone(stone), _food(food),
				  _humanPlayer(humanPlayer), _players(players),
				  _armyOnItsWayToTheTown(nullptr), _armyOnItsWayToTheSpottedBuilding(nullptr),
				  _neutralPlayer(neutralPlayer), _states(states)
{
	setDefaultKeys();
}

Player::~Player() { }

void Player::initiateFog(int a, int b){
	_boardSizeX=a;
	_boardSizeY=b;
	_fog=std::vector<std::vector<bool> >(a);

	for(unsigned int i=0;i<_boardSizeX;++i){
		_fog.at(i)=std::vector<bool>(b);
	}
}

void Player::print(){

}

void Player::updateFogOfWar(unsigned int a,unsigned int b,bool c){
	for(unsigned int i=0;i<_boardSizeX;++i){
		for(unsigned int j=0;j<_boardSizeY;++j){
			if(!_fog.at(j).at(i) && _sight>=sqrt(pow(abs(a-i),2)+pow(abs(b-j),2))){
				_fog.at(j).at(i) = true;
			}
			if(a==j && b==i ) _fog.at(i).at(j) = true;
		}
	}
}

bool Player::getFog(int a, int b){
	return _fog.at(a).at(b);
}

void Player::setFog(int a, int b, bool c){
	_fog.at(a).at(b)=c;
}

const std::vector<std::shared_ptr<Player> >& Player::getPlayers() const
{
	return _players;
}

void Player::addArmy(std::shared_ptr<Army> army)
{
	if(std::find(_playersArmiesVector.begin(), _playersArmiesVector.end(), army) == _playersArmiesVector.end())
		_playersArmiesVector.push_back(army);
}

void Player::removeArmy(std::shared_ptr<Army> army)
{
	std::vector<std::shared_ptr<Army>>::iterator i = std::find(_playersArmiesVector.begin(), _playersArmiesVector.end(), army);
	if(i != _playersArmiesVector.end())
		_playersArmiesVector.erase(i);
}

void Player::updateArmyPosition(const std::shared_ptr<Army>& army, const std::shared_ptr<Field>& newPosition)
{
	std::vector<std::shared_ptr<Army>>::iterator i = std::find(_playersArmiesVector.begin(), _playersArmiesVector.end(), army);
	if(i != _playersArmiesVector.end())	{
		i->get()->getCurrentField()->removeArmy();
		i->get()->setCurrentField(newPosition);
		i->get()->getCurrentField()->addArmy(army);
	}
}

void Player::addBuilding(std::shared_ptr<Building> building)
{
	if(std::find(_playersBuildingsVector.begin(), _playersBuildingsVector.end(), building) == _playersBuildingsVector.end())
		_playersBuildingsVector.push_back(building);
}

void Player::removeBuilding(std::shared_ptr<Building> building)
{
	std::vector<std::shared_ptr<Building>>::iterator i = std::find(_playersBuildingsVector.begin(), _playersBuildingsVector.end(), building);
	if(i == _playersBuildingsVector.end())
	{
		auto _neutralPlayerBuildingsVector = _neutralPlayer->getBuildingsVector();
		std::vector<std::shared_ptr<Building>>::iterator j = std::find(_neutralPlayerBuildingsVector.begin(), _neutralPlayerBuildingsVector.end(), building);
		if(j == _neutralPlayerBuildingsVector.end())
		{
			return;
		}
		else
			_neutralPlayerBuildingsVector.erase(j);
	}
	else
		_playersBuildingsVector.erase(i);

}

void Player::addTown(std::shared_ptr<Town> town)
{
	if(std::find(_playersTownsVector.begin(), _playersTownsVector.end(), town) == _playersTownsVector.end())
		_playersTownsVector.push_back(town);
}

void Player::removeTown(std::shared_ptr<Town> town)
{
	std::vector<std::shared_ptr<Town>>::iterator i = std::find(_playersTownsVector.begin(), _playersTownsVector.end(), town);
	if(i == _playersTownsVector.end())
	{
		auto _neutralPlayerTownsVector = _neutralPlayer->getTownsVector();
		std::vector<std::shared_ptr<Town>>::iterator j = std::find(_neutralPlayerTownsVector.begin(), _neutralPlayerTownsVector.end(), town);
		if(j == _neutralPlayerTownsVector.end())
		{
			return;
		}
		else
			_neutralPlayerTownsVector.erase(j);
	}
	else
		_playersTownsVector.erase(i);
}
