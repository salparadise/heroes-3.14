#include "Headers/DialogLayer.hpp"

DialogLayer::DialogLayer(std::shared_ptr<Game> g):Layer(g) {
	_isActive = true;
}

bool DialogLayer::processEvent(sf::Event &e) {
	_game->getConfig().desktop->HandleEvent(e);

	if(_gui != nullptr) {
		return false;
	}

	return true;
}

void DialogLayer::draw(sf::RenderTarget &window, sf::RenderStates states) const {
	if(_gui != nullptr) {
		_game->getConfig().sfgui->Display(*(_game->getConfig().window));
	}
}

void DialogLayer::update(sf::Time dt) {
	_game->getConfig().desktop->Update(0.f);
	if(_gui != nullptr) {
		if(_game->getConfig().clickedField != nullptr) {
			sf::Vector2i position = _game->getConfig().window->mapCoordsToPixel(sf::Vector2f(_game->getConfig().clickedField->getPosition().x, _game->getConfig().clickedField->getPosition().y), _game->getConfig().boardView);
			auto allocation = _gui->GetAllocation();
			_gui->SetAllocation(sf::FloatRect(position.x + _game->getConfig().fieldSize, position.y, allocation.width, allocation.height));
		}
	}

	if(_game->getConfig().clickedField != nullptr && _gui == nullptr && !_game->getConfig().nextPlayer) {
		createPopup();
	}

	if(_game->getConfig().nextPlayer) {
		closeCurrent();
		_game->getConfig().desktop->Remove(_gui);
		_gui = nullptr;
	}
}

void DialogLayer::createPopup() {
	auto field = _game->getConfig().clickedField;
	int options = 0;

	if(!field->isFree()) {
		_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND);

		auto box = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 5.0f);

		auto close = sfg::Button::Create("Close");
		close->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&DialogLayer::closeCurrent, this));

		if(field->getBuilding() != nullptr) {
			auto building = field->getBuilding();

			if(field->getBuilding()->getOwner() == _game->getCurrentPlayer()) {
				auto button = sfg::Button::Create();

				if(building->getType() == "Town") {
					std::shared_ptr<Town> town = std::dynamic_pointer_cast<Town>(building);
					button->SetLabel("Enter the town");
					button->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&Town::select, town));
				}
				else {
					button->SetLabel("Destroy building");
					button->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&DialogLayer::destroyBuilding, this));
				}

				button->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&DialogLayer::closeCurrent, this));
				box->Pack(button);
				++options;
			}
		}


		if(!field->isFreeArmy() && field->getArmy()->getOwner() == _game->getCurrentPlayer()) {
			auto army = field->getArmy();
			auto select = sfg::Button::Create("Select army");
			select->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&Army::select, army));
			select->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&DialogLayer::closeCurrent, this));
			box->Pack(select);
			++options;
		}

		if(!field->isFreeArmy()) {
			auto amount = sfg::Button::Create("Number of units");
			amount->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&DialogLayer::numberOfTroops, this));
			box->Pack(amount);
			++options;
		}

		box->Pack(close, false);

		if(options > 0) {
			sf::Vector2i position = _game->getConfig().window->mapCoordsToPixel(sf::Vector2f(field->getPosition().x, field->getPosition().y), _game->getConfig().mainView);
			_gui->SetAllocation(sf::FloatRect(position.x + _game->getConfig().fieldSize, position.y, 100, 50));
			_gui->Add(box);
			_game->getConfig().desktop->Add(_gui);
		}
		else {
			_gui = nullptr;
		}
	}
}

void DialogLayer::closeCurrent() {
	Layer::closeCurrent();
	_gui = nullptr;
}

void DialogLayer::numberOfTroops() {
	auto army = _game->getConfig().clickedField->getArmy();

	closeCurrent();

	_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR);
	_gui->SetTitle(army->getOwner()->getName() + ": " + army->getOwner()->getRace()->getName());
	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
	auto close = sfg::Button::Create("Close");
	close->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&DialogLayer::closeCurrent, this));

	auto table = sfg::Table::Create();

	for(int i = 0; i < 10; ++i) {
		auto th = sfg::Label::Create(army->getOwner()->getRace()->getCreature(i)->getName()),
			 td = sfg::Label::Create(toString(army->getCreaturesAmount(i + 1)));

		table->Attach( th, sf::Rect<sf::Uint32>( 0, i, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );
		table->Attach( td, sf::Rect<sf::Uint32>( 1, i, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );
	}

	box->Pack(table, false);
	box->Pack(close, false);
	_gui->SetAllocation(sf::FloatRect(150, 150, 400, 300));
	_gui->Add(box);
	centerWindow(_gui, _game->getConfig().window);
	_game->getConfig().desktop->Add(_gui);
}

void DialogLayer::destroyBuilding() {
	auto field = _game->getConfig().clickedField;

	closeCurrent();

	field->getBuilding()->getOwner()->removeBuilding(field->getBuilding());
	field->removeBuilding();
}
