# Kibols of Might and Magic (Heroes 3.14)
KoMM is an open source spinn-off from "Heroes Of Might And Magic III". It was developed as a student project in 7 person team.

Gamepaly rules are loosely based on HoMM mechanics. Rendering system was developed using SFML library.
We managed to implement a FSM-based scripted AI system from scratch. Scripts are written in Lua.

## Dependencies
- [SFML 2.1](https://www.sfml-dev.org/download/sfml/2.1/)
- [SFGUI 0.2.0](https://github.com/TankOs/SFGUI/releases)
- [Lua 5.2](https://www.lua.org/)
- [LuaBind Deboostified](https://github.com/decimad/luabind-deboostified)

Code compiles with GCC 4.8.2 (and later?).

## Screens

![ingame](http://i66.tinypic.com/feggwi.png)
Screen from the game

![ingame](http://i64.tinypic.com/1251cog.png)
In-town look

## Documentation (in polish)
Detailed description of the project: [Dokumentacja.pdf](https://drive.google.com/file/d/0B4HKN4HGapEJLWFtdmdsTXJSOXM/view?usp=sharing)