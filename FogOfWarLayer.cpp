/*
 * FogOfWarLayer.cpp
 *
 *  Created on: 14 gru 2013
 *      Author: hawker
 */

#include "Headers/FogOfWarLayer.hpp"

FogOfWarLayer::FogOfWarLayer(std::shared_ptr<Game> g):Layer(g) {
	_isActive = true;
	_drawBuffer = std::make_shared<sf::RenderTexture>();
	_drawBuffer->create(_game->getConfig().boardSize.x * _game->getConfig().fieldSize, _game->getConfig().boardSize.y * _game->getConfig().fieldSize);
}

void FogOfWarLayer::draw(sf::RenderTarget &window, sf::RenderStates states) const{
	if(_isActive) {
		Game::Config &config = _game->getConfig();
		window.setView(config.boardView);

		_drawBuffer->clear(sf::Color::Transparent);

		sf::RectangleShape black =  sf::RectangleShape(sf::Vector2f(config.fieldSize, config.fieldSize));
		black.setFillColor(sf::Color::Black);

		for(int i = 0; i < config.boardSize.y; ++i) {
			for(int j = 0; j < config.boardSize.x; ++j) {
				if(!_game->getCurrentPlayer()->getFog(i,j)){
					black.setPosition(j * config.fieldSize, i * config.fieldSize);
					_drawBuffer->draw(black);
				}
			}
		}

		_drawBuffer->display();

		sf::Sprite board(_drawBuffer->getTexture());
		window.draw(board);
	}
}
 bool FogOfWarLayer::processEvent(sf::Event& e){
	 /*
	  * Wykomentowane przez: Lukasz
	  * Powod: 	Nie wydaje mi sie, zeby to bylo nam potrzebne.
	  * 		Kliknac mozna tylko na swoje jednostki / budynki, a te na pewno sa widoczne.
	  * 		Nie usuwam, bo moze jednak kiedys sie przyda.
	  */

	 /*
	 sf::Vector2f position;
	 sf::Vector2i position2;
	 Game::Config& config = _game->getConfig();
	 switch(e.type) {
	 case sf::Event::MouseButtonPressed:
		position2.x = e.mouseButton.x;
		position2.y = e.mouseButton.y;
		position = config.window->mapPixelToCoords(position2, config.boardView);
		if(position.x < 0 || position.y < 0 || position.x > config.boardSize.x * config.fieldSize || position.y > config.boardSize.y * config.fieldSize) return true;

		if(_game->getCurrentPlayer()->getFog(
			(int)(position.x / config.fieldSize),
			(int)(position.y / config.fieldSize)
		)) return true;
		else return false;

	 default:
		 return true;
	} */
	return true;
}

 void FogOfWarLayer::update(sf::Time dt){
	 Game::Config &config = _game->getConfig();
	 std::shared_ptr<Field> field;

	 	for(int i = 0; i < config.boardSize.y; ++i) {
	 		for(int j = 0; j < config.boardSize.x; ++j) {
	 			field = _game->getField(i, j);

	 			if(field->getArmy() != nullptr&& field->getArmy()->getOwner()!=nullptr){


	 				if(field->getArmy()->getOwner()->getName().compare(_game->getCurrentPlayer()->getName())==0) {
	 					//std::cout<<field->getArmy()->getOwner()->getName()<<"="<<_game->getCurrentPlayer()->getName();
	 					_game->getCurrentPlayer()->updateFogOfWar(i,j,false);
	 				}
	 			}
	 			if(field->getBuilding() !=nullptr && field->getBuilding()->getOwner()!=nullptr){

	 				if(field->getBuilding()->getOwner()->getName().compare(_game->getCurrentPlayer()->getName())==0) {
	 					//std::cout<<field->getBuilding()->getOwner()->getName()<<"="<<_game->getCurrentPlayer()->getName();
	 					_game->getCurrentPlayer()->updateFogOfWar(i,j,false);
	 				}

	 			}
	 		}
	 	}
	}


 /* namespace My_SGA */
