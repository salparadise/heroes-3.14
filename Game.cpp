#include "Headers/Game.hpp"
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <ctime>

//include the luabind headers. Make sure you have the paths set correctly
//to the lua, luabind and Boost files.
#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>
#include <luabind/shared_ptr_converter.hpp>

#include "Headers/ScriptedStateMachine.hpp"

#include "Headers/Town.hpp"

Game::Game(std::shared_ptr<sf::RenderWindow> w, TextureHolder& textures):textures(textures), _window(w) {
	srand(time(0));
	_config.window = _window;
	_config.boardView = _window->getDefaultView();
	_config.mainView = _window->getView();
	_config.boardMove = {0, 0};
	_config.fieldSize = 50;
	_config.fieldsOnScreen = {
		(int)(_window->getSize().x / _config.fieldSize),
		(int)(_window->getSize().y / _config.fieldSize)
	};
	_config.zoomLevel = 0;
	_config.sfgui = std::make_shared<sfg::SFGUI>();
	_config.desktop = std::make_shared<sfg::Desktop>();
	_config.armyColor = {
			sf::Color(0, 169, 225),
			sf::Color(196, 0, 0),
			sf::Color(37, 156, 0),
			sf::Color(149, 80, 178),
			sf::Color(255, 64, 0),
			sf::Color(45, 62, 80),
			sf::Color(255, 191, 0),
			sf::Color(0, 183, 146),
			sf::Color(255, 255, 255)
	};


	racesInicialization();

	_neutralPlayer = std::make_shared<Player>("Neutral", _races[1], _players, true, _neutralPlayer, states);

	// -------------------------------------------------------------------------Lua
	pLua = luaL_newstate();

	//open the lua libaries - new in lua5.1
	luaL_openlibs(pLua);

	//open luabind
	luabind::open(pLua);

	registerScriptedStateMachineWithLua(pLua);
	registerObjectWithLua(pLua);
	registerActiveObjectWithLua(pLua);
	registerArmyWithLua(pLua);

	runLuaScript(pLua, "Resources/AI/ArmyStateMachine.lua");

	//grab the global table from the lua state. This will inlclude
	//all the functions and variables defined in the scripts run so far
	//(StateMachineScript.lua in this example)
	states = luabind::globals(pLua);
	// ---------------------------------------------------------------------end Lua

//	setDefaultCommandMap();
	//laduje do sfg::Destkop plik styli, ktore definiuja wyglad okien sfgui
	_config.desktop->GetEngine().LoadThemeFromFile("Resources/theme.css");
}

/*************************************************************************************************/
//konstruktor do wczytywania gry.

Game::Game(std::shared_ptr<sf::RenderWindow> w, TextureHolder& t, std::string fileName) : Game(w, t){
	std::fstream plik;
	plik.open( fileName, std::ios::in );

	if( plik.good() == true )
    	{
    		std::string line;
    		getline(plik, line );	//wczytanie id mapy
			std::string mapName;
			std::stringstream kk(line);
			kk >>_mapPath;
			loadMapFromJson(prepareJSONFile("Resources/maps/" + _mapPath));

    		getline(plik, line );	//wczytanie tury
			std::stringstream ss(line);
			ss >> _currentPlayer;
    		getline(plik, line );
    		getline(plik, line );

    		getline(plik, line ); //pierwszy gracz
    		while(line.length()!=0){

    			std::stringstream ss(line);
    			std::string name, res;
    			ss >> name;
				int raceId=0;
				ss >> raceId;
    			int gold, wood, stone, food;
				bool isAlive, isHuman;
    			ss >> res;
    			gold=stringTo<int>(res);
    			ss >> res;
    			wood=stringTo<int>(res);
    			ss >> res;
    			stone=stringTo<int>(res);
    			ss >> res;
    			food=stringTo<int>(res);
				ss >> res;
				if(res=="1") isAlive=true;
				else  isAlive=false;
				ss >> res;
				if(res=="1") isHuman=true;
				else  isHuman=false;

				std::shared_ptr<Race> race(getRace(raceId));
				std::shared_ptr<Player> player(new Player(  name, race, _players, isHuman, _neutralPlayer, states, gold, wood, stone, food) );
				if(!isAlive) player->dead();


    			//std::cout << name << " " <<raceId <<" " << gold << " " <<  wood << " " << stone<< " " << food << std::endl;
				if(player!=nullptr)

				player->initiateFog(_config.boardSize.x,_config.boardSize.y);
    			for(int k=0;k<_config.boardSize.x;++k){

    				getline(plik, line );
    				std::stringstream ss(line);
					for(int h=0;h<_config.boardSize.y;++h){

						int j;
						ss >> res;
						j= stringTo<int>(res);

						if(j==1)
						player->setFog(k,h, true);
						else
						player->setFog(k,h, false);

					}

				}
				_players.push_back(player);
    			getline(plik, line ); //kolejny gracz
    		}
			getline(plik, line );
			getline(plik, line );
			//wczytanie armii
			while(line.length()!=0){
				std::stringstream ss(line);
				int k, h, move, moveD, amount;
				std::string player, tmp;
				ss >> tmp;
				k=stringTo<int>(tmp);
				ss >> tmp;
				h=stringTo<int>(tmp);
				ss >> player;
				ss >> tmp;
				move=stringTo<int>(tmp);
				ss >> tmp;
				moveD=stringTo<int>(tmp);
				std::shared_ptr<Army> army( new Army(getPlayer(player), move, getField(k,h)));
				getField(k,h)-> addArmy(army);
				getPlayer(player)->addArmy(army);


				for(int l=1; l <=10 ; l++){
					ss >> tmp;
					amount = stringTo<int>(tmp);
					army->updateCreature(l, amount);

				}

				getline(plik, line );
			}
			getline(plik, line );
			getline(plik, line );
			//wczytanie budynkow
			while(line.length()!=0){
				std::stringstream ss(line);
				std::string tmp, player;
				int k, h;
				ss >> tmp;
				k=stringTo<int>(tmp);
				ss >> tmp;
				h=stringTo<int>(tmp);
				ss >> player;
				if(player!="Neutral") getField(k,h)->getBuilding()->changeOwner(getPlayer(player));
				if(player!="Neutral") getPlayer(player)->addBuilding(getField(k,h)->getBuilding());
				std::shared_ptr<Town> town = std::dynamic_pointer_cast<Town>(getField(k,h)->getBuilding());
				if(player!="Neutral") getPlayer(player)->addTown(town);

				getline(plik, line );

			}
    	}
		plik.close();

}

/*************************************************************************************************/


/*************************************************************************************************/

Game::~Game() {
	_config.desktop->RemoveAll();
	lua_close(pLua);
}

/*************************************************************************************************/

Game::Config& Game::getConfig() {
	return _config;
}

/*************************************************************************************************/

int Game::isEnd() {
	return _end;
}

/*************************************************************************************************/

void Game::end(int player) {
	_end = player;
	_config.desktop->RemoveAll();
}

/*************************************************************************************************/

std::shared_ptr<Field> Game::getField(int x, int y) const {
	return _board[x][y];
}

/*************************************************************************************************/

void Game::setGameState(int gs) {
	_gameState = gs;
}

int Game::getGameState() {
	return _gameState;
}


std::vector<std::shared_ptr<Field> > Game::getFieldNeighbours(std::shared_ptr<Field> field) const
{
	bool fieldFound = false;
	int i, j;
	std::vector<std::shared_ptr<Field> > neighbourFields;

	for(i=0; fieldFound == false && i< _board.size(); i++)
	{
		for(j=0; fieldFound == false && j<_board[0].size(); j++)
		{
			if(_board[i][j] == field)
			{
				fieldFound = true;
				break;
			}
		}
	}

	for(int ii = i; ii < i+3; ++ii)
	{
		for(int jj = j; jj < j+3; ++jj)
		{
			if( !((ii-1)<0 || (jj-1)<0 || (ii+1)>_config.boardSize.x || (jj+1)>_config.boardSize.y))
				neighbourFields.push_back(_board[ii-1][jj-1]);
			else
				neighbourFields.push_back(nullptr);
		}
	}

	return (neighbourFields);
}

std::vector<std::shared_ptr<Field> > Game::getFieldNeighbours(sf::Vector2i position) const
{
	std::vector<std::shared_ptr<Field> > neighbourFields;
	neighbourFields = std::vector<std::shared_ptr<Field> >(9, nullptr);

	for(int i = position.x; i < position.x+3; ++i)
	{
		for(int j = position.y; j < position.y+3; ++j)
		{
			if( !((i-1)<0 || (j-1)<0 || (i+1)>_config.boardSize.x || (j+1)>_config.boardSize.y))
				neighbourFields[(i-position.x)*3 + j-position.y] = _board[i-1][j-1];
			else
				neighbourFields[(i-position.x)*3 + j-position.y] = nullptr;
		}
	}

	return (neighbourFields);
}

/*************************************************************************************************/

void Game::processKey(sf::Event &e){
	switch (_commandMap[getCurrentPlayer()->handleInput(e)])
	{
	case 1:

				if( _config.boardView.getCenter().x > 0.5 * sf::VideoMode::getDesktopMode().width){
				_config.boardView.move(-50, 0);
				_config.boardMove.x -= 50;
				}
				break;

	case 2:
				if( _config.boardView.getCenter().x < 50 * 100 - 0.5 * sf::VideoMode::getDesktopMode().width){
				_config.boardView.move(50, 0);
				_config.boardMove.x += 50;
				}
				break;

	case 3:
				if( _config.boardView.getCenter().y > 0.5 * sf::VideoMode::getDesktopMode().height){
				_config.boardView.move(0, -50);
				_config.boardMove.y -= 50;
				}
				break;

	case 4:
				if( _config.boardView.getCenter().y < 50 * 100 - 0.5 * sf::VideoMode::getDesktopMode().height){
				_config.boardView.move(0, 50);
				_config.boardMove.y += 50;
				}
				break;

	case 5:
				nextPlayer();
				break;

	case 6:
				getCurrentPlayer()->dead();
				nextPlayer();
				break;

	default:_window->close();

	}

}

/*************************************************************************************************/
void Game::setDefaultCommandMap(){
	_commandMap["MapLeft"]=1;
	_commandMap["MapRight"]=2;
	_commandMap["MapUp"]=3;
	_commandMap["MapDown"]=4;
	_commandMap["E"]=5;
	_commandMap["K"]=6;
}

void Game::nextPlayer(){
	_config.selectedArmy = nullptr;
	_config.clickedField = nullptr;
	_config.nextPlayer = true;

	///\ zabija wszystkich graczy bez armii i budynkow
	for(auto i : _players) {
		if((i->alive()) && (i->getArmiesVector().size() == 0) && i->getBuildingsVector().size() == 0 && i->getTownsVector().size() == 0) {
			i->dead();
		}
	}

	///\ sprawdzenie warunkow zwyciestwa
	if( checkIfWon(_players[_currentPlayer]) ) {
		auto gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR);
		gui->SetTitle("War is over!");
		auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
		auto close = sfg::Button::Create("Close");
		close->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&Game::end, this,_currentPlayer));
		auto label = sfg::Label::Create("Player: " + _players[_currentPlayer]->getName() + " wins!");

		box->Pack(label, true);
		box->Pack(close, true);

		gui->SetAllocation(sf::FloatRect(200, 200, 200, 200));
		gui->Add(box);
		_config.desktop->Add(gui);
	}

	///\ nowa tura gdy ostatni z graczy
	if(++_currentPlayer>=_players.size()) {
		_currentPlayer=0;
		///\zjadanie i zabijanie armii
			for(auto i : _players) {
				//i->setFood(10); //do testowania
				if(i->alive() ) {
					int sum=0;
					int partsum=0;
					std::vector<double> armysNumbersOfCreatures;
					armysNumbersOfCreatures.clear();
					for(auto j:i->getArmiesVector()){

						partsum=0;
						for(int k=0;k<10;++k){
							partsum+=j->getCreaturesAmount(k+1);
						}
						sum+=partsum;
						armysNumbersOfCreatures.push_back(partsum);

					}
					i->setFood(i->getFood()-sum);
					if(i->getFood()<0&&i->getArmiesVector().size()>0){
						int deathCount= abs(i->getFood());
						int it=0;
						for(auto j:i->getArmiesVector()){
								for(int k=0;k<10;++k){
												int creaturesToKillInThisArmy=(int)((armysNumbersOfCreatures.at(it)/sum)*deathCount);
												int tmp =(int)( j->getCreaturesAmount(k+1)  -
														creaturesToKillInThisArmy);
												if(tmp<0) {
													j->updateCreature(k+1,-(j->getCreaturesAmount(k+1)));
													creaturesToKillInThisArmy=abs(tmp);
												}
												else{
													j->updateCreature(k+1,-creaturesToKillInThisArmy);
													break;
												}
												++it;
								}
						}
					}

				}

			}
		nextTurn();
	}

	///\ przeskakujemy martwego gracza
	if( !(_players[_currentPlayer]->alive()) )  {
		nextPlayer();
	}
	else
	{
		_players[_currentPlayer]->print();

		if(_players[_currentPlayer]->isHumanPlayer() && !checkIfWon(_players[_currentPlayer])) {
			auto gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR);
			gui->SetTitle("Next player");
			auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
			auto close = sfg::Button::Create("OK");
			close->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&Game::closeCurrent, this));
			auto label = sfg::Label::Create("Current player: " + _players[_currentPlayer]->getName());

			box->Pack(label, true);
			box->Pack(close, true);

			gui->SetAllocation(sf::FloatRect(200, 200, 200, 200));
			gui->Add(box);
			_config.desktop->Add(gui);
		}

		for(auto army : _players[_currentPlayer]->getArmiesVector())
		{
			army->restoreMoveD();
			army->update();
		}
		for(auto building : _players[_currentPlayer]->getBuildingsVector())
			building->updateResources();

		for(auto town : _players[_currentPlayer]->getTownsVector())
			town->updateResources();

		//do tego momentu gracz kierowany przez AI zrobil juz wszystko
		//co mial zrobic wiec przechodzimy do kolejnego gracza
		if(!_players[_currentPlayer]->isHumanPlayer())
			nextPlayer();
	}

	save("Resources/saves/qsave.dat");
}


bool Game::checkIfWon(std::shared_ptr<Player> player) {
	bool condition = true;
	for(auto i : _players) {
		if( i != player &&  i->alive() ) {
			condition = false;
		}
	}
	return condition;
}

void Game::nextTurn()
{
	++_turn;
	_config.selectedArmy = nullptr;
	_config.clickedField = nullptr;
}

int Game::getTurn() {
	return _turn;
}

Json::Value Game::getFromJSON(const std::string JSONHandler, const int TownIdValue, const std::string& JSONKey)
{
	unsigned int i;
	Json::Value root, key;
	Json::Reader reader;
	bool parsingSuccessful = reader.parse( JSONHandler, root );
	if ( !parsingSuccessful )
		throw std::runtime_error("Failed to parse configuration\n" + reader.getFormattedErrorMessages());
	for(i = 0; i < root.size(); ++i)  // Iterates over the sequence elements.
		if(root[i]["id"].asInt() == TownIdValue)
			break;
	key = root[i][JSONKey];
	//return key.asString();
	return key;
}

Json::Value Game::getDataFromFile(std::string path, std::string key, int i) {
	std::string map = Game::prepareJSONFile(path);
	return getDataFromString(map, key, i);
}

Json::Value Game::getDataFromString(std::string &str, std::string key, int i) {
	Json::Value root;
	Json::Reader reader;

	if(!reader.parse(str, root)) {
		throw std::runtime_error("Failed to parse configuration\n" + reader.getFormattedErrorMessages());
	}
	if(i < 0) {
		return root[key];
	}
	else {
		return root[i][key];
	}
}

std::string Game::prepareJSONFile(const std::string& filename)
{
	std::ifstream in(filename, std::ios::in);
	if (in)
	{
		std::string json;
		in.seekg(0, std::ios::end);
		json.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&json[0], json.size());
		in.close();
		return(json);
	}
	throw std::runtime_error("error in Game::prepareJSONFile. cannot load " + filename);
}

void Game::closeCurrent() {
	auto widget = sfg::Context::Get().GetActiveWidget();

	while(widget->GetName() != "Window") {
		widget = widget->GetParent();
	}

	_config.nextPlayer = false;
	_config.desktop->Remove(widget);
}

void Game::racesInicialization(){
	std::string jednostki = prepareJSONFile("Resources/scripts/creatures.json");
	std::string rasy = prepareJSONFile("Resources/scripts/races.json");
	std::string herosi = prepareJSONFile("Resources/scripts/heros.json");

	{
		// Magiczna sztuczka. Ma za zadanie obejsc problemy z zamkami
		int textures[] = { 8, 8, 8, 10, 8, 9, 8, 11, 8, 8};

		for (int i = 0; i < 15; i++){
			std::string nazwa = getFromJSON(rasy, i, "name").asString();
			if(nazwa.compare("")!=0) {
				_races.push_back(std::make_shared<Race>(nazwa, getFromJSON(rasy, i, "id").asInt()));

				int texture = getFromJSON(rasy, i, "texture_id").asInt();
				_races[_races.size() - 1]->setTextureId(textures[texture]);
			}
		}

		for (int i = 0; i < 100; i++){
			std::string nazwa = getFromJSON(jednostki, i, "name").asString();
			if(nazwa.compare("") == 0) continue;
			int j = getFromJSON(jednostki, i, "town_id").asInt();
			std::shared_ptr<Race> race = getRace(j);

			if(race != nullptr) {
				if(!getFromJSON(jednostki, i, "upgrade").asString().compare("false")) {
					race->setCreature(std::make_shared<Creature>(
						nazwa,
						stringTo<int>(getFromJSON(jednostki, i, "max_health").asString()),
						stringTo<int>(getFromJSON(jednostki, i, "max_damage").asString()),
						stringTo<int>(getFromJSON(jednostki, i, "min_damage").asString()),
						stringTo<int>(getFromJSON(jednostki, i, "range").asString()),
						4 + stringTo<int>(getFromJSON(jednostki, i, "level").asString()) )
					);
				}
				else {
					race->setCreature(std::make_shared<Creature>(
						nazwa,
						stringTo<int>(getFromJSON(jednostki, i, "max_health").asString()),
						stringTo<int>(getFromJSON(jednostki, i, "max_damage").asString()),
						stringTo<int>(getFromJSON(jednostki, i, "min_damage").asString()),
						stringTo<int>(getFromJSON(jednostki, i, "range").asString()),
						stringTo<int>(getFromJSON(jednostki, i, "level").asString()) - 1
					));
				}
			}
		}

		for( int i=0; i<20; i++){
			std::string nazwa = getFromJSON(herosi, i, "name").asString();
			if(nazwa.compare("") == 0)continue;
			int j=stringTo<int>(getFromJSON(herosi, i, "town_id").asString());
			std::shared_ptr<Race> rase = getRace(j);

			if(rase!=nullptr){
				rase->setHero(std::make_shared<Hero>(nazwa));
			}
		}

	}
}

void Game::loadMapFromJson(const std::string& JSONHandler)
{
	Json::Value root;
	Json::Reader reader;
	bool parsingSuccessful = reader.parse( JSONHandler, root );
	if ( !parsingSuccessful )
		throw std::runtime_error("Failed to parse configuration\n" + reader.getFormattedErrorMessages());

	const Json::Value towns = root["towns"];
	const Json::Value mines = root["mines"];
	const Json::Value fields = root["fields"];

	_config.boardSize.x = root["width"].asInt();
	_config.boardSize.y = root["height"].asInt();

//-------------------------------------------------------------------------

	//Inicjalizacja _board'a
	//tekstury terenu posiadaja indeksy 0-7, budynki 8-15, jednostki 16
	_board = std::vector<std::vector<std::shared_ptr<Field>>>(_config.boardSize.y);
	for(int i = 0; i < _config.boardSize.y; ++i) {
		_board[i] = std::vector<std::shared_ptr<Field>>(_config.boardSize.x);

		for(int j = 0; j < _config.boardSize.y; ++j) {
			_board[i][j] = std::shared_ptr<Field>(new Field(sf::Vector2i(i*_config.fieldSize, j*_config.fieldSize), textures));
		}
	}

	for(int i = 0; i < _config.boardSize.y; ++i)
	{
		for(int j = 0; j < _config.boardSize.x; ++j)
		{
			int x = fields[i * _config.boardSize.x + j]["x"].asInt() - 1,
				y = fields[i * _config.boardSize.x + j]["y"].asInt() - 1;



			_board[x][y]->setTextureId(fields[i*_config.boardSize.x+j]["texture_id"].asInt());
			_board[x][y]->setCost(fields[i*_config.boardSize.x+j]["cost"].asInt());
			_board[x][y]->setGroundColor(fields[i*_config.boardSize.x+j]["color"].asString());
		}
	}

	for(unsigned i = 0; i < towns.size(); ++i)
	{
		auto town = std::make_shared<Town>();
		town->setBuildingId(towns[i]["town_id"].asInt());
		town->setTextureId(towns[i]["mine_id"].asInt()+7);

		town->changeOwner(_neutralPlayer);
		town->setCurrentField(_board[towns[i]["x"].asInt()-1][towns[i]["y"].asInt()-1]);
		_board[towns[i]["x"].asInt()-1][towns[i]["y"].asInt()-1]->setBuilding(town);
		_neutralPlayer->addTown(town);
	}

	for(unsigned i = 0; i < mines.size(); ++i)
	{
		int mineId = mines[i]["mine_id"].asInt();
		std::shared_ptr<Building> mine;
		switch(mineId)
		{
		case 1:
			mine = std::dynamic_pointer_cast<Building>(std::make_shared<GoldMine>());
			break;
		case 2:
			mine = std::dynamic_pointer_cast<Building>(std::make_shared<Mill>());
			break;
		case 3:
			mine = std::dynamic_pointer_cast<Building>(std::make_shared<OrePit>());
			break;
		case 4:
			mine = std::dynamic_pointer_cast<Building>(std::make_shared<Sawmill>());
			break;
		}

		mine->setBuildingId(mines[i]["mine_id"].asInt());
		mine->setTextureId(mines[i]["mine_id"].asInt()+11);

		mine->changeOwner(_neutralPlayer);
		mine->setCurrentField(_board[mines[i]["x"].asInt()-1][mines[i]["y"].asInt()-1]);
		_board[mines[i]["x"].asInt()-1][mines[i]["y"].asInt()-1]->setBuilding(mine);
		_neutralPlayer->addBuilding(mine);
	}

	// W: wypelniam neighbourField
	for(int i = 0; i < _config.boardSize.x; ++i) {
		for(int j = 0; j < _config.boardSize.y; ++j) {
			_board[i][j]->setNeighbourFields(getFieldNeighbours(sf::Vector2i(i,j)));
		}
	}
//-------------------------------------------------------------------------

}

std::shared_ptr<Race> Game::getRace(int id){
	for(unsigned int i=0; i<_races.size(); i++){
		if(_races[i]!=nullptr)if(_races[i]->getId()==id) return _races[i];

	}
	return nullptr;
}

void Game::runLuaScript(lua_State* L, const char* script_name)
{
	if (int error = luaL_dofile(L, script_name) != 0)
	{
		std::ostringstream buffer;
		buffer << "ERROR (" << error << "): Problem with lua script file " << script_name;
		throw std::runtime_error(buffer.str());
	}
}

void Game::registerScriptedStateMachineWithLua(lua_State* pLua)
{
	luabind::module(pLua)
    		[
    		 luabind::class_<ScriptedStateMachine<Army> >("ScriptedStateMachine")
    		 .def("popState", &ScriptedStateMachine<Army>::popState)
    		 .def("pushState", &ScriptedStateMachine<Army>::pushState)
    		 .def("getCurrentState", &ScriptedStateMachine<Army>::getCurrentState)
    		 ];
}

void Game::registerObjectWithLua(lua_State* pLua)
{
	luabind::module(pLua)
    		[
    		 luabind::class_<Object>("Object")//std::shared_ptr<Object>
    		 .def_readonly("id", &Object::_id)
    		 .def("getId", &Object::getId)
    		 ];
}

void Game::registerActiveObjectWithLua(lua_State* pLua)
{
	luabind::module(pLua)
    		[
    		 luabind::class_<ActiveObject, Object, std::shared_ptr<ActiveObject> >("ActiveObject")//inny zapis dziedziczenia po Object: luabind::bases<Object>. konieczny przy dziedziczeniu wielobazowym
    		 ];
}


void Game::registerArmyWithLua(lua_State* pLua)
{

	luabind::module(pLua)
		[
		 luabind::class_<std::enable_shared_from_this<Army> >("std::enable_shared_from_this<Army>"),
		 luabind::class_<Army, luabind::bases<ActiveObject, std::enable_shared_from_this<Army> >, std::shared_ptr<Army> >("Army")
		 .def("attack", &Army::attack)
		 .def("getFSM", &Army::getFSM)
		 .def(luabind::const_self == luabind::const_self)
		 .def("enemyInRange", &Army::enemyInRange)
		 .def("myArmyInMoveRange",&Army::myArmyInMoveRange)
		 .def("buildingInRange",&Army::buildingInRange)
		 .def("townInRange",&Army::townInRange)
		 .def("isEnemyStronger", &Army::isEnemyStronger)
		 .def("goTo", (bool(Army::*)(const std::shared_ptr<Field>&)) &Army::goTo)
		 .def("goTo", (bool(Army::*)(const std::shared_ptr<Object>&)) &Army::goTo)
		 .def("goAfter",&Army::goAfter)
		 .def("flee",&Army::flee)
		 .def("selectClosestTown",&Army::selectClosestTown)
		 .def("mergeWithArmy",&Army::mergeWithArmy)
		 .def("claimThatBuilding",&Army::claimThatBuilding)
		 .def("setArmyToTheTown",&Army::setArmyToTheTown)
		 .def("setArmyToTheSpottedBuilding",&Army::setArmyToTheSpottedBuilding)
		 .def("resetArmyToTheTown",&Army::resetArmyToTheTown)
		 .def("resetArmyToTheSpottedBuilding",&Army::resetArmyToTheSpottedBuilding)
		 .def("getArmyToTheTown",&Army::getArmyToTheTown)
		 .def("getArmyToTheSpottedBuilding",&Army::getArmyToTheSpottedBuilding)
		 .def("wander",&Army::wander)
		 .def("buyTroops",&Army::buyTroops)
		 .def("getNumberOfArmies",&Army::getNumberOfArmies)
		 .def("isTownDefencesStronger",&Army::isTownDefencesStronger)
		 .def("checkRefCount", &Army::checkRefCount)
		 //.def("",&Army::)
		 //.def_readwrite("", &Army::)
		 ];
	luabind::module(pLua)
		[
		 luabind::class_<Building, ActiveObject, std::shared_ptr<Building> >("Building")
		];
	luabind::module(pLua)
		[
		 luabind::class_<Town, Building, std::shared_ptr<Town> >("Town")
		];
}

//Funkcja do zapisu gry. Zapisuje co sie dalo, dalej brakuje paru elementow
void Game::save(std::string fileName){
	std::fstream plik;
	plik.open( fileName, std::ios::out );
	if( plik.good() == true )
    	{

		//id mapy
		plik <<  _mapPath << std::endl;
		//dane na temat gry: liczba graczy, tura
		plik << _currentPlayer;

		plik << std::endl;
		//linia pusta
		plik << std::endl;
		//linia pusta
		plik << std::endl;
		//wpisanie graczy
		//nazwa, rasa, surowce (gold wood stone food), sight, isAlive(0 || 1), i moze by tak fog...
		//kazdy gracz w osobnej linii
		for(std::shared_ptr<Player> i : _players){

			plik << i->getName();
			plik << " ";
			plik << i->getRace()->getId();
			plik << " ";
			plik << i->getGold();
			plik << " ";
			plik << i->getWood();
			plik << " ";
			plik << i->getStone();
			plik << " ";
			plik << i->getFood();
			plik << " ";
			if(i->alive()) plik << "1";
			else plik << "0";
			plik << " ";
			if(i->isHumanPlayer())plik << "1";
			else plik << "0";

			plik << std::endl;

			//fog of war

			for(unsigned int k=0;k<_config.boardSize.x;++k){
				for(unsigned int h=0;h<_config.boardSize.y;++h){
					if(i->getFog(k , h )) plik << "1";
					else plik << "0";
					plik << " ";
				}
				plik << std::endl;
			}

		}
		plik << std::endl;
		plik << std::endl;
		//zczytanie armii z mapy

		for(unsigned int k=0;k<_config.boardSize.x;++k){
			for(unsigned int h=0;h<_config.boardSize.y;++h){

				std::shared_ptr<Army> a= _board[k][h]->getArmy();
				if(a!=nullptr){
					plik << k << " " << h << " " << a->getOwner()->getName() << " ";
					plik << a->getMove() << " " << a->getMoveD();
					for(int i=1; i<=10; i++){
						plik << " ";
						plik << a->getCreatureAmount(i);

					}
					plik << std::endl;
				}



			}
		}
		plik << std::endl;
		plik << std::endl;
		//przeiterowanie calej mapy i zczytanie budynkow (wlascicieli)
		for(unsigned int k=0;k<_config.boardSize.x;++k){
			for(unsigned int h=0;h<_config.boardSize.y;++h){

				std::shared_ptr<Building> a= _board[k][h]->getBuilding();
				if(a!=nullptr){
					plik << k << " " << h << " " << a->getOwner()->getName() << std::endl;
				}

			}
		}
		//std::cout << "Udalo sie zapisac";

	}
	plik.close();



}

std::shared_ptr<Player> Game::getPlayer(std::string name){
	for(std::shared_ptr<Player> i : _players){
		if(i->getName()== name) return i;
	}
}

