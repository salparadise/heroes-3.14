/**
 * @file PathFinding.cpp
 *
 * @date 8 gru 2013
 * @author Wojtek
 * @version 1.0
 *
 */

#include "Headers/PathFinding.hpp"

PathFinding::PathFinding() {
	
}

PathFinding::~PathFinding() {

}

std::vector<std::shared_ptr<Field>> PathFinding::showPossibleMoves(const std::shared_ptr<Field> startField, int range) {
	std::vector<std::shared_ptr<Field>> possibleMoves;

	std::multiset<Node, NodeCompare> closedSet;
	std::multiset<Node, NodeCompare> openSet;

	openSet.insert(Node(startField));

	std::multiset<Node, NodeCompare>::iterator currentNode;

	// f_ oznacza koszt dojscia do danego fielda, z g_ i h_ nie korzystam wcale
	while( !openSet.empty() && openSet.begin()->f_ <= range ) {

	//	std::cout << openSet.begin()->f_ << "\t";
		currentNode = openSet.begin();
		closedSet.insert(*currentNode);

		HasField hasField;
	//	int i=0;
		for(auto it : currentNode->field_->getNeighbourFields()) {
			//std::cout << i++;
			//std::cout << it->getCost() << " ";
			hasField.field_ = it;
			if( it == nullptr || (find_if(closedSet.begin(), closedSet.end(), hasField) != closedSet.end()) || (it)->getCost() == 666 ) {
				continue;
			}

			int f = 0;
			if( currentNode->field_->getPosition().x - it->getPosition().x != 0 && currentNode->field_->getPosition().y - it->getPosition().y != 0 ) { // true for v[1], v[3], v[5], v[7]
				f = (1.41)*(it)->getCost() + currentNode->f_; // sqrt(2)
			} else if( currentNode->field_->getPosition().x - it->getPosition().x == 0 && currentNode->field_->getPosition().y - it->getPosition().y == 0 ) {
				continue;
			} else {
				f = ((it)->getCost()) + currentNode->f_;
			}
			auto openSetIt = find_if(openSet.begin(), openSet.end(), hasField);
			if( openSetIt == openSet.end() ) {
				openSet.insert( Node(f, it)  );
			//	std::cout << "+ ";
			}
			else if( f < openSetIt->f_ ) {
				openSet.insert( Node(f, openSetIt->field_) );
				openSet.erase(openSetIt);
			//	std::cout << "x ";
			}
		//	std::cout << f << "\t";
		}
		openSet.erase(currentNode);

	}

	for( auto i : closedSet ) {
		possibleMoves.push_back(i.field_);
	}
	//std::cout << "\n";
	return possibleMoves;
}


int PathFinding::calculateHeuristic(const std::shared_ptr<Field> currentField, const std::shared_ptr<Field> destinationField) {
	int h = 0;

	int x1 = currentField->getPosition().x/50, y1 = currentField->getPosition().y/50,
		x2 = destinationField->getPosition().x/50, y2 = destinationField->getPosition().y/50;

	h = abs(x1-x2)*10 + abs(y1-y2)*10;

	return h;
}


std::map<int, std::shared_ptr<Field>> PathFinding::findPathAStar(const std::shared_ptr<Field> startField, const std::shared_ptr<Field> destinationField, int range) {
	std::multiset<Node, NodeCompare> closedSet;
	std::multiset<Node, NodeCompare> openSet;

	openSet.insert(Node(startField));

	std::multiset<Node, NodeCompare>::iterator currentNode;
	while( !openSet.empty() ) {

		currentNode = openSet.begin();

		if(currentNode->field_ == destinationField) {
			closedSet.insert(*currentNode);

			auto temp = (reconstructPath(closedSet, destinationField));
			temp.insert(std::pair<int, std::shared_ptr<Field>>(0, startField));

			return temp;
		}

		closedSet.insert(*currentNode);

		HasField hasField;
		for(auto it : currentNode->field_->getNeighbourFields()) {
			hasField.field_ = it;
			if( it == nullptr || (find_if(closedSet.begin(), closedSet.end(), hasField) != closedSet.end()) || (it)->getCost() == 666 )
				continue;

			int g = 0;
			if( currentNode->field_->getPosition().x - it->getPosition().x != 0 && currentNode->field_->getPosition().y - it->getPosition().y != 0 ) { // true for v[1], v[3], v[5], v[7]
				g = (1.41)*(it)->getCost() + currentNode->g_; // sqrt(2)
			} else if( currentNode->field_->getPosition().x - it->getPosition().x == 0 && currentNode->field_->getPosition().y - it->getPosition().y == 0 ) {
				continue;
			} else {
				g = ((it)->getCost()) + currentNode->g_;
			}

			std::multiset<Node, NodeCompare>::iterator openSetIt = find_if(openSet.begin(), openSet.end(), hasField);
			if( openSetIt == openSet.end() ) {
			//	std::cout << "dupa" << g <<"\t";
				int h = calculateHeuristic(it, destinationField);
			//	std::cout << "dupa" << g <<"\t" << h <<"\t";
				openSet.insert( Node(g+h, g, h, it, std::make_shared<Node>(*currentNode) )  );
			}
			else if( g < openSetIt->g_ ) {
				openSet.insert(Node(g + openSetIt->h_, g, openSetIt->h_, openSetIt->field_, std::make_shared<Node>(*currentNode)));
				openSet.erase(openSetIt);
			}
		}
		openSet.erase(currentNode);
	}
	std::map<int, std::shared_ptr<Field>> t;
	return t; //jakis typedef? NO_PATH? std::exception ??
}


std::map<int, std::shared_ptr<Field>> PathFinding::reconstructPath(const std::multiset<Node, NodeCompare> closedSet, const std::shared_ptr<Field> destinationField) {
	std::map<int, std::shared_ptr<Field>> path;

	HasField hasField;
	hasField.field_ = destinationField;

	std::multiset<Node>::iterator it = find_if(closedSet.begin(), closedSet.end(), hasField);

	while( it->parent_ != nullptr) {
		path.insert(std::pair<int, std::shared_ptr<Field>>(it->g_, it->field_));

		hasField.field_ = it->parent_->field_;
		it = find_if(closedSet.begin(), closedSet.end(), hasField);
	}
//std::cout << "\n\n";
	return path;
}

