#include "Headers/Layer.hpp"

void Layer::closeCurrent() {
	auto widget = sfg::Context::Get().GetActiveWidget();

	while(widget->GetName() != "Window") {
		widget = widget->GetParent();
	}

	_game->getConfig().clickedField = nullptr;
	_game->getConfig().desktop->Remove(widget);
}

