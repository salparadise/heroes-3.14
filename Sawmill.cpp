#include "Headers/Sawmill.hpp"

Sawmill::Sawmill() {
	_income = 3;
	_textureId = 12;
}

Sawmill::~Sawmill() {

}

void Sawmill::updateResources() {
	if(_owner != nullptr) {
		_owner->updateWood(_income);
	}
}
