#include "Headers/GroundLayer.hpp"

GroundLayer::GroundLayer(std::shared_ptr<Game> g):Layer(g) {
	_isActive = true;
	_drawBuffer = std::make_shared<sf::RenderTexture>();
	_drawBuffer->create(_game->getConfig().boardSize.x * _game->getConfig().fieldSize, _game->getConfig().boardSize.y * _game->getConfig().fieldSize);

	_groundMusicPlayer=std::shared_ptr<MusicPlayer>(new MusicPlayer());

//	for(auto i : _game->getPlayers())
//		for(auto j : i->getArmiesVector())
//			_game->getBoard()[static_cast<int>(j->getCurrentField()->getPosition().x/_game->getConfig().fieldSize)][static_cast<int>(j->getCurrentField()->getPosition().y/_game->getConfig().fieldSize)]->addArmy(j);
}

bool GroundLayer::processEvent(sf::Event &e) {
	Game::Config& config = _game->getConfig();

	if(_isActive && _movingArmy.army == nullptr && _gui == nullptr && !config.nextPlayer) {
		sf::Vector2f position;
		sf::Vector2i position2;
		std::vector<std::shared_ptr<Field>> neighbourFields;

		switch(e.type) {
		case sf::Event::MouseButtonPressed:
			position2.x = e.mouseButton.x;
			position2.y = e.mouseButton.y;
			position = config.window->mapPixelToCoords(position2, config.boardView);
			if(position.x < 0 || position.y < 0 || position.x > config.boardSize.x * config.fieldSize || position.y > config.boardSize.y * config.fieldSize) return true;
			if(config.selectedArmy == nullptr) {
				auto field = _game->getField((int)(position.x / config.fieldSize), (int)(position.y / config.fieldSize));

				if(!field->isFreeArmy() || !field->isFreeBuilding()) {
					config.clickedField = field;
				}
			}
			else {
				auto field = _game->getField((int)(position.x / config.fieldSize), (int)(position.y / config.fieldSize));
				if(e.mouseButton.button == sf::Mouse::Button::Left) {
					if(find(_possibleMoves.begin(), _possibleMoves.end(), field) != _possibleMoves.end() && field != config.selectedArmy) {
						_endField = field;

						if(_endField->isFree()) {
							acceptMove();
						}
						else {
							std::string result;
							if(!_endField->isFreeArmy()) {
								if(_endField->getArmy()->getOwner() == _game->getCurrentPlayer()) {
									result = "Merge armies, sir?";
								}
								else {
									result = "Shall we attack 'em, sir?";
								}
							}
							else if(!_endField->isFreeBuilding()) {
								if(_endField->getBuilding()->getOwner() == _game->getCurrentPlayer()) {
									acceptMove();
									return false;
								}
								else {
									result = "You want us to take the building?";
								}
							}
							_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR);
							_gui->SetTitle("Are you sure?");
							auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
							auto close = sfg::Button::Create("No, just kidding.");
							auto ok = sfg::Button::Create("Hell yeah!");
							close->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&GroundLayer::closeCurrent, this));
							close->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&GroundLayer::cancelMove, this));
							ok->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&GroundLayer::closeCurrent, this));
							ok->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&GroundLayer::acceptMove, this));
							auto label = sfg::Label::Create(result);

							box->Pack(label, true);
							box->Pack(ok, true);
							box->Pack(close, true);

							_gui->SetAllocation(sf::FloatRect(200, 200, 200, 200));
							_gui->Add(box);
							centerWindow(_gui, _game->getConfig().window);
							config.desktop->Add(_gui);
						}
					}
				}
				else {
					if(config.selectedArmy->getArmy() != nullptr) {
						config.selectedArmy->getArmy()->deselect();
						_possibleMoves.clear();
					}
				}
				config.selectedArmy = nullptr;
				config.clickedField = nullptr;
			}
			break;

		case sf::Event::MouseWheelMoved:
			if(double scroll = e.mouseWheel.delta * 0.1f) {
				if(scroll < 0.f) {
					++config.zoomLevel;
				}
				else {
					--config.zoomLevel;
				}

				if(config.zoomLevel < 0) config.zoomLevel = 0;
				else if(config.zoomLevel > 15) config.zoomLevel = 15;

				auto size = config.window->getDefaultView().getSize();
				config.boardView.setSize(size.x + 50 * config.zoomLevel, size.y + 50 * config.zoomLevel * ((double)size.y / (double)size.x));
			}
			break;

		case sf::Event::MouseMoved:

			break;

		default:
			break;
		}
	}

	return true;
}

void GroundLayer::draw(sf::RenderTarget &window, sf::RenderStates states) const {
	Game::Config &config = _game->getConfig();
	if(_isActive && !config.nextPlayer) {
		window.setView(config.boardView);
		int currentPlayer, k = 0;
		sf::Color armyColor;

		_drawBuffer->clear(sf::Color::Black);


		for(auto i : _game->getPlayers()) {
			if(i == _game->getCurrentPlayer()) {
				currentPlayer = k;
				armyColor = config.armyColor[k];
				armyColor.a = 150;
			}
			++k;
		}

		//rysowanie podloza i budynkow bez graczy
		std::shared_ptr<Field> field;
		sf::Sprite sprite;

		for(int i = 0; i < config.boardSize.y; ++i) {
			for(int j = 0; j < config.boardSize.x; ++j) {
				field = _game->getField(j, i);
				_drawBuffer->draw(*field);
			}
		}

		//rysowanie budynkow
		sf::Sprite buildingSprite;

		for(auto i : _game->getPlayers()) {
			for(auto j : i->getBuildingsVector()) {
				buildingSprite.setTexture(_game->textures[j->getTextureId()]);
				buildingSprite.setPosition(j.get()->getCurrentField()->getPosition().x, j.get()->getCurrentField()->getPosition().y - 50);
				_drawBuffer->draw(buildingSprite);
			}
		}

		sf::Sprite townSprite;

		for(auto i : _game->getPlayers()) {
			for(auto j : i->getTownsVector()) {
				townSprite.setTexture(_game->textures[i->getRace()->getTextureId()]);
				townSprite.setPosition(j.get()->getCurrentField()->getPosition().x, j.get()->getCurrentField()->getPosition().y - 50);
				_drawBuffer->draw(townSprite);
			}
		}

		for(auto i : _game->getNeutralPlayer()->getBuildingsVector()) {
			buildingSprite.setTexture(_game->textures[i->getTextureId()]);
			buildingSprite.setPosition(i.get()->getCurrentField()->getPosition().x, i.get()->getCurrentField()->getPosition().y - 50);
			_drawBuffer->draw(buildingSprite);
		}

		for(auto i : _game->getNeutralPlayer()->getTownsVector()) {
			buildingSprite.setTexture(_game->textures[_game->getNeutralPlayer()->getRace()->getTextureId()]);
			buildingSprite.setPosition(i.get()->getCurrentField()->getPosition().x, i.get()->getCurrentField()->getPosition().y - 50);
			_drawBuffer->draw(buildingSprite);
		}

		//rysowanie niebieskich kwadratow pokazujacych mozliwy ruch
		sf::RectangleShape rect;
		rect.setSize(sf::Vector2f(50, 50));
		rect.setFillColor(sf::Color(armyColor));

		if(config.selectedArmy != nullptr && _movingArmy.army == nullptr) {
			for(auto i : _possibleMoves) {
				if(i != nullptr && i != config.selectedArmy) {
					rect.setPosition(i->getPosition().x, i->getPosition().y);
					_drawBuffer->draw(rect);
				}
			}

			rect.setPosition(config.selectedArmy->getPosition().x, config.selectedArmy->getPosition().y);
			_drawBuffer->draw(rect);
		}

		//rysowanie armii polegajac na wektorze przechowujacym wskazniki do wszystkich armii

		sf::Sprite armySprite;
		armySprite.setTexture(_game->textures[24]);

		k = 0;

		for(auto i : _game->getPlayers()) {
			armySprite.setTexture(_game->textures[24 + k]);

			sf::RectangleShape rect;
			rect.setSize(sf::Vector2f(50, 50));

			for(auto j : i->getArmiesVector()) {
				if(j != _movingArmy.army) {

					if(k == currentPlayer) {
						rect.setFillColor(armyColor);
						rect.setPosition(j.get()->getCurrentField()->getPosition().x, j.get()->getCurrentField()->getPosition().y);
						_drawBuffer->draw(rect);
					}

					armySprite.setPosition(j.get()->getCurrentField()->getPosition().x, j.get()->getCurrentField()->getPosition().y - 50);
					_drawBuffer->draw(armySprite);
				}
			}

			++k;
		}

		// rysowanie aktualnie poruszanej armii
		if(_movingArmy.army != nullptr) {
			sf::RectangleShape rect;
			rect.setSize(sf::Vector2f(50, 50));
			rect.setFillColor(armyColor);

			for(auto i : _movingArmy.path) {
				rect.setPosition(sf::Vector2f(i.second->getPosition().x, i.second->getPosition().y));
				_drawBuffer->draw(rect);
			}

			sf::Sprite sprite;
			sprite.setPosition(_movingArmy.current.x, _movingArmy.current.y - 50);
			sprite.setTextureRect(sf::IntRect(0, 0, 50, 100));
			sprite.setTexture(_game->textures[24 + currentPlayer]);
			_drawBuffer->draw(sprite);
		}

		_drawBuffer->display();
		sf::Sprite board(_drawBuffer->getTexture());
		window.draw(board);

		window.setView(config.mainView);
	}
}

void GroundLayer::update(sf::Time dt) {
	double v = 1;
	auto screenSize = _game->getConfig().window->getSize();
	double move = v * dt.asMilliseconds();
	sf::Vector2i mousePosition = sf::Mouse::getPosition(*(_game->getConfig().window));
	Game::Config &config = _game->getConfig();

	if(config.nextPlayer) {
		_movingArmy.army = nullptr;
	}

	if(mousePosition.x <= config.fieldSize && config.boardView.getCenter().x > 0.5 * screenSize.x) {
		config.boardView.move(-move, 0);
		config.boardMove.x -= move;
	}
	if(mousePosition.x >= screenSize.x - config.fieldSize * 5 && mousePosition.x <= screenSize.x - config.fieldSize * 4 && config.boardView.getCenter().x < config.fieldSize * config.boardSize.x - 0.5 * screenSize.x + 4 * config.fieldSize) {
		config.boardView.move(move, 0);
		config.boardMove.x += move;
	}
	if(mousePosition.y <= config.fieldSize && config.boardView.getCenter().y > 0.5 * screenSize.y - 40) {//F: -40 z uwagi na topBar'a
		config.boardView.move(0, -move);
		config.boardMove.y -= move;
	}
	if(mousePosition.y >= screenSize.y - config.fieldSize && config.boardView.getCenter().y < config.fieldSize * config.boardSize.y - 0.5 * screenSize.y) {
		config.boardView.move(0, move);
		config.boardMove.y += move;
	}

	if(config.selectedArmy == nullptr) {
		_possibleMoves.clear();

		std::shared_ptr<Field> field;
		for(int i = 0; i < config.boardSize.y; ++i) {
			for(int j = 0; j < config.boardSize.x; ++j) {
				field = _game->getField(i, j);
				if(field->getArmy() != nullptr && field->getArmy()->isSelected()) {
					config.selectedArmy = field;
					_possibleMoves = PathFinding().showPossibleMoves(config.selectedArmy, config.selectedArmy->getArmy()->getMoveD());
				}
			}
		}
	}

	if(_movingArmy.army != nullptr) {
		auto &ma = _movingArmy;	// skrot

		if(ma.current == ma.endField->getPosition()) {
			std::string result = "";

			auto field = ma.endField;
			int moveCost = 0;
			for(auto i : ma.path) {
				if(i.second == ma.startField) {
					continue;	// Pierwsze pole nie jest wliczane do kosztu
				}

				if(i.second != nullptr) {
					moveCost += i.second->getCost();
				}
			}

			if(field->getArmy() != nullptr || field->getBuilding() != nullptr) {
				if(field->getArmy() != nullptr) {
					if(field->getArmy()->getOwner() == _game->getCurrentPlayer()) {
						// Napotkano sojusznicza armie. Dojdzie do polaczenia obu oddzialow.
						field->getArmy()->deselect();
						ma.army->mergeArmy(field->getArmy());
						ma.army->move(ma.endField, moveCost);

						result += "Armies merged!\n";
					}
					else {
						// Napotkano wroga armie. Stoczymy bitwe.
						ma.army->deselect();
						ma.startField->addArmy(nullptr);
						auto winner = ma.army->attack(field->getArmy());

						//dzwiek ataku
						{
						_groundMusicPlayer->_sounds["attack"]->setLoop(false);
						_groundMusicPlayer->_sounds["attack"]->setVolume(75);
						_groundMusicPlayer->_sounds["attack"]->play();
						}

						if(winner == ma.army) {
							field->getArmy()->getOwner()->removeArmy(field->getArmy());
							field->addArmy(nullptr);
							field->addArmy(ma.army);
							ma.army->move(ma.endField, moveCost);
							result += "Victory is yours. Good job, sir!\n";
						}
						else if(winner == field->getArmy()) {
							_game->getCurrentPlayer()->removeArmy(ma.army);
							ma.army = nullptr;
							result += "The battle is lost.\nYour army has been defeated.\n";
						}
						else {
							_game->getCurrentPlayer()->removeArmy(ma.army);
							field->getArmy()->getOwner()->removeArmy(field->getArmy());
							field->addArmy(nullptr);
							ma.army = nullptr;
							result += "Ehm.\nYour army has been defeated.\nBut during the battle\nyou have killed all enemies.\n";
						}
					}
				}

				if(field->getBuilding() != nullptr && ma.army != nullptr) {
					if(field->getBuilding()->getOwner() != _game->getCurrentPlayer()) {
						std::shared_ptr<Town> town = std::dynamic_pointer_cast<Town>(field->getBuilding());
						if(town == nullptr) {
							field->getBuilding()->getOwner()->removeBuilding(field->getBuilding());
							field->getBuilding()->changeOwner(_game->getCurrentPlayer());
							_game->getCurrentPlayer()->addBuilding(field->getBuilding());
							result += "Building taken.\n";
						}
						else {
							// Jest to miasto
							auto winner = ma.army->attack(town->getArmy());

							if(winner == ma.army) {
								town->getOwner()->removeBuilding(town);
								town->changeOwner(_game->getCurrentPlayer());
								town->initDefense();
								_game->getCurrentPlayer()->addTown(town);

								ma.army->move(ma.endField, moveCost);
								result += "Town taken! Excellent!\n";
							}
							else {
								_game->getCurrentPlayer()->removeArmy(ma.army);
								ma.army = nullptr;
								result += "Town had too many defenders!\nYour army is lost.";
							}

						}

					}

					if(ma.army != nullptr) {
						ma.army->move(field, moveCost);
						field->addArmy(ma.army);
						field->getArmy()->deselect();
						ma.startField->addArmy(nullptr);
					}
				}

				ma.army = nullptr;
			}
			else {
					//dzwiek poruszania sie
				{
						_groundMusicPlayer->_sounds["moving"]->setLoop(false);
						_groundMusicPlayer->_sounds["moving"]->setVolume(75);
						_groundMusicPlayer->_sounds["moving"]->play();
				}
				
				ma.startField->addArmy(nullptr);
				ma.army->move(ma.endField, moveCost);
			
				ma.army = nullptr;
			}

			if(result != "") {
				auto gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR);
				gui->SetTitle("Message:");
				auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
				auto close = sfg::Button::Create("OK");
				close->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&GroundLayer::closeCurrent, this));
				auto label = sfg::Label::Create(result);

				box->Pack(label, true);
				box->Pack(close, true);

				gui->SetAllocation(sf::FloatRect(200, 200, 200, 200));
				gui->Add(box);
				centerWindow(gui, _game->getConfig().window);
				_game->getConfig().desktop->Add(gui);
			}

		}
		else {
			std::shared_ptr<Field> current = nullptr,
								   next = nullptr;

			current = _game->getField(ma.current.x / config.fieldSize, ma.current.y / config.fieldSize);

			for(auto i = ma.path.begin(); i != ma.path.end(); ++i) {
				if(i->second != nullptr) {
					if(next != nullptr) {
						next = i->second;
						break;
					}

					if(i->second == current) {
						next = current;
					}
				}
			}

			double vx = 0,
				   vy = 0;

			if(next == nullptr || next == current) {
				next = ma.endField;
			}

			if(next->getPosition().x < ma.current.x) {
				vx = -1.5;
			}
			else if(next->getPosition().x > ma.current.x) {
				vx = 1.5;
			}

			if(next->getPosition().y < ma.current.y) {
				vy = -1.5;
			}
			else if(next->getPosition().y > ma.current.y) {
				vy = 1.5;
			}

			_movingArmy.current.x += vx;
			_movingArmy.current.y += vy;
		}
	}
}

void GroundLayer::acceptMove() {
	_movingArmy.army = _game->getConfig().selectedArmy->getArmy();
	_movingArmy.startField = _game->getConfig().selectedArmy;
	_movingArmy.endField = _endField;
	_movingArmy.path = PathFinding().findPathAStar(_movingArmy.startField, _movingArmy.endField, _game->getConfig().selectedArmy->getArmy()->getMoveD());
	_movingArmy.current = _game->getConfig().selectedArmy->getPosition();

	_game->getConfig().selectedArmy->getArmy()->deselect();
	_game->getConfig().selectedArmy = nullptr;
	_endField = nullptr;
	_gui = nullptr;
}

void GroundLayer::cancelMove() {
	_endField = nullptr;
	_gui = nullptr;
}
