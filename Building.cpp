/**
 * @file Building.cpp
 *
 * @date 10.01.2014
 * @version 1.0
 *
 */

#include "Headers/Building.hpp"
#include "Headers/Field.hpp"

bool Building::operator==(const Building& rhsBuilding)
{
	if(this->getCurrentField()->getPosition() == rhsBuilding.getCurrentField()->getPosition() && this->_id == rhsBuilding._id)
		return true;
	else
		return false;
}
