#include "Headers/SidebarLayer.hpp"
#include "Headers/StringConverter.hpp"

SidebarLayer::SidebarLayer(std::shared_ptr<Game> g):Layer(g), _guiPositionAndSize(sf::FloatRect(_game->getConfig().window->getSize().x-_sidebarWidth, _topbarHeight, _sidebarWidth, _game->getConfig().window->getSize().y)), _topbarPositionAndSize(sf::FloatRect(0, 0, _game->getConfig().window->getSize().x, _topbarHeight)) {
	_isActive = true;
	Game::Config& config = _game->getConfig();
	sf::View mainView = config.mainView;
	_sfgui = config.sfgui;

	//GUI: belka na gorze
	_topbar = sfg::Window::Create(sfg::Window::Style::BACKGROUND);
	_topbar->SetTitle("Top Bar");
	_topbar->SetAllocation(_topbarPositionAndSize);
	_topbar->SetId("topBar");

	//GUI: menu boczne
	_sidebar = sfg::Window::Create(sfg::Window::Style::BACKGROUND);
	_sidebar->SetTitle( "Title" );
	_sidebar->SetAllocation(_guiPositionAndSize);
	_sidebar->SetId("sideBar");

	//MINIMAP: tworze obraz minimapy
	sf::Uint8 pixels[config.boardSize.x*config.boardSize.y*4];
	auto board = _game->getBoard();
	for(auto boardX = board.begin(); boardX != board.end(); ++boardX)
	{
		unsigned i = (boardX - board.begin());
		for(auto boardY = boardX->begin(); boardY != boardX->end(); ++boardY)
		{
			unsigned j = (boardY - boardX->begin()) ;
			pixels[j*board.size()*4+i*4] = boardY->get()->getGroundColor().r;
			pixels[j*board.size()*4+i*4+1] = boardY->get()->getGroundColor().g;
			pixels[j*board.size()*4+i*4+2] = boardY->get()->getGroundColor().b;
			pixels[j*board.size()*4+i*4+3] = 0xffffff;//boardY->get()->getGroundColor().a dla fajnego efektu
		}
	}
	minimapImage.create(board.size(), board.size(), pixels);

	//do konstruktora sf::View() podaje obszar ktory bede pokazywal w widoku
	minimap = sf::View(sf::FloatRect(0,0,config.boardSize.x*config.fieldSize, config.boardSize.y*config.fieldSize));//(sf::FloatRect(0,0,mainView.getSize().x, mainView.getSize().y));
	//do setViewport podaje kwadrat do ktorego sf::View (w tym przypadku minimap) bedzie rysowany. dziala w zakresie [0;1]!
	minimap.setViewport(sf::FloatRect((_guiPositionAndSize.left+5.0)/static_cast<float>(mainView.getSize().x), (_guiPositionAndSize.top+5.0)/static_cast<float>(mainView.getSize().y), (_guiPositionAndSize.top+5.0+_sidebarWidth)/static_cast<float>(mainView.getSize().x), (_guiPositionAndSize.top+5.0+_sidebarWidth)/static_cast<float>(mainView.getSize().y)));
	minimap.zoom(0.1f);

	table = sfg::Table::Create();
	sfml_canvas = sfg::Canvas::Create();
	sfml_canvas->SetRequisition(sf::Vector2f( 20.f, 20.f ));
	sfml_canvas->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&SidebarLayer::viewMove, this));

	//SIDEBAR: Guziki i Comboboxy
	button2 = sfg::Button::Create("Wyjdz");
	button2->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&Game::end, _game,99));
	button3 = sfg::Button::Create("Koniec tury");
	button3->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&Game::nextPlayer, _game));

	_sidebarComboBoxCastle = sfg::ComboBox::Create();
	_sidebarComboBoxCastle->GetSignal(sfg::ComboBox::OnSelect).Connect(std::bind(&SidebarLayer::selectCastle, this));

	_sidebarComboBoxArmy = sfg::ComboBox::Create();
	_sidebarComboBoxArmy->GetSignal(sfg::ComboBox::OnSelect).Connect(std::bind(&SidebarLayer::selectAndCenterArmy, this));

	//sf::Rect<sf::Uint32>( od ktorej kolumny, od ktorego wiersza sie rysuje, na szerokosc ilu kolumn, na szerokosc ilu wierszy )
	table->Attach( sfml_canvas, sf::Rect<sf::Uint32>( 0, 0, 2, 1 ), sfg::Table::EXPAND | sfg::Table::FILL, sfg::Table::EXPAND | sfg::Table::FILL, sf::Vector2f( 0.f, 0.f ) );
	table->Attach( button2, sf::Rect<sf::Uint32>( 0, 1, 1, 1 ), sfg::Table::FILL, sfg::Table::FILL, sf::Vector2f( 0.f, 0.f ) );
	table->Attach( button3, sf::Rect<sf::Uint32>( 1, 1, 1, 1 ), sfg::Table::FILL, sfg::Table::FILL, sf::Vector2f( 0.f, 0.f ) );
	table->Attach(_sidebarComboBoxCastle, sf::Rect<sf::Uint32>( 0, 2, 2, 1 ), sfg::Table::FILL, sfg::Table::EXPAND, sf::Vector2f( 0.f, 0.f ));
	table->Attach(_sidebarComboBoxArmy, sf::Rect<sf::Uint32>( 0, 3, 2, 1 ), sfg::Table::FILL, sfg::Table::EXPAND, sf::Vector2f( 0.f, 0.f ));

	_sidebar->Add(table);

	//TOPBAR
	_topbarTable = sfg::Table::Create();
	_topbarLabel1 = sfg::Label::Create("Jedzenie: ");
	_topbarLabel2 = sfg::Label::Create("Drewno: ");
	_topbarLabel3 = sfg::Label::Create("Kamien: ");
	_topbarLabel4 = sfg::Label::Create("Zloto: ");

	//znalezione nie kradzione
	_topbarImage1 = sfg::Image::Create(_game->textures[17].copyToImage());
	_topbarImage2 = sfg::Image::Create(_game->textures[19].copyToImage());
	_topbarImage3 = sfg::Image::Create(_game->textures[18].copyToImage());
	_topbarImage4 = sfg::Image::Create(_game->textures[23].copyToImage());

	_topbarTable->Attach(_topbarImage1, sf::Rect<sf::Uint32>( 0, 0, 1, 1 ), sfg::Table::EXPAND, sfg::Table::EXPAND);
	_topbarTable->Attach(_topbarLabel1, sf::Rect<sf::Uint32>( 0, 0, 2, 1 ), sfg::Table::EXPAND | sfg::Table::FILL, sfg::Table::FILL);
	_topbarTable->Attach(_topbarImage2, sf::Rect<sf::Uint32>( 2, 0, 1, 1 ), sfg::Table::EXPAND, sfg::Table::EXPAND);
	_topbarTable->Attach(_topbarLabel2, sf::Rect<sf::Uint32>( 2, 0, 2, 1 ), sfg::Table::EXPAND | sfg::Table::FILL, sfg::Table::FILL);
	_topbarTable->Attach(_topbarImage3, sf::Rect<sf::Uint32>( 4, 0, 1, 1 ), sfg::Table::EXPAND, sfg::Table::EXPAND);
	_topbarTable->Attach(_topbarLabel3, sf::Rect<sf::Uint32>( 4, 0, 2, 1 ), sfg::Table::EXPAND | sfg::Table::FILL, sfg::Table::FILL);
	_topbarTable->Attach(_topbarImage4, sf::Rect<sf::Uint32>( 6, 0, 1, 1 ), sfg::Table::EXPAND, sfg::Table::EXPAND);
	_topbarTable->Attach(_topbarLabel4, sf::Rect<sf::Uint32>( 6, 0, 2, 1 ), sfg::Table::EXPAND | sfg::Table::FILL, sfg::Table::FILL);

	_topbar->Add(_topbarTable);

	config.desktop->Add(_topbar);

	config.desktop->Add(_sidebar);

	updateResources();
}

void SidebarLayer::viewMove() const
{
	sf::Vector2i position2;
	sf::Vector2f worldPosition;
	Game::Config& config = _game->getConfig();
	auto screenSize = config.boardView.getSize();
	position2 = sf::Mouse::getPosition();
	worldPosition = config.window->mapPixelToCoords(position2, minimap);

	if(worldPosition.x < 0.5 * screenSize.x)
		worldPosition.x = 0.5 * screenSize.x;
	else if(worldPosition.x > config.fieldSize * config.boardSize.x - 0.5 * screenSize.x + 4 * config.fieldSize)
		worldPosition.x = config.fieldSize * config.boardSize.x - 0.5 * screenSize.x + 4 * config.fieldSize;

	if(worldPosition.y < 0.5 * screenSize.y + _topbarHeight)
		worldPosition.y = 0.5 * screenSize.y + _topbarHeight;
	else if(worldPosition.y > config.fieldSize * config.boardSize.y - 0.5 * config.boardView.getSize().y)
		worldPosition.y = config.fieldSize * config.boardSize.y - 0.5 * config.boardView.getSize().y;

	config.boardView.setCenter(worldPosition);
}

void SidebarLayer::selectAndCenterArmy() const
{
	Game::Config& config = _game->getConfig();
	const std::vector<std::shared_ptr<Army>>& currentPlayersArmiesVector = _game->getCurrentPlayer()->getArmiesVector();

	auto screenSize = config.boardView.getSize();
	int selectedItem = static_cast<int>(_sidebarComboBoxArmy->GetSelectedItem());
	sf::Vector2f viewPosition(currentPlayersArmiesVector[selectedItem]->getCurrentField()->getPosition());

	if(viewPosition.x < 0.5 * screenSize.x)
		viewPosition.x = 0.5 * screenSize.x;
	else if(viewPosition.x > config.fieldSize * config.boardSize.x - 0.5 * screenSize.x + 4 * config.fieldSize)
		viewPosition.x = config.fieldSize * config.boardSize.x - 0.5 * screenSize.x + 4 * config.fieldSize;

	if(viewPosition.y < 0.5 * screenSize.y - _topbarHeight)
		viewPosition.y = 0.5 * screenSize.y - _topbarHeight;
	else if(viewPosition.y > config.fieldSize * config.boardSize.y - 0.5 * config.boardView.getSize().y)
		viewPosition.y = config.fieldSize * config.boardSize.y - 0.5 * config.boardView.getSize().y;
	config.boardView.setCenter(viewPosition);
}

void SidebarLayer::selectCastle() const
{
	Game::Config& config = _game->getConfig();
	const std::vector<std::shared_ptr<Building>>& currentPlayersBuildingsVector = _game->getCurrentPlayer()->getBuildingsVector();

	auto screenSize = config.boardView.getSize();
	int selectedItem = static_cast<int>(_sidebarComboBoxArmy->GetSelectedItem());
	sf::Vector2f viewPosition(currentPlayersBuildingsVector[selectedItem]->getCurrentField()->getPosition());
//	std::cout << config.boardView.getSize().x << config.boardView.getCenter().x << "\n";

	if(viewPosition.x < 0.5 * screenSize.x)
		viewPosition.x = 0.5 * screenSize.x;
	else if(viewPosition.x > config.fieldSize * config.boardSize.x - 0.5 * screenSize.x + 4 * config.fieldSize)
		viewPosition.x = config.fieldSize * config.boardSize.x - 0.5 * screenSize.x + 4 * config.fieldSize;

	if(viewPosition.y < 0.5 * screenSize.y - _topbarHeight)
		viewPosition.y = 0.5 * screenSize.y - _topbarHeight;
	else if(viewPosition.y > config.fieldSize * config.boardSize.y - 0.5 * config.boardView.getSize().y)
		viewPosition.y = config.fieldSize * config.boardSize.y - 0.5 * config.boardView.getSize().y;

	config.boardView.setCenter(viewPosition);
}

void SidebarLayer::updateResources()
{
	std::string stats = "";
	stats += "Jedzenie: ";
	stats += toString<int>(_game->getCurrentPlayer()->getFood());
	_topbarLabel1->SetText(stats);
	stats = "";
	stats += "Drewno: ";
	stats += toString<int>(_game->getCurrentPlayer()->getWood());
	_topbarLabel2->SetText(stats);
	stats = "";
	stats += "Kamien: ";
	stats += toString<int>(_game->getCurrentPlayer()->getStone());
	_topbarLabel3->SetText(stats);
	stats = "";
	stats += "Zloto: ";
	stats += toString<int>(_game->getCurrentPlayer()->getGold());
	_topbarLabel4->SetText(stats);
}

bool SidebarLayer::processEvent(sf::Event &e) {
	if(_isActive)
	{
		Game::Config& config = _game->getConfig();
		_sidebar->HandleEvent(e);
		//_topbar->HandleEvent(e);
		config.desktop->HandleEvent(e);
		if(e.type == sf::Event::MouseButtonPressed) {
			updateResources();
		}
	}
	return (true);
}

void SidebarLayer::draw(sf::RenderTarget &window, sf::RenderStates states) const {
	if(_isActive)
	{
		Game::Config& config = _game->getConfig();
		//zmienne tymczasowe do rysowania minimapy
		sf::Image redDotImage, blackDotImage;
		sf::Texture minimapTexture;
		sf::Sprite minimapSprite;
		int k = 0;
		sf::Color armyColor = config.armyColor[k];
		redDotImage.create(1, 1, armyColor);
		blackDotImage.create(1, 1, sf::Color::Black);
		minimapTexture.loadFromImage(minimapImage);

		//aktualizacja minimapy o pozycje jednostek
		for(auto player : _game->getPlayers())
		{
			redDotImage.setPixel(0,0,config.armyColor[k]);
			for(auto army : player->getArmiesVector())
				minimapTexture.update(redDotImage, static_cast<unsigned>(army->getCurrentField()->getPosition().x/config.fieldSize), static_cast<unsigned>(army->getCurrentField()->getPosition().y/config.fieldSize));
			++k;
		}

		//aktualizacja minimapy o mgle wojny
		for(int i = 0; i < config.boardSize.y; ++i)
			for(int j = 0; j < config.boardSize.x; ++j)
				if(!_game->getCurrentPlayer()->getFog(i,j))
					minimapTexture.update(blackDotImage, j, i);

		minimapSprite.setTexture(minimapTexture);
		//domyslnie 192 bo taki bedzie rozmiar mapy (jest podzielne przez 16; rozmiary map sa wielokrotnoscia 16)
		//zmienilem na 176 bo sie nie miescilo do sfgui
		minimapSprite.setScale(176/config.boardSize.x, 176/config.boardSize.x);
		//przelaczam sie na widok minimap bo na minimapie chce narysowac mojeopowyzej przygotowanego sprite'a
//		window.setView(minimap);

		sfml_canvas->Bind();
		sfml_canvas->Draw(minimapSprite);
		sfml_canvas->Display();
		sfml_canvas->Unbind();

//		window.setView(config.mainView);
//		config.sfgui->Display(*(config.window));
		window.draw(*_sidebar);
		window.draw(*_topbar);
	}
}

void SidebarLayer::update(sf::Time dt)
{
	Game::Config& config = _game->getConfig();
	static sf::Time time;
	time += dt;

	_sidebar->Update(dt.asMilliseconds());
	_topbar->Update(dt.asMilliseconds());

	if(time.asMilliseconds() > 1000)
	{
		time = sf::Time::Zero;

		const std::vector<std::shared_ptr<Army>>& currentPlayersArmiesVector = _game->getCurrentPlayer()->getArmiesVector();
		const std::vector<std::shared_ptr<Building>>& currentPlayersBuildingsVector = _game->getCurrentPlayer()->getBuildingsVector();

		if( !(_sidebarComboBoxArmy->HasModal() || _sidebarComboBoxCastle->HasModal()) )
		{
			//Czyszcze _sidebarComboBoxArmy/Castle aby zaraz wsadzic tam nowe wartosci z army/buildingFieldsVector
			for(std::size_t i=0; i < _sidebarComboBoxArmy->GetItemCount(); i++)
				_sidebarComboBoxArmy->RemoveItem(i);

			for(std::size_t i=0; i < _sidebarComboBoxCastle->GetItemCount(); i++)
				_sidebarComboBoxCastle->RemoveItem(i);

			for_each(currentPlayersArmiesVector.begin(), currentPlayersArmiesVector.end(),
					[&](const std::shared_ptr<Army> army) {
						std::string str = "";
						str += "Armia nr ";
						str += toString<int>(army->_id);
						str += " (";
						str += toString<int>(army->getCurrentField()->getPosition().x/config.fieldSize);
						str += ", ";
						str += toString<int>(army->getCurrentField()->getPosition().y/config.fieldSize);
						str += ")";
//						std::ostringstream oss;
//						oss.clear();
//						oss << "Armia nr " << army->_id << " (" << army->getCurrentField()->getPosition().x/config.fieldSize << ", " << army->getCurrentField()->getPosition().y/config.fieldSize << ")";
						_sidebarComboBoxArmy->AppendItem(str);
					} );

			for_each(currentPlayersBuildingsVector.begin(), currentPlayersBuildingsVector.end(),
					[&](const std::shared_ptr<Building> building) {
						std::ostringstream oss;
						oss << "Budynek (" << building->getCurrentField()->getPosition().x/config.fieldSize << ", " << building->getCurrentField()->getPosition().y/config.fieldSize << ")";
						_sidebarComboBoxCastle->AppendItem(oss.str());
					} );
		}
	}

	//aktualizacja minimapy ze wzgledu na polozenie armiii
//	sf::CircleShape armyCircleShape;
//	std::shared_ptr<sf::Texture> circleShapeTexture = std::make_shared<sf::Texture>();
//	armyCircleShape.setTexture(circleShapeTexture.get());
//	armyCircleShape.setRadius(192/config.fieldsOnScreen.x);
//	armyCircleShape.setFillColor(sf::Color::Red);
//	for(auto player : _game->getPlayers())
//		for(auto army : player->getArmiesVector())
//			minimapTexture.update(armyCircleShape.getTexture()->copyToImage(), (army->getCurrentField()->getPosition().x*192)/config.boardSize.x, (army->getCurrentField()->getPosition().y*192)/config.boardSize.y);
/*
	nie chce ponizszego jeszcze usuwac bo mam tu przyklad jak sie robi funkcje lambda
	for(int i=0; i < config.boardSize.x; i++)
	{
		for(int j=0; j < config.boardSize.y; j++)
		{
			if(_game->getField(i, j)->getArmy() != nullptr)
			{
				if(std::find_if(armyFieldsVector.begin(), armyFieldsVector.end(),
						[&](const std::shared_ptr<Field> field) -> bool {
							return (_game->getField(i, j)->getPosition() == field->getPosition());
						} ) == armyFieldsVector.end())
					armyFieldsVector.push_back(_game->getField(i, j));
			}
			else if(_game->getField(i, j)->getBuilding() != nullptr)
			{
				if(std::find_if(buildingFieldsVector.begin(), buildingFieldsVector.end(),
						[&](const std::shared_ptr<Field> field) -> bool {
							return (_game->getField(i, j)->getPosition() == field->getPosition());
						} ) == buildingFieldsVector.end())
					buildingFieldsVector.push_back(_game->getField(i, j));
			}
		}
	}
*/

	//tylko po to aby nie dalo sie zmienic rozmiaru ani przesunac gui
	_sidebar->SetAllocation(_guiPositionAndSize);
	_topbar->SetAllocation(_topbarPositionAndSize);
}
