#include "Headers/TopLayer.hpp"

TopLayer::TopLayer(std::shared_ptr<Game> g):Layer(g) {
	_isActive = true;
}

bool TopLayer::processEvent(sf::Event &e) {
	if(_isActive) {
		switch(e.type) {
		case sf::Event::Closed:
			_game->end();
			break;

		default:

			break;
		}
	}
	return true;
}

void TopLayer::draw(sf::RenderTarget &window, sf::RenderStates states) const {
	if(_isActive) {

	}
}

void TopLayer::update(sf::Time dt) {

}
