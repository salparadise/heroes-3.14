#include "Headers/MetaGame.hpp"

TextureHolder MetaGame::textures;
FontHolder MetaGame::fonts;

MetaGame::MetaGame() {
	//http://stackoverflow.com/questions/14836691/is-it-better-to-use-shared-ptr-reset-or-operator/14837300#14837300
	//_window = std::shared_ptr<sf::RenderWindow>(new sf::RenderWindow(sf::VideoMode(800, 600), "Heroes 3.5", sf::Style::Fullscreen));
	getCfg();
	if(_rozdzielczosc.find("x") != std::string::npos){
		std::string values=_rozdzielczosc;
	unsigned int a = stringTo<unsigned int>(values.substr(0,values.find("x")));
	unsigned int b = stringTo<unsigned int>(values.substr(values.find("x")+1,values.length()-values.find("x")+1));
	_window = std::make_shared<sf::RenderWindow>(sf::VideoMode(a, b), "Heroes 3.5", sf::Style::Fullscreen);
	}

	else _window = std::make_shared<sf::RenderWindow>(sf::VideoMode(1000, 700), "Heroes 3.5", sf::Style::Fullscreen);
	_window->setFramerateLimit(30);
	//_mainMenu = std::shared_ptr<MainMenu>(new MainMenu());

	_musicPlayer=std::shared_ptr<MusicPlayer>(new MusicPlayer());
}


void MetaGame::metaLoop(){
	try {
			sfg::SFGUI sfgui;
			sf::Event event;

			//poczatek muzyki w menu
			_musicPlayer->play(MusicPlayer::Menu);
			
			_window->resetGLStates();

			_desktop.LoadThemeFromFile("Resources/menu.css");
			loadResources();

			while(_window->isOpen()) {
				//-------------------------------------Update---------------------------------------------------------------
				if(_gui != nullptr) _desktop.Update(0.f);
				else createMenu();

				sf::Vector2i offset(((int)_window->getSize().x - 1366) / 2,
									((int)_window->getSize().y - 768) / 2);

				if(offset.y < 0) offset.y = 0;

				_background.setTexture(textures[33]);
				_background.setPosition(sf::Vector2f(offset.x, offset.y));

				switch(state){
				case RESOLUTION:{
					std::string values = _rozdzielczosc;
					unsigned int a = stringTo<unsigned int>(values.substr(0,values.find("x")));
					unsigned int b = stringTo<unsigned int>(values.substr(values.find("x")+1,values.length()-values.find("x")+1));
					if(a==_window->getSize().x && b==_window->getSize().y) {
						clearMenu();
						break;
					}
					_window->create(sf::VideoMode(a, b), "Heroes 3.5", sf::Style::Fullscreen);
					_window->resetGLStates();
					updateCfg();
					_resolution=nullptr;


					createMenu();
					break;
				}

				case HOTSEAT:
				{
					std::vector<std::tuple<std::string, int, bool, sf::Vector2i>> players;

					std::string path;
					auto maps = listFiles("./Resources/maps/");
					int mapNo = _availableMaps->GetSelectedItem() - 1,
						j = 0;

					if(mapNo >= 0) {
						for(auto m : maps) {
							if(j == mapNo) {
								path = m;
								break;
							}
							++j;
						}
					}

					int maxPlayers = 0;

					try {
						maxPlayers = getMapData(path, "town_count").asInt();
					}
					catch(...) {
						clearMenu();
						state = MAINMENU;
						break;
					}

					if(maxPlayers < 1) {
						clearMenu();
						state = MAINMENU;
						break;
					}
					Json::Value towns = getMapData(path, "towns");
					std::vector<sf::Vector2i> positions;

					for(unsigned i = 0; i < towns.size(); ++i) {
						positions.push_back(sf::Vector2i(towns[i]["x"].asInt() - 1, towns[i]["y"].asInt() - 1));
					}

					j = 0;
					for(auto i : _playerGui) {
						if(std::get<0>(i)->GetText() != "Player") {
							players.push_back(std::make_tuple(
								std::get<0>(i)->GetText(),
								_playerRaces[std::get<1>(i)->GetSelectedItem()].first,
								std::get<2>(i)->IsActive(),
								positions[j++]
							));
						}
					}

					for(int i = 0; i < maxPlayers; ++i) {
						if(players.size() >= maxPlayers) break;


						players.push_back(std::make_tuple(
							"Player" + toString(i + 1),
							_playerRaces[std::get<1>(_playerGui[i])->GetSelectedItem()].first,
							std::get<2>(_playerGui[i])->IsActive(),
							positions[i]
						));
					}

					clearMenu();

					_game = std::make_shared<HotSeatGame>(players, _window, textures, path);

					_musicPlayer->play(MusicPlayer::Map);
					gameLoop();
					_musicPlayer->play(MusicPlayer::Menu);
					_desktop.LoadThemeFromFile("Resources/menu.css");
				}
					break;

				case NETWORK:
					break;
					
				case LOAD:
					clearMenu();
					
					_game = std::make_shared<Game>( _window, textures, "Resources/saves/qsave.dat");
					
					_musicPlayer->play(MusicPlayer::Map);
					gameLoop();
					_musicPlayer->play(MusicPlayer::Menu);
					_desktop.LoadThemeFromFile("Resources/menu.css");
					
					break;
				case QGAME :
				{
					std::vector<std::tuple<std::string, int, bool, sf::Vector2i>> players;

					std::string path;
					auto maps = listFiles("./Resources/maps/");
					int mapNo = _availableMaps->GetSelectedItem() - 1,
						j = 0;

					if(mapNo >= 0) {
						for(auto m : maps) {
							if(j == mapNo) {
								path = m;
								break;
							}
							++j;
						}
					}

					int maxPlayers = 0;

					try {
						maxPlayers = getMapData(path, "town_count").asInt();
					}
					catch(...) {
						clearMenu();
						state = MAINMENU;
						break;
					}

					if(maxPlayers < 1) {
						clearMenu();
						state = MAINMENU;
						break;
					}
					Json::Value towns = getMapData(path, "towns");
					std::vector<sf::Vector2i> positions;

					for(unsigned i = 0; i < towns.size(); ++i) {
						positions.push_back(sf::Vector2i(towns[i]["x"].asInt() - 1, towns[i]["y"].asInt() - 1));
					}

					j = 0;
					for(auto i : _playerGui) {
						if(std::get<0>(i)->IsLocallyVisible()) {
							std::string name = std::get<0>(i)->GetText();
							if(name.compare("Player") == 0) name = std::string(j == 0 ? "Human " : "Computer ") + "Player " + toString(j + 1);

							players.push_back(std::make_tuple(
								name,
								_playerRaces[std::get<1>(i)->GetSelectedItem()].first,
								std::get<2>(i)->IsActive(),
								positions[j++]
							));
						}
					}

					clearMenu();

					_game = std::make_shared<SinglePlayerGame>(players, _window, textures, path);
					_musicPlayer->play(MusicPlayer::Map);
					gameLoop();
					if(campa){
						if(_winner==0) ++_lastWonCampaign;
						updateCfg();
						campa=false;
					}
					_musicPlayer->play(MusicPlayer::Menu);
					_desktop.LoadThemeFromFile("Resources/menu.css");
				}
					break;

				case QUIT:
					_window->close(); break;

				default : break;
				}

				state=MAINMENU;//Petla gry przerwana, wracamy do menu

				//------------------------------------Obsluga eventow w glownym menu-----------------------------------------
				while(_window->pollEvent(event)){

					if(!handleKeyEvent(event) ) _desktop.HandleEvent(event);

				}
				//------------------------------------Rysowanie ----------------------------------------
				_window->clear();

				_window->setView(_window->getDefaultView());
				_window->draw(_background);
				sf::Sprite logo;
				logo.setTexture(textures[34]);
				logo.setPosition(sf::Vector2f(50, 50));
				_window->draw(logo);

				sf::Sprite menu;
				menu.setTexture(textures[35]);
				menu.setPosition(sf::Vector2f(50, 190));

				if(_gui != nullptr) {
					if(_gui->GetId().compare("mainMenu") == 0) {
						_window->draw(menu);
					}
					_window->draw(*_gui);
				}
				_window->display();
			}

		}
		catch(std::exception& e) {
			if(_window->isOpen()) {
				_window->close();
			}
			std::cout << "Exception caught in MetaGame::metaLoop(): " << e.what() << "Press any key to close." << std::endl;
			std::cin.get();
		}
}


void MetaGame::gameLoop() {
	try {
		sf::Event event;

		_game->setGameState(GAME);

		//dlaczego std::make_shared: http://stackoverflow.com/questions/14836691/is-it-better-to-use-shared-ptr-reset-or-operator/14837300#14837300
		_layers.push_back(std::make_shared<GroundLayer>(_game));
		_layers.push_back(std::make_shared<FogOfWarLayer>(_game));
		_layers.push_back(std::make_shared<SidebarLayer>(_game));
		_layers.push_back(std::make_shared<DialogLayer>(_game));
		_layers.push_back(std::make_shared<TownLayer>(_game));
		_layers.push_back(std::make_shared<TopLayer>(_game));
		_layers.push_back(std::make_shared<MetaLayer>(_game));

		sf::Clock clock;
		sf::Time lastUpdate = sf::Time::Zero;
		sf::Time timePerFrame = sf::seconds(1.f / 120.f);

		while(_window->isOpen())
		{
			_window->setView(_game->getConfig().boardView);

			lastUpdate += clock.restart();

			while(_window->pollEvent(event))
			{
				for(auto i = _layers.rbegin(); i != _layers.rend(); ++i) {
					if((*i) && !(*i)->processEvent(event)) {
						break;
					}
				}
			}

			while(lastUpdate > timePerFrame)
			{
				lastUpdate -= timePerFrame;

				for(auto &i : _layers) {
					if(*i) {
						i->update(timePerFrame);
					}
				}
			}

			if(_game->getGameState()==META) _layers.back()->SetIsActive(true);

			_window->clear();
			for(auto &i : _layers) {
				if(*i) {
					_window->draw(*i);
				}
			}

			_window->display();
			sf::sleep(sf::microseconds(50));
			int whowon =_game->isEnd();
			if(whowon>=0) {
				_winner = whowon;
				//_window->close();
				break;

			}
		}
		_layers.clear();
		if(_game.unique())
			_game = nullptr;//dokladnie to samo co _game.reset();
	}
	catch (std::ifstream::failure& e)
	{
		std::cout << "Exception reading file\n";
	}
	catch(luabind::error& e)
	{
		luabind::object error_msg(luabind::from_stack(e.state(), -1));
		std::cout << error_msg << " error in luabind; what(): " << e.what() << "\n";
	}
	catch(std::exception& e)
	{
		if(_window->isOpen()) {
			_window->close();
		}
		std::cout << "Exception caught in MetaGame::gameLoop(): " << e.what() << ". Press any key to close." << std::endl;
		std::cin.get();
	}
}

void MetaGame::guiSetPosition() {
	_gui->SetPosition(
		sf::Vector2f(
			_window->getSize().x / 2.f - _gui->GetAllocation().width / 2.f,
			_window->getSize().y / 2.f - _gui->GetAllocation().height / 2.f
		)
	);
}



void MetaGame::createMenu(){
	_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND);
	_gui->SetId("mainMenu");

		auto out = sfg::Button::Create("Esc -Exit");
		auto qg = sfg::Button::Create("F1 - Single Player");
		auto hmp = sfg::Button::Create("F2 - Hotseat Multi Player");
		auto ca = sfg::Button::Create("F3 - Campaign");
		
		auto options = sfg::Button::Create("F3 - Settings");
		auto net = sfg::Button::Create("F5 - Network Game");
		out->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::exit, this));
		qg->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::createSelectPlayers, this, true,false));
		ca->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::createSelectPlayers, this, true,true));
		hmp->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::createSelectPlayers, this, false,false));
		net->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::networkMenu, this));
		options->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::options, this));
//		nmp->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::nmpgame, this));

		auto box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

		box->Pack(qg, false);
		box->Pack(hmp,false);
		box->Pack(ca,false);
		box->Pack(net, false);
		box->Pack(options,false);
		box->Pack(out, false);

		_gui->Add(box);
		_gui->SetPosition(sf::Vector2f(
			50,
			180
		));
		_gui->SetAllocation(sf::FloatRect(
			40,
			180,
			220,
			_gui->GetRequisition().y
		));

		_desktop.RemoveAll();
		_desktop.Add(_gui);
}


int MetaGame::getMaximumNumberOfPlayersFromJSON(const std::string &mapa){
	Json::Value root;
	Json::Reader reader;
	bool parsingSuccessful = reader.parse(mapa, root );
	if ( !parsingSuccessful )
		throw std::runtime_error("Failed to parse configuration\n" + reader.getFormattedErrorMessages());

	return root["town_count"].asInt();
}

void MetaGame::loadMap(){
	if(_mapName->GetText()!="random"){
		try{
			mapa = Game::prepareJSONFile("Resources/maps/"+_mapName->GetText());
			int maxPlayers = getMaximumNumberOfPlayersFromJSON(mapa);
			_mapLoadEffect->SetText("Max Players: " +toString(maxPlayers) );
			//Jakby ktos sie krzywi� na t� p�tle to nie mo�na u�yc fajniejszej iteracji
			for(int i=0;i<maxPlayers;++i){
				_raceBoxes.at(_raceBoxes.size()-1-i)->Show(false);
			}
		}
		catch(...){
			_mapLoadEffect->SetText("Error");
		}
	}
}

void MetaGame::getRacesList(){
	std::string rasy = Game::prepareJSONFile("Resources/scripts/races.json");
	Json::Value root, key;
	std::vector<std::pair<int, std::string>> vrasy;
	Json::Reader reader;

	if(!reader.parse(rasy, root)) {
		throw std::runtime_error("Failed to parse configuration\n" + reader.getFormattedErrorMessages());
	}

	for(unsigned int i = 0; i < root.size(); ++i) {
		vrasy.push_back(std::pair<int, std::string>(root[i]["id"].asInt(), root[i]["name"].asString()));
	}

	_playerRaces = vrasy;
}

std::set<std::string> MetaGame::listFiles(std::string directory) {
    std::set <std::string> files;
    struct dirent * file;
    DIR * dir;

    if(( dir = opendir( directory.c_str() ) ) ) {

        while( ( file = readdir(dir) ) ) {
            files.insert( file->d_name );
        }

        closedir( dir );
    }

    ///\ deleting ".." and "."
    files.erase(".");
    files.erase("..");

    return files;
}

void MetaGame::networkMenu() {
	clearMenu();
	_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND);

	auto server = sfg::Button::Create("Server");
	auto client = sfg::Button::Create("Client");
	auto cancel = sfg::Button::Create("Cancel");

	server->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::serverGame, this));
	client->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::clientGame, this));
	cancel->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::clearMenu, this));

	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
	auto label = sfg::Label::Create("Start game as:");

	box->Pack(label);
	box->Pack(server);
	box->Pack(client);
	box->Pack(cancel);

	_gui->Add(box);
	guiSetPosition();

	_desktop.Add(_gui);
}

void MetaGame::serverGame() {
	clearMenu();
	_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND);

	auto spin = sfg::SpinButton::Create(0, 7, 1);
	auto label = sfg::Label::Create("Number of players: ");
	auto ok = sfg::Button::Create("OK");
	auto cancel = sfg::Button::Create("Cancel");
	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);

	cancel->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::clearMenu, this));

	box->Pack(label);
	box->Pack(spin);
	box->Pack(ok);
	box->Pack(cancel);

	_gui->Add(box);
	guiSetPosition();

	_desktop.Add(_gui);

}

void MetaGame::clientGame() {
	clearMenu();
	_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND);

	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
	auto label = sfg::Label::Create("Insert server IP address:");
	auto ip = sfg::Entry::Create("127.0.0.1");
	auto ok = sfg::Button::Create("OK");
	auto cancel = sfg::Button::Create("Cancel");

	cancel->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::clearMenu, this));

	box->Pack(label);
	box->Pack(ip);
	box->Pack(ok);
	box->Pack(cancel);

	_gui->Add(box);
	guiSetPosition();
	_desktop.Add(_gui);

	// ip->GetText();
}


void MetaGame::serverConnectPlayers() {


}

void MetaGame::createSelectPlayers(bool single, bool ca){
	clearMenu();
	campa=ca;
	_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND);
	_mapLoadEffect=sfg::Label::Create();
	getRacesList();

	auto ok = sfg::Button::Create("Start game");
	auto menu = sfg::Button::Create("Back to menu");

	_availableMaps = sfg::ComboBox::Create();
	auto maps = listFiles("./Resources/maps/");

	_availableMaps->AppendItem("Choose map:");
	for(auto i : maps) {
		try {
			if(!ca){
				std::string tmpmap =getMapData(i, "name").asString();
				if(tmpmap.substr(0,2).compare("_C")!=0 ) _availableMaps->AppendItem(getMapData(i, "name").asString());

			}
			else {
				std::string tmpmap =getMapData(i, "name").asString();
				if(tmpmap.substr(0,2).compare("_C")==0 && stringTo<int>(tmpmap.substr(2,2)) <= _lastWonCampaign)
				{
					_availableMaps->AppendItem(getMapData(i, "name").asString());
				}
			}
		}
		catch(...) {
			std::cout << "Mapa " << i << " jest niepoprawna." << std::endl;
		}
	}

	_availableMaps->SelectItem(0);

	auto box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	if(single) {
		ok->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::qgame, this));
	}
	else {
		ok->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::hotseat, this));
	}

	menu->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::clearMenu, this));
	_availableMaps->GetSignal(sfg::ComboBox::OnSelect).Connect(std::bind(&MetaGame::createNewGameTable, this, single));

	for(int i = 0; i < 8; ++i) {
		_playerGui.push_back(std::make_tuple(
			sfg::Entry::Create("Player"),
			sfg::ComboBox::Create(),
			sfg::CheckButton::Create("")
		));

		for(auto j : _playerRaces) {
			std::get<1>(_playerGui[i])->AppendItem(j.second);
		}

		std::get<0>(_playerGui[i])->SetRequisition(sf::Vector2f(150, 0));
		std::get<1>(_playerGui[i])->SelectItem(0);
		std::get<0>(_playerGui[i])->Show(false);
		std::get<1>(_playerGui[i])->Show(false);
		std::get<2>(_playerGui[i])->Show(false);
	}

	box->Pack(_availableMaps);

	auto table = sfg::Table::Create();
	table->SetColumnSpacings(5);

	for(int i = 0; i < 8; ++i) {
		table->Attach(std::get<0>(_playerGui[i]), sf::Rect<sf::Uint32>( 0, i, 1, 1 ), sfg::Table::FILL, sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ));
		table->Attach(std::get<1>(_playerGui[i]), sf::Rect<sf::Uint32>( 1, i, 1, 1 ), sfg::Table::FILL, sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ));
		table->Attach(std::get<2>(_playerGui[i]), sf::Rect<sf::Uint32>( 2, i, 1, 1 ), sfg::Table::FILL, sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ));
	}

	box->Pack(table, false);

	box->Pack(ok,false);
	box->Pack(menu,false);

	_gui->Add(box);
	guiSetPosition();
	_desktop.Add(_gui);
}

void MetaGame::createNewGameTable(bool single) {
	auto maps = listFiles("./Resources/maps/");
	int mapNo = _availableMaps->GetSelectedItem() - 1,
		j = 0,
		maxPlayers = 0;
	std::string path;


	if(mapNo >= 0) {
		for(auto m : maps) {
			if(j == mapNo) {
				path = m;
				break;
			}
			++j;
		}

		maxPlayers = getMaximumNumberOfPlayersFromJSON(Game::prepareJSONFile("./Resources/maps/" + path));
	}

	int max = 8;

	for(int i = 0; i < max; ++i) {
		bool cond = i < maxPlayers;
		std::get<0>(_playerGui[i])->Show(cond);
		std::get<1>(_playerGui[i])->Show(cond);
		std::get<2>(_playerGui[i])->Show(cond && !single);

		if(!cond) {
			std::get<0>(_playerGui[i])->SetText("Player");
		}
	}

}

Json::Value MetaGame::getMapData(std::string path, std::string key) {
	std::string map = Game::prepareJSONFile("Resources/maps/" + path);
	Json::Value root;
	Json::Reader reader;

	if(!reader.parse(map, root)) {
		throw std::runtime_error("Failed to parse configuration\n" + reader.getFormattedErrorMessages());
	}
	return root[key];
}

void MetaGame::OnComboSelect(){
	_rozdzielczosc = static_cast<std::string>(_resolution->GetSelectedText() );

}


void MetaGame::options(){
	clearMenu();
		_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND);
		_resolution =sfg::ComboBox::Create();
		auto vGraphics = sf::VideoMode::getFullscreenModes();

		for(auto iv: vGraphics){
		if(iv.isValid() && iv.width>799 && iv.width<1440) _resolution->AppendItem(toString(iv.width)+"x"+toString(iv.height));
		}

		auto box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );
		_resolution->GetSignal( sfg::ComboBox::OnSelect ).Connect( std::bind( &MetaGame::OnComboSelect, this ) );
		box->Pack(_resolution,false);
		//auto box2 =  sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 5.0f );
		auto ok = sfg::Button::Create("OK");
		auto exit = sfg::Button::Create("EXIT");
		box->Pack(ok,false);
		box->Pack(exit,false);

		exit->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::clearMenu, this));

		ok->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&MetaGame::setResolution, this));
		_gui->Add(box);
		guiSetPosition();
		_desktop.Add(_gui);
}
void MetaGame::setResolution(){
state=RESOLUTION;

}

void MetaGame::clearMenu(){
	_desktop.RemoveAll();
	_availableMaps = nullptr;
	_resolution=nullptr;
	_mapLoadEffect=nullptr;
		_mapName=nullptr;
		while(_playerGui.size()!=0){
			_playerGui.pop_back();
		}

		while(_raceBoxes.size()!=0){
			_raceBoxes.pop_back();
						}
	_gui=nullptr;
}
void MetaGame::updateCfg(){
	std::fstream plik;
	plik.open("config.txt", std::ios::trunc | std::ios::out);
	if(plik.good()){
		plik<<"Rozdzielczosc: "<<_rozdzielczosc<<std::endl;
		plik<<"Kampania: "<<_lastWonCampaign;
				plik.close();
	}
}

void MetaGame::getCfg(){
	try{

		std::fstream plik;
		plik.open("config.txt", std::ios::in | std::ios::out);
		if (!plik.good()){
			plik.close();
			throw(0);
		}
		std::string resw,resh,dumm;
		plik>>dumm;
		if(dumm.compare("Rozdzielczosc:")!=0){
			plik.close();
			throw (0);
		}
		plik>>_rozdzielczosc;
		plik>>dumm;
				if(dumm.compare("Kampania:")!=0){
					plik.close();
					throw (0);
				}
				plik>>_lastWonCampaign;

	}catch(...){
		_rozdzielczosc="800x600";
		_lastWonCampaign=0;
		updateCfg();

	}
}

bool MetaGame::handleKeyEvent(sf::Event &e){
	if(e.type!=sf::Event::KeyPressed) return false;
	else {
		if(e.key.code==sf::Keyboard::Escape) exit();
		if(e.key.code>=sf::Keyboard::F1 && e.key.code<=sf::Keyboard::F15){
			switch (e.key.code){
			case sf::Keyboard::F1 : qgame(); break;
			case sf::Keyboard::F2 : createSelectPlayers(); break;
			case sf::Keyboard::F3 : qgame(); break;
			case sf::Keyboard::F4 : load(); break;
			default: break;
			}
			return true;
		}
		else return false;
	}
}

void MetaGame::loadResources()
{
	textures.loadByJSON("Resources/scripts/textures.json");
}
