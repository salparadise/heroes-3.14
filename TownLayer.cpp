#include "Headers/TownLayer.hpp"

TownLayer::TownLayer(std::shared_ptr<Game> g):Layer(g) {
	_isActive = true;
	_drawBuffer = std::make_shared<sf::RenderTexture>();
	_drawBuffer->create(_game->getConfig().boardSize.x * _game->getConfig().fieldSize, _game->getConfig().boardSize.y * _game->getConfig().fieldSize);

	sf::Vector2i offset;
	sf::Vector2u windowSize = _game->getConfig().window->getSize();

	offset.x = (windowSize.x - 800) / 2;
	offset.y = (windowSize.y - 600) / 2;

	for(auto i : _game->getPlayers()) {
		for(auto j : i->getTownsVector()) {
			j->initDefense();
			j->setOffset(offset);
		}
	}
}

bool TownLayer::processEvent(sf::Event &e) {
	if(!_isActive || _town == nullptr) {
		return true;
	}

	if(_gui != nullptr) {
		_game->getConfig().desktop->HandleEvent(e);
		return false;
	}

	sf::Vector2i click;
	auto config = _game->getConfig();
	auto building = _town->getActiveBuilding();
	auto buildings = _town->getBuildings();

	switch(e.type) {
	case sf::Event::MouseButtonPressed:
		click.x = e.mouseButton.x;
		click.y = e.mouseButton.y;

		for(auto i : buildings) {
			if(click.x >= i->area.left && click.x <= i->area.left + i->area.width && click.y >= i->area.top && click.y <= i->area.top + i->area.height) {
				//kliknieto na ten budynek
				_town->setActiveBuilding(i);
				_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR);
				auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
				auto close = sfg::Button::Create("Close");
				close->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&TownLayer::closeCurrent, this));

				if(i->active) {
					switch(i->type) {
					case Town::BARRACKS:
					{
						_gui->SetTitle("Barracks");
						auto label = sfg::Label::Create("Do you want to recruit some troops?\nCost:");
						int costs[] = { 15, 30, 45, 60, 90 };
						int cost = costs[i->id - 1];

						if(_town->hasBank()) {
							cost = cost * 0.9;
						}

						auto table = sfg::Table::Create();
						auto th1 = sfg::Label::Create("Gold: "),
							 td1 = sfg::Label::Create(toString(cost));

						table->Attach( th1, sf::Rect<sf::Uint32>( 0, 1, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );
						table->Attach( td1, sf::Rect<sf::Uint32>( 1, 1, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );

						box->Pack(label, true);
						box->Pack(table, true);
						_range = sfg::SpinButton::Create(0, _game->getCurrentPlayer()->getGold() / cost, 1);
						box->Pack(_range, true);

						auto ok = sfg::Button::Create("We want them to army!");
						ok->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&TownLayer::buyTroops, this));
						box->Pack(ok, true);
					}
						break;

					case Town::STONEMASON:
					{
						_gui->SetTitle("Stonemason");
						auto label = sfg::Label::Create("Do you want to improve walls?");

						auto table = sfg::Table::Create();
						auto th1 = sfg::Label::Create("Gold: "),
							 td1 = sfg::Label::Create(toString(10));

						table->Attach( th1, sf::Rect<sf::Uint32>( 0, 1, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );
						table->Attach( td1, sf::Rect<sf::Uint32>( 1, 1, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );

						auto th2 = sfg::Label::Create("Wood: "),
							 td2 = sfg::Label::Create(toString(10));

						table->Attach( th2, sf::Rect<sf::Uint32>( 0, 2, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );
						table->Attach( td2, sf::Rect<sf::Uint32>( 1, 2, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );

						auto th3 = sfg::Label::Create("Stone: "),
							 td3 = sfg::Label::Create(toString(25));
						table->Attach( th3, sf::Rect<sf::Uint32>( 0, 3, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );
						table->Attach( td3, sf::Rect<sf::Uint32>( 1, 3, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );

						box->Pack(label, true);
						box->Pack(table, false);

						auto th4 = sfg::Label::Create("Number of defenders: "),
							 td4 = sfg::Label::Create(toString(_town->getArmy()->getCreatureAmount(1)));
						table->Attach( th4, sf::Rect<sf::Uint32>( 0, 4, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );
						table->Attach( td4, sf::Rect<sf::Uint32>( 1, 4, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );

						box->Pack(label, true);
						box->Pack(table, false);

						auto ok = sfg::Button::Create("Improve");
						ok->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&TownLayer::improveWalls, this));
						box->Pack(ok, true);
					}
						break;

					case Town::TOWNHALL:
					{
						_gui->SetTitle("Town Hall");
						auto label = sfg::Label::Create("Your troops are now upgraded.\nNew forms of training produce better results.\nTroops recruited in barracks are stronger and faster then before.");

						box->Pack(label, true);
					}
						break;

					case Town::BANK:
					{
						_gui->SetTitle("Bank");
						auto label = sfg::Label::Create("Welcome to City Bank.\nYour troops are now 10% cheaper.\n(But of course still excellent trained)");
						box->Pack(label, true);
					}
						break;

					case Town::MILL:
					{
						_gui->SetTitle("Warehouse");
						auto label = sfg::Label::Create("You've already trained cooks.\nYour food income is now 20 units higher!");
						box->Pack(label, true);
					}
						break;

					case Town::CLOSE:
					{
						_gui = nullptr;
						_town = nullptr;
						//_background->SetState(sfg::Widget::State::NORMAL);
						_game->getConfig().desktop->Remove(_background);
						_background = nullptr;

						_game->getConfig().desktop->Remove(_whiteBoard);
						_whiteBoard = nullptr;
					}
						break;
					}
				}
				else {
					// trzeba zrobic zakupy
					_gui->SetTitle("Building unavailable.");

					auto ok = sfg::Button::Create("Buy it!");
					ok->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&TownLayer::closeCurrent, this));
					ok->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&TownLayer::buyBuilding, this));
					auto label = sfg::Label::Create("Building must be bought first.");

					auto table = sfg::Table::Create();
					auto th1 = sfg::Label::Create("Gold: "),
						 td1 = sfg::Label::Create(toString(i->costGold));

					table->Attach( th1, sf::Rect<sf::Uint32>( 0, 1, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );
					table->Attach( td1, sf::Rect<sf::Uint32>( 1, 1, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );

					auto th2 = sfg::Label::Create("Wood: "),
						 td2 = sfg::Label::Create(toString(i->costWood));

					table->Attach( th2, sf::Rect<sf::Uint32>( 0, 2, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );
					table->Attach( td2, sf::Rect<sf::Uint32>( 1, 2, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );

					auto th3 = sfg::Label::Create("Stone: "),
						 td3 = sfg::Label::Create(toString(i->costStone));
					table->Attach( th3, sf::Rect<sf::Uint32>( 0, 3, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );
					table->Attach( td3, sf::Rect<sf::Uint32>( 1, 3, 1, 1 ), sfg::Table::FILL , sfg::Table::FILL, sf::Vector2f( 5.f, 5.f ) );

					box->Pack(label, true);
					box->Pack(table, false);
					box->Pack(ok, true);
				}

				if(_gui != nullptr) {
					box->Pack(close, true);
					_gui->SetAllocation(sf::FloatRect(200, 200, 200, 200));
					_gui->Add(box);
					centerWindowTown(_gui);
					_game->getConfig().desktop->Add(_gui);
				}
			}
		}

		break;

	default:

		break;
	}

	return false;
}

void TownLayer::draw(sf::RenderTarget &window, sf::RenderStates states) const {
	if(_isActive && _town != nullptr) {
		window.setView(_game->getConfig().mainView);
		_drawBuffer->clear(sf::Color::White);

		sf::RectangleShape rect;
		rect.setFillColor(sf::Color::White);
		rect.setPosition(0, 0);
		rect.setSize(_game->getConfig().mainView.getSize());
		_drawBuffer->draw(rect);

		_drawBuffer->display();
		sf::Sprite board(_drawBuffer->getTexture());
		window.draw(board);

		_game->getConfig().sfgui->Display(*(_game->getConfig().window));
	}
}

void TownLayer::update(sf::Time dt) {
	_game->getConfig().desktop->Update(0.f);

	if(_town == nullptr) {
		for(auto i : _game->getPlayers()) {
			for(auto j : i->getTownsVector()) {
				std::shared_ptr<Town> town = std::dynamic_pointer_cast<Town>(j);

				if(town != nullptr) {
					if(town->isSelected()) {
						_isActive = true;
						_town = town;
						_town->deselect();

						_whiteBoard = sfg::Window::Create(sfg::Window::Style::BACKGROUND);
						_whiteBoard->SetId("townBg");
						_whiteBoard->SetAllocation(sf::FloatRect(0, 40, _game->getConfig().window->getSize().x, _game->getConfig().window->getSize().y));
						_whiteBoard->SetState(sfg::Widget::State::INSENSITIVE);
						_whiteBoard->SetZOrder(2);
						_game->getConfig().desktop->Add(_whiteBoard);

						_background = sfg::Image::Create(_game->textures[21].copyToImage());
						_background->SetState(sfg::Widget::State::INSENSITIVE);
						_background->SetAllocation(sf::FloatRect(0, 40, _game->getConfig().window->getSize().x, _game->getConfig().window->getSize().y - 40));
						_background->SetZOrder(1);
						_game->getConfig().desktop->Add(_background);
					}
				}
			}
		}
	}
}

void TownLayer::improveWalls() {
	Layer::closeCurrent();

	int costGold = 10,
		costWood = 10,
		costStone = 25;

	auto player = _game->getCurrentPlayer();
	_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR);
	_gui->SetTitle("Improve defense");
	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
	auto close = sfg::Button::Create("Close");
	close->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&TownLayer::closeCurrent, this));
	auto label = sfg::Label::Create();

	if(costGold <= player->getGold() && costWood <= player->getWood() && costStone <= player->getStone()) {
		_town->getArmy()->updateCreature(1, 10);
		label->SetText("Walls improved. Town is now protected by more defenders.");
	}
	else {
		label->SetText("More resources required to improve defense.");
	}

	box->Pack(label, true);
	box->Pack(close, true);

	_gui->SetAllocation(sf::FloatRect(200, 200, 200, 200));
	_gui->Add(box);
	centerWindowTown(_gui);
	_game->getConfig().desktop->Add(_gui);

	_town->setActiveBuilding(nullptr);
}

void TownLayer::buyTroops() {
	Layer::closeCurrent();

	int cost[] = { 15, 30, 45, 60, 90 };
	auto building = _town->getActiveBuilding();
	int number = (int)_range->GetValue(),
		totalCost = number * cost[building->id - 1];

	if(_town->hasBank()) {
		totalCost = totalCost * 0.9;
	}

	if(number > 0) {
		_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR);
		_gui->SetTitle("Recruitment");
		auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
		auto close = sfg::Button::Create("Close");
		close->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&TownLayer::closeCurrent, this));
		auto label = sfg::Label::Create();

		if(totalCost > _game->getCurrentPlayer()->getGold()) {
			// Nie ma wystarczajacych srodkow
			label->SetText("More resources required to recruit troops.");
		}
		else {
			// Kup kup kup
			auto field = _game->getField(_town->getCurrentField()->getPosition().x / _game->getConfig().fieldSize, _town->getCurrentField()->getPosition().y / _game->getConfig().fieldSize);
			int upgrade = (_town->hasTownHall() ? 5 : 0);

			if(field->isFreeArmy()) {
				auto army = std::make_shared<Army>(_game->getCurrentPlayer(), field, 0);
				army->updateCreature(building->id + upgrade, number);
				if(!_town->getOwner()->isHumanPlayer())
					if(luabind::type(_game->getLuabindStates()) == LUA_TTABLE)
						army->getFSM()->setStartState(_game->getLuabindStates()["State_Wander"]);
				_game->getCurrentPlayer()->addArmy(army);
				field->addArmy(army);
				label->SetText("New army is waiting for your orders!");
			}
			else {
				field->getArmy()->updateCreature(building->id + upgrade, number);
				label->SetText("Units added to your army!");
			}

			_game->getCurrentPlayer()->updateGold(-totalCost);
		}

		box->Pack(label, true);
		box->Pack(close, true);

		_gui->SetAllocation(sf::FloatRect(200, 200, 200, 200));
		_gui->Add(box);
		centerWindowTown(_gui);
		_game->getConfig().desktop->Add(_gui);
	}
	else {
		_gui = nullptr;
	}

	_town->setActiveBuilding(nullptr);
	_range = nullptr;
}

void TownLayer::buyBuilding() {
	Layer::closeCurrent();

	auto building = _town->getActiveBuilding();
	int costGold = building->costGold,
		costWood = building->costWood,
		costStone = building->costStone;
	auto player = _game->getCurrentPlayer();

	_gui = sfg::Window::Create(sfg::Window::Style::BACKGROUND | sfg::Window::Style::TITLEBAR);
	_gui->SetTitle("Building");
	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);
	auto close = sfg::Button::Create("Close");
	close->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&TownLayer::closeCurrent, this));
	auto label = sfg::Label::Create();

	if(costGold <= player->getGold() && costWood <= player->getWood() && costStone <= player->getStone()) {
		player->updateGold(-costGold);
		player->updateWood(-costWood);
		player->updateStone(-costStone);
		building->active = true;

		label->SetText("Building bought. You can use it now.");
	}
	else {
		label->SetText("More resources required to buy this building.");
	}

	box->Pack(label, true);
	box->Pack(close, true);

	_gui->SetAllocation(sf::FloatRect(200, 200, 200, 200));
	_gui->Add(box);
	centerWindowTown(_gui);
	_game->getConfig().desktop->Add(_gui);

	_town->setActiveBuilding(nullptr);
}

void TownLayer::centerWindowTown(sfg::Window::Ptr w) {
	centerWindow(w, _game->getConfig().window);
	sf::Vector2f pos = w->GetAbsolutePosition();
	pos.x += 100;
	w->SetPosition(pos);
}

void TownLayer::closeCurrent() {
	Layer::closeCurrent();

	_range = nullptr;
	_gui = nullptr;
}
