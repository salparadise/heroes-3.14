#include "Headers/Race.hpp"

 void Race::setCreature(std::shared_ptr<Creature> creature){
 	_creatures.push_back(creature);
 }

  void Race::setHero(std::shared_ptr<Hero> hero){
 	_heroes.push_back(hero);
 }


 std::shared_ptr<Creature> Race::getCreature(int lv){
 	for(int i=0; i<_creatures.size(); i++){
		if(_creatures[i]!=nullptr && _creatures[i]->getLevel()==lv) return _creatures[i];

	}
 	return nullptr;
 }

 void Race::printRace(){

 }

Race::Race(std::string raceName, int id){
	_name=raceName;
	_id=id;
}
