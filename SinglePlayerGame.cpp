/**
 * @file SinglePlayerGame.cpp
 *
 * @date 9 sty 2014
 * @author Wojtek
 * @version 1.0
 *
 */

#include "Headers/SinglePlayerGame.hpp"


SinglePlayerGame::SinglePlayerGame(
		std::vector<std::tuple<std::string, int, bool, sf::Vector2i>> gracze,
		std::shared_ptr<sf::RenderWindow> w,
		TextureHolder& textures,
		std::string path) : Game(w, textures)  {

	loadMapFromJson(prepareJSONFile("Resources/maps/" + path));
	_mapPath=path;

	int j = 0;
	for(auto i : gracze) {
		_players.push_back(std::make_shared<Player>(std::get<0>(i), getRace(std::get<1>(i)), _players, j == 0, _neutralPlayer, states));

		///\ przypisuje mu miasto
		std::shared_ptr<Town> town = std::dynamic_pointer_cast<Town>(_board[std::get<3>(i).x][std::get<3>(i).y]->getBuilding());
		town->changeOwner(_players[j]);
		town->setTextureId(_players[j]->getRace()->getId());
		_players[j]->addTown(town);
		_neutralPlayer->removeTown(town);

		///\ oraz pseudolosowa armie skladajaca sie z jednostek 1 oraz 2 lvl
		auto army = std::make_shared<Army>(_players[j], getField(std::get<3>(i).x, std::get<3>(i).y));
		getField(std::get<3>(i).x, std::get<3>(i).y)->addArmy(army);
		army->updateCreature(1, rand() % 10 + 5);
		army->updateCreature(2, rand() % 5 + 1);
		if(!_players[j]->isHumanPlayer())
			if (luabind::type(states) == LUA_TTABLE)
				army->getFSM()->setStartState(states["State_Wander"]);
		_players[j]->addArmy(army);

		++j;
	}

	for(auto p: _players) {
		p->initiateFog(_config.boardSize.x, _config.boardSize.y);
	}

}
