#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFGUI/SFGUI.hpp>
#include <iostream>
#include "Headers/MetaGame.hpp"
#include "Headers/ResourceManager.hpp"

int main() {
	sfg::SFGUI sfgui;
	MetaGame metaGame;
	metaGame.metaLoop();
}
