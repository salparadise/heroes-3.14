#include "Headers/Farm.hpp"

Farm::Farm() {
	_textureId = 22;
	_income = 30;
}

Farm::~Farm() {

}

void Farm::updateResources() {
	if(_owner != nullptr) {
		_owner->updateFood(_income);
	}
}
