#include "Headers/Field.hpp"
#include "Headers/StringConverter.hpp"
#include <cstdlib>

Field::Field(sf::Vector2i position, TextureHolder& textures, int cost, std::shared_ptr<StaticObject> ground, std::shared_ptr<Building> building, std::shared_ptr<Army> army) :
		_position(position), _cost(cost), _ground(ground), _building(building), _army(army), _textureId(0), textures(textures) {

}

void Field::draw(sf::RenderTarget &target, sf::RenderStates states) const {
	sf::Sprite sprite;

	//tekstury terenu posiadaja indeksy 0-7, budynki 8-15, jednostki 16
	sprite.setTexture(textures[_textureId]);

	sprite.setPosition(_position.x, _position.y - 50);
	target.draw(sprite);
}

void Field::setGround(std::shared_ptr<StaticObject> ground) {
	_ground = ground;
}

void Field::setTextureId(int textureId)
{
	_textureId = textureId;
}

void Field::setBuilding(std::shared_ptr<Building> building) {
	_building = building;
}

void Field::setPosition(sf::Vector2i position) {
	_position = position;
}

void Field::addArmy(const std::shared_ptr<Army>& army) {
	_army = army;
}

bool Field::isFree() const {
	return (_building == nullptr && _army == nullptr);
}

bool Field::isFreeBuilding() const {
	return (_building == nullptr);
}

void Field::removeArmy() {
	_army = nullptr;
}

sf::Vector2i Field::getPosition() const {
	return (_position);
}

bool Field::isFreeArmy() const {
	return (_army == nullptr);
}

std::shared_ptr<Army> Field::getArmy() {
	return _army;
}

void Field::setGroundColor(const std::string& hexColor)
{
	int hex;
	hex = strtol(hexColor.c_str(), nullptr, 16);
	_groundColor.r = (hex >> 16) & 0xff;
	_groundColor.g = (hex >> 8) & 0xff;
	_groundColor.b = (hex) & 0xff;
	_groundColor.a = 1.0;

//	std::cout << "hexString: " << std::hex << hexColor << ", hex: "<< hex << "; red: " << ((hex >> 16) & 0xff) << ", green: " << std::hex << ((hex >> 8) & 0xff) << ", blue: " << ((hex) & 0xff) << std::endl;
}

sf::Color Field::getGroundColor() const
{
	return _groundColor;
}

void Field::removeBuilding() {
	_building = nullptr;
}



//void Field::removeArmy(std::shared_ptr<Army> army) {
//	std::vector<std::shared_ptr<Army> >::iterator toDel = std::find_if(_army.begin(), _army.end(), army);
//	_army.erase(toDel);
//}


