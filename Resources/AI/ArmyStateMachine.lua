-------------------------------------------------------------------------------

-- State_Wander

-------------------------------------------------------------------------------
State_Wander = {}

State_Wander["enter"] = function(army)

	math.randomseed( os.time() )
	-- print (army.id .. " Uhh im bored...")

end

State_Wander["execute"] = function(army)

	-- print (army.id .. " Walkin here and there in the hot n' thusty heat of the desert ")

	if army:enemyInRange() then -- (nie radzi sobie gdy enemyInRange() zwraca 0/NULL/nullptr); UPDATE: <luabind/shared_ptr_converter.hpp>
		-- print (army.id .. " Enemy in range!")
		army:getFSM():pushState(State_Enemy)
	elseif army:buildingInRange() then
		-- print (army.id .. " Building in range!")
		-- if not army:getArmyToTheSpottedBuilding() then --jesli zadna armia nie wybiera sie jeszcze do budynku
			army:getFSM():pushState(State_GoToBuilding)
		-- end
	elseif army:townInRange() and not army:isTownDefencesStronger(army:townInRange()) then
		-- print (army.id .. " Town in range!")
		-- if not army:getArmyToTheTown() then
			army:getFSM():pushState(State_GoToTown)
		-- end
	else
		-- print (army.id .. " Ima wanderer")
		army:wander()
	end

	-- czesc, ktora powinna nalezec do Player'a
	-- if math.random(0, 3) == 1 then
	-- 	army:buyTroops()
	-- end
	-- koniec czesci Player'a

	-- if army:getNumberOfArmies() > 1 then
	-- 	if army:myArmyInMoveRange() then
	-- 		army:mergeWithArmy(army:myArmyInMoveRange())
	-- 	end
	-- end


end

State_Wander["exit"] = function(army)

	-- print (army.id .. " Gettin' ready for an adventure!")

end

-------------------------------------------------------------------------------

-- State_Enemy

-------------------------------------------------------------------------------
State_Enemy = {}

State_Enemy["enter"] = function(army)

	-- print (army.id .. " Lookin' for some enemy..")

end

State_Enemy["execute"] = function(army)

	if army:enemyInRange() then
		if army:isEnemyStronger(army:enemyInRange()) then
			-- print (army.id .. " The Force is strong with this one. I better go.")
			army:getFSM():pushState(State_Flee)
		else
			-- print (army.id .. " The Force is NOT THAT strong with this one.")
			army:getFSM():pushState(State_GoAfter)
		end
	else
		-- print (army.id .. " No enemy in sight!")
		army:getFSM():popState()
	end

end

State_Enemy["exit"] = function(army)

	-- print (army.id .. " No enemy in sight!")

end

-------------------------------------------------------------------------------

-- State_GoAfter

-------------------------------------------------------------------------------
State_GoAfter = {}

State_GoAfter["enter"] = function(army)

	-- print (army.id .. " Lets chase some bad boys!")

end

State_GoAfter["execute"] = function(army)

	-- print("in chase")
	army:goAfter(army:enemyInRange())
	army:getFSM():popState()

end

State_GoAfter["exit"] = function(army)

	-- print (army.id .. " No enemy in sight!")

end

-------------------------------------------------------------------------------

-- State_Flee

-------------------------------------------------------------------------------
State_Flee = {}

State_Flee["enter"] = function(army)

	-- print (army.id .. " Fly, you fools!")

end

State_Flee["execute"] = function(army)

	-- print("running away")
	army:flee(army:enemyInRange())
	army:getFSM():popState()

end

State_Flee["exit"] = function(army)

	-- print (army.id .. " No enemy in sight!")

end

-------------------------------------------------------------------------------

-- State_GoToBuilding

-------------------------------------------------------------------------------
State_GoToBuilding = {}

State_GoToBuilding["enter"] = function(army)

	-- army:setArmyToTheSpottedBuilding(army)
	-- print (army.id .. " Settin' a new course - a building ahead!")

end

State_GoToBuilding["execute"] = function(army)

	if army:enemyInRange() then
		army:getFSM():pushState(State_Enemy)
	else
		if army:claimThatBuilding(army:buildingInRange()) then
			army:getFSM():popState()
		end
	end

end

State_GoToBuilding["exit"] = function(army)

	-- army:resetArmyToTheSpottedBuilding()
	-- print (army.id .. " Puttin' mah boots on n' gettin' ready for an adventure!")

end

-------------------------------------------------------------------------------

-- State_GoToTown

-------------------------------------------------------------------------------
State_GoToTown = {}

State_GoToTown["enter"] = function(army)

	-- army:setArmyToTheTown(army)
	-- print (army.id .. " Settin' a new course - a town ahead!")

end

State_GoToTown["execute"] = function(army)

	if army:enemyInRange() then
		army:getFSM():pushState(State_Enemy)
	else
		if army:claimThatTown(army:townInRange()) then
			army:getFSM():popState()
		end
	end

end

State_GoToTown["exit"] = function(army)

	-- army:resetArmyToTheTown()
	-- print (army.id .. " Puttin' mah boots on n' gettin' ready for an adventure!")

end