#include "Headers/Town.hpp"

Town::Town() : _selected(false), _foodIncome(50) {
	_textureId = 8;
	_income = 100;

	setOffset(sf::Vector2i(0, 0));

	_building = nullptr;

	_army = std::make_shared<Army>();
}

Town::~Town() {
}

std::shared_ptr<Hero> Town::newHero(int type) {
	return nullptr;
}

std::shared_ptr<Creature> Town::newCreature(int type) {
	return nullptr;
}

void Town::updateResources() {
	if(_owner != nullptr) {
		_owner->updateGold(_income);
		_owner->updateFood(_foodIncome);
	}
}

void Town::initDefense() {
	_army->updateCreature(1, 100);
}

void Town::changeOwner(std::shared_ptr<Player> newOwner) {
	_owner = newOwner;
	_army->changeOwner(newOwner);
}

void Town::setOffset(sf::Vector2i offset) {
	_offset = offset;
	_buildings.clear();

	for(int i = 0; i < 5; ++i) {
		Part tmp = {
			sf::IntRect(_offset.x + i * 160, _offset.y + 40, 120, 200),
			BARRACKS,
			i + 1,
			(i == 0),
			i * 50,
			i * 5,
			i * 5
		};

		_buildings.push_back(std::make_shared<Part>(tmp));
	}

	{
		Part tmp = {
			sf::IntRect(_offset.x, _offset.y + 240, 120, 200),
			TOWNHALL,
			6,
			false,
			500,
			50,
			50
		};
		_buildings.push_back(std::make_shared<Part>(tmp));
	}

	{
		Part tmp = {
			sf::IntRect(_offset.x + 160, _offset.y + 240, 120, 200),
			STONEMASON,
			7,
			true
		};
		_buildings.push_back(std::make_shared<Part>(tmp));
	}

	{
		Part tmp = {
			sf::IntRect(_offset.x + 320, _offset.y + 240, 120, 200),
			BANK,
			8,
			false,
			300,
			30,
			30
		};
		_buildings.push_back(std::make_shared<Part>(tmp));
	}

	{
		Part tmp = {
			sf::IntRect(_offset.x + 480, _offset.y + 240, 120, 200),
			MILL,
			9,
			false,
			300,
			30,
			30
		};
		_buildings.push_back(std::make_shared<Part>(tmp));
	}

	{
		Part tmp = {
			sf::IntRect(_offset.x + 640, _offset.y + 240, 120, 200),
			CLOSE,
			10,
			true,
			0,
			0,
			0
		};
		_buildings.push_back(std::make_shared<Part>(tmp));
	}
}

