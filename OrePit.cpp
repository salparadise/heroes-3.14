#include "Headers/OrePit.hpp"

OrePit::OrePit() {
	_textureId = 13;
	_income = 3;
}

OrePit::~OrePit() {

}

void OrePit::updateResources() {
	if(_owner != nullptr) {
		_owner->updateStone(_income);
	}
}
