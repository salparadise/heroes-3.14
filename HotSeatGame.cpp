#include "Headers/HotSeatGame.hpp"
#include "Headers/Game.hpp"

HotSeatGame::HotSeatGame(int ileGraczy, std::shared_ptr<sf::RenderWindow> w, TextureHolder& textures) : Game(w, textures)
{
//	for(int i=0; i<ileGraczy;i++)
//		_players.push_back( std::make_shared<Player>("test" + toString(i + 1), _races[i % 2 + 1], _players, true, _neutralPlayer, states) );
	loadMapFromJson(prepareJSONFile("Resources/maps/20.json"));

	_players.push_back( std::make_shared<Player>("test" + toString(1), _races[2], _players, true, _neutralPlayer, states) );
	_players.push_back( std::make_shared<Player>("test" + toString(2), _races[1], _players, false, _neutralPlayer, states) );

	for(auto p: _players)
		p->initiateFog(_config.boardSize.x,_config.boardSize.y);


	auto army1 = std::make_shared<Army>(_players[0], getField(7,3), 0);
	getField(7,3)->addArmy(army1);
	army1->setHero(std::make_shared<Hero>("Hero 1"));
	army1->updateCreature(1, 1);
	army1->updateCreature(2, 95);
	if(!_players[0]->isHumanPlayer())
		if (luabind::type(states) == LUA_TTABLE)
			army1->getFSM()->setStartState(states["State_Wander"]);
	_players[0]->addArmy(army1);

	auto army2 = std::make_shared<Army>(_players[1], getField(8,10), 5);
	getField(8,10)->addArmy(army2);
	army2->setHero(std::make_shared<Hero>("Hero 2"));
	army2->updateCreature(1, 1);
	army2->updateCreature(2, 205);
	if(!_players[1]->isHumanPlayer())
			if (luabind::type(states) == LUA_TTABLE)
				army2->getFSM()->setStartState(states["State_Wander"]);
	_players[1]->addArmy(army2);

	auto army3 = std::make_shared<Army>(_players[0], getField(8,12), 2);
	getField(8,12)->addArmy(army3);
	army3->updateCreature(1, 1);
	army3->updateCreature(2, 92);
	if(!_players[0]->isHumanPlayer())
			if (luabind::type(states) == LUA_TTABLE)
				army3->getFSM()->setStartState(states["State_Wander"]);
	_players[0]->addArmy(army3);

	auto army4 = std::make_shared<Army>(_players[0], getField(11,14), 6);
	getField(11,14)->addArmy(army4);
	army4->updateCreature(1, 3);
	army4->updateCreature(2, 92);
	if(!_players[0]->isHumanPlayer())
			if (luabind::type(states) == LUA_TTABLE)
				army4->getFSM()->setStartState(states["State_Wander"]);
	_players[0]->addArmy(army4);

	auto mill = std::make_shared<Mill>();
	mill->changeOwner(_players[0]);
	mill->setCurrentField(getField(9, 9));
	_players[0]->addBuilding(mill);
	getField(9, 9)->setBuilding(mill);

	auto town = std::make_shared<Town>();
	town->changeOwner(_players[0]);
	town->setCurrentField(getField(9, 11));
	_players[0]->addTown(town);
	getField(9, 11)->setBuilding(town);

	auto farm = std::make_shared<Farm>();
	farm->setCurrentField(getField(11, 12));
	farm->changeOwner(_neutralPlayer);
	_neutralPlayer->addBuilding(farm);
	getField(11, 12)->setBuilding(farm);

	auto orepit = std::make_shared<OrePit>();
	orepit->setCurrentField(getField(7, 7));
	orepit->changeOwner(_neutralPlayer);
	_neutralPlayer->addBuilding(orepit);
	getField(7, 7)->setBuilding(orepit);

	auto goldmine = std::make_shared<GoldMine>();
	goldmine->setCurrentField(getField(5, 7));
	goldmine->changeOwner(_neutralPlayer);
	_neutralPlayer->addBuilding(goldmine);
	getField(5, 7)->setBuilding(goldmine);

	auto sawmill = std::make_shared<Sawmill>();
	sawmill->setCurrentField(getField(5, 10));
	sawmill->changeOwner(_neutralPlayer);
	_neutralPlayer->addBuilding(sawmill);
	getField(5, 10)->setBuilding(sawmill);
};


HotSeatGame::HotSeatGame(
		std::vector<std::tuple<std::string, int, bool, sf::Vector2i>> gracze,
		std::shared_ptr<sf::RenderWindow> w,
		TextureHolder& textures,
		std::string path) : Game(w, textures)  {

	loadMapFromJson(prepareJSONFile("Resources/maps/" + path));
	_mapPath=path;

	int j = 0;
	for(auto i : gracze) {
		_players.push_back(std::make_shared<Player>(std::get<0>(i), getRace(std::get<1>(i)), _players, !std::get<2>(i), _neutralPlayer, states));

		///\ przypisuje mu miasto
		std::shared_ptr<Town> town = std::dynamic_pointer_cast<Town>(_board[std::get<3>(i).x][std::get<3>(i).y]->getBuilding());
		town->changeOwner(_players[j]);
		town->setTextureId(_players[j]->getRace()->getId());
		_players[j]->addTown(town);
		_neutralPlayer->removeTown(town);

		///\ oraz pseudolosowa armie skladajaca sie z jednostek 1 oraz 2 lvl
		auto army = std::make_shared<Army>(_players[j], getField(std::get<3>(i).x, std::get<3>(i).y));
		getField(std::get<3>(i).x, std::get<3>(i).y)->addArmy(army);
		army->updateCreature(1, rand() % 10 + 5);
		army->updateCreature(2, rand() % 5 + 1);
		if(!_players[j]->isHumanPlayer())
			if (luabind::type(states) == LUA_TTABLE)
				army->getFSM()->setStartState(states["State_Wander"]);
		_players[j]->addArmy(army);

		++j;
	}

	for(auto p : _players) {
		p->initiateFog(_config.boardSize.y, _config.boardSize.x);
	}
}
