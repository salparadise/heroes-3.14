# encoding: utf-8

class TroopsController < ApplicationController
  before_filter :get_troop, except: [:index, :new, :create]
  before_filter :get_towns, only: [:edit, :new, :create]

  def index
    @troops = Troop.includes( :town ).by_name

    respond_to do |format|
      format.html 
      format.json { send_data @troops.to_json, type: :json, disposition: 'attachment' }
    end      
  end

  def new
    @troop = Troop.new
  end

  def create
    @troop = Troop.create( troop_params )
    
    if @troop.save
      redirect_to troops_path, notice: 'Jednostka została dodana'
    else
      render 'new'
    end
  end

  def edit
  
  end

  def update
    @troop.update( troop_params )
    
    redirect_to troops_path, notice: 'Jednostka została zaktualizowana'
  end

  def destroy  
    @troop.destroy

    respond_to do |format|
      format.js
    end
  end

  private

  def get_towns
    @towns = Town.by_name
  end

  def get_troop
    @troop = Troop.find( params[:id] )
  end

  def troop_params
    params.require( :troop ).permit( 
      [:abilities, :cost, :level, :max_health, :max_damage, :min_damage, :name, :range, :town_id, :troop_type_id, :upgrade ] 
    )
  end  

end
