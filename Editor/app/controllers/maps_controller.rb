# encoding: utf-8

class MapsController < ApplicationController
  before_filter :get_map,    except: [:fields, :index, :new, :create]
  before_filter :get_fields, only:   [:fields]

  def index
    @maps = Map.by_name

    respond_to do |format|
      format.html 
      format.json { send_data @maps.to_json, type: :json, disposition: 'attachment' }
    end
  end

  def show
    respond_to do |format|
      format.json { send_data @map.to_json, type: :json, disposition: 'attachment' }
    end
  end

  def new
    @map = Map.new
  end

  def create
    @map = Map.new( map_params )
    
    if @map.save
      redirect_to maps_path, notice: 'Mapa została dodana'
    else
      render 'new'
    end    
  end

  def edit

  end

  def update
    @map.update( map_update_params )
    
    redirect_to maps_path, notice: 'Mapa została zaktualizowana'
  end

  def destroy
    @map.destroy

    respond_to do |format|
      format.js
    end
  end

  def fields
    @map = Map.find( params[:id] )

    @fields = @map.fields.includes( :ground ).order( 'y asc, x asc' )
  end

  def randomize
    @map.randomize

    redirect_to fields_map_path( @map )
  end

  private

  def get_map
    @map = Map.find( params[:id] )
  end

  def get_fields
    @grounds = Ground.by_name
    @mines   = Mine.by_name
    @towns   = Town.by_name
  end

  def map_params
    params.require( :map ).permit( [:is_random, :name, :victory_condition, :defeat_condition, :size, :town_count] )
  end 

  def map_update_params
    params.require( :map ).permit( [:name, :victory_condition, :defeat_condition] )
  end     
end
