# encoding: utf-8

class TroopTypesController < ApplicationController
  before_filter :get_type, except: [:index, :new, :create]

  def index
    @types = TroopType.includes( :troops ).by_name

    respond_to do |format|
      format.html
      format.json { send_data @types.to_json, type: :json, disposition: 'attachment' }
    end
  end

  def new
    @type = TroopType.new
  end

  def create
    @type = TroopType.create( type_params )
    
    if @type.save
      redirect_to troop_types_path, notice: 'Typ jednostki został dodany'
    else
      render 'new'
    end    
  end

  def edit

  end

  def update
    @type.update( type_params )
    
    redirect_to troop_types_path, notice: 'Typ został zaktualizowany'
  end

  def destroy    
    @type.destroy

    respond_to do |format|
      format.js
    end
  end

  private

  def get_type
    @type = TroopType.find( params[:id] )
  end

  def type_params
    params.require( :troop_type ).permit( [:flying, :magic, :name, :shooting] )
  end  

end
