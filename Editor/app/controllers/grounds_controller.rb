# encoding: utf-8

class GroundsController < ApplicationController
  before_filter :get_ground, except: [:index, :new, :create]

  def index
    @grounds = Ground.by_name

    respond_to do |format|
      format.html 
      format.json { send_data @grounds.to_json, type: :json, disposition: 'attachment' }
    end
  end

  def new
    @ground = Ground.new
  end

  def create
    @ground = Ground.new( ground_params )
    
    if @ground.save
      redirect_to grounds_path, notice: 'Powierzchnia została dodana'
    else
      render 'new'
    end    
  end

  def edit

  end

  def update
    @ground.update( ground_params )
    
    redirect_to grounds_path, notice: 'Powierzchnia została zaktualizowana'
  end

  def destroy
    @ground.destroy

    respond_to do |format|
      format.js
    end
  end

  private

  def get_ground
    @ground = Ground.find( params[:id] )
  end

  def ground_params
    params.require( :ground ).permit( [:color, :cost, :name, :probability, :sprite] )
  end  
end
