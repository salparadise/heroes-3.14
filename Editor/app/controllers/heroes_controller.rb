# encoding: utf-8

class HeroesController < ApplicationController
  before_filter :get_hero, except: [:index, :new, :create]

  def index
    @heroes = Hero.by_name

    respond_to do |format|
      format.html 
      format.json { send_data @heroes.to_json, type: :json, disposition: 'attachment' }
    end    
  end

  def new
    @hero = Hero.new
  end

  def create
    @hero = Hero.new( hero_params )
    
    if @hero.save
      redirect_to heroes_path, notice: 'Bohater został dodany'
    else
      render 'new'
    end
  end

  def edit

  end

  def update
    @hero.update( hero_params )
    
    redirect_to heroes_path, notice: 'Bohater został zaktualizowany'
  end

  def destroy
    @hero.destroy

    respond_to do |format|
      format.js
    end
  end

  private

  def get_hero
    @hero = Hero.find( params[:id] )
  end

  def hero_params
    params.require( :hero ).permit( [:name, :town_id] )
  end   
end
