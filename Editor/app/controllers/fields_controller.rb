class FieldsController < ApplicationController

  def create
    @field = Field.where( field_find_params ).first
    
    @field.update( field_create_params ) 

    respond_to do |format|
      format.js
    end
  end

  private

  def field_find_params
    { 
      map_id: params[:field][:map_id], 
      x: params[:field][:x],  
      y: params[:field][:y] 
    }
  end

  def field_create_params
    params.require( :field ).permit( [:ground_id, :map_id, :mine_id, :town_id, :x, :y] )
  end    

end
