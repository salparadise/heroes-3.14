# encoding: utf-8

class TownsController < ApplicationController
  before_filter :get_town, except: [:index, :new, :create]

  def index
    @towns = Town.includes( [:heroes, :troops] ).by_name

    respond_to do |format|
      format.html 
      format.json { send_data @towns.to_json, type: :json, disposition: 'attachment' }
    end
  end

  def new
    @town = Town.new
  end

  def create
    @town = Town.new( town_params )
    
    if @town.save
      redirect_to towns_path, notice: 'Miasto zostało dodane'
    else
      render 'new'
    end    
  end

  def edit

  end

  def update
    @town.update( town_params )
    
    redirect_to towns_path, notice: 'Miasto zostało zaktualizowane'
  end

  def destroy
    @town.destroy

    respond_to do |format|
      format.js
    end
  end

  private

  def get_town
    @town = Town.find( params[:id] )
  end

  def town_params
    params.require( :town ).permit( [:ground_id, :name, :sprite] )
  end  
end
