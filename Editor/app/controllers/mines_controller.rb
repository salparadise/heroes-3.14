# encoding: utf-8

class MinesController < ApplicationController
  before_filter :get_mine, except: [:index, :new, :create]

  def index
    @mines = Mine.by_name

    respond_to do |format|
      format.html 
      format.json { send_data @mines.to_json, type: :json, disposition: 'attachment' }
    end
  end

  def new
    @mine = Mine.new
  end

  def create
    @mine = Mine.new( mine_params )
    
    if @mine.save
      redirect_to mines_path, notice: 'Kopalnia została dodana'
    else
      render 'new'
    end    
  end

  def edit

  end

  def update
    @mine.update( mine_params )
    
    redirect_to mines_path, notice: 'Kopalnia została zaktualizowana'
  end

  def destroy
    @mine.destroy

    respond_to do |format|
      format.js
    end
  end

  private

  def get_mine
    @mine = Mine.find( params[:id] )
  end

  def mine_params
    params.require( :mine ).permit( [:name, :sprite] )
  end  
end
