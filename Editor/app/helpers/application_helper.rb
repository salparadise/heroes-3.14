module ApplicationHelper

  def menu_link_to body, url, options = {}
    options[:class] ||= ''
    options[:class]  += 'active' if current_page?( url )

    content_tag :li, link_to( body, url ), options
  end
end
