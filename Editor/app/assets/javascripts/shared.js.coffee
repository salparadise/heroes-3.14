jQuery ->
  $( '.table' ).dataTable
    'sPaginationType' : 'bootstrap'

  if $( '.sprite img' ).length > 0
    $sprite = $( '.sprite' )

    $sprite.append( '<ul></ul>' )

    for i in [0..( ( $sprite.find( 'img' ).width() / 50 - 1 ) ) ]
      $sprite.find( 'ul' ).append( '<li></li>')

    index   = $sprite.siblings( 'input' ).val()
    index ||= 0

    $sprite.find( 'li' ).eq( index ).addClass( 'active' )

    $sprite.find( 'li' ).click (e) ->
      e.preventDefault()

      value = $( @ ).index()

      $sprite.find( '.active' ).removeClass( 'active' )
      $( @ ).addClass( 'active' )

      $sprite.siblings( 'input' ).val( value )
