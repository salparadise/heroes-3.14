update_field = ( path, map_id, x, y, ground_id, town_id = null, mine_id = null ) -> 
  field = {
    map_id: map_id,
    ground_id: ground_id,
    x: x,
    y: y
  }

  unless town_id == null then field.town_id = town_id
  unless mine_id == null then field.mine_id = mine_id

  $.ajax
    url:  path,
    type: 'post',
    data: { field: field }

jQuery ->

  # Set fields size
  $( '.fields .inner' ).width( "#{ $( '.fields' ).data( 'size' ) * 49 }" )

  # Select one building only
  $( 'input[name=mine_id]' ).change ->
    if $( 'input[name=town_id]:radio:checked' ).val() > 0
      $( 'input[name=town_id]:radio:first' ).prop( 'checked', 'checked' )

  # Select one building only
  $( 'input[name=town_id]' ).change ->
    if $( 'input[name=mine_id]:radio:checked' ).val() > 0
      $( 'input[name=mine_id]:radio:first' ).prop( 'checked', 'checked' )

  # Select mode
  $( 'input[name=mode_id]' ).change ->
    if $( @ ).val() == '1'
      $( 'input[name=mine_id], input[name=town_id]' ).removeAttr( 'disabled' )
      $( '.fields' ).find( '.start' ).removeClass( 'start active' )
    else
      $( 'input[name=mine_id], input[name=town_id]' ).prop( 'disabled', 'disabled' )
    
  # Field click
  $( '.fields .field' ).click (e) ->
    e.preventDefault()

    $ground = $( 'input[name=ground_id]:radio:checked' )
    $mode   = $( 'input[name=mode_id]:radio:checked' )
    $mine   = $( 'input[name=mine_id]:radio:checked' )
    $town   = $( 'input[name=town_id]:radio:checked' )


    ground = {
      id:     $ground.val(),
      sprite: $ground.data( 'sprite' )
    }

    mine = {
      id:     $mine.val(),
      sprite: $mine.data( 'sprite' )
    }

    town = {
      id:     $town.val(),
      sprite: $town.data( 'sprite' )
    }

    map_id = $( '.fields' ).data( 'map-id' )
    path   = $( '.fields' ).data( 'path' )

    # Single mode
    if $mode.val() == '1'
      x = $( @ ).data( 'x' )
      y = $( @ ).data( 'y' )

      if town.id > 0 or mine.id > 0
        if town.sprite > 0 then shift = town.sprite
        else shift = mine.sprite

        $( @ ).find( '.field-sprite' )
          .removeClass( 'ground' )
          .addClass( 'building' )
          .css( 'background-position' : "-#{ shift * 50 }px 0px" )
      else
        $( @ ).find( '.field-sprite' )
          .removeClass( 'building' )
          .addClass( 'ground' )
          .css( 'background-position' : "-#{ ground.sprite * 50 }px 0px" )

      update_field( path, map_id, x, y, ground.id, town.id, mine.id )
    # Multiple mode
    else
      if $( '.fields' ).find( '.start' ).length > 0
        x_s = $( '.fields' ).find( '.start' ).data( 'x' )
        y_s = $( '.fields' ).find( '.start' ).data( 'y' )

        x_e = $( @ ).data( 'x' )
        y_e = $( @ ).data( 'y' )

        if x_s > x_e
          x_start = x_e
          x_end   = x_s
        else
          x_start = x_s
          x_end   = x_e

        if y_s > y_e
          y_start = y_e
          y_end   = y_s
        else
          y_start = y_s
          y_end   = y_e

        for y in [y_start..y_end]
          for x in [x_start..x_end]
            update_field( path, map_id, x, y, ground.id )

            $field = $( ".field[data-y=#{ y }][data-x=#{ x }]" ).find( '.field-sprite' )

            unless $field.hasClass( 'building' )
              $field.addClass( 'ground' ).css( 'background-position' : "-#{ ground.sprite * 50 }px 0px" )

        $( '.fields' ).find( '.start' ).removeClass( 'start active' )
      else
        $( @ ).addClass( 'start active' ) 