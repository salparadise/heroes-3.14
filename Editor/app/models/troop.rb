class Troop < ActiveRecord::Base
  belongs_to :town
  belongs_to :type, class_name: 'TroopType', foreign_key: :troop_type_id

  scope :by_name, -> { order( name: :asc ) }

  validates :abilities,  presence: true
  validates :cost,       presence: true
  validates :level,      presence: true
  validates :max_health, presence: true
  validates :max_damage, presence: true
  validates :min_damage, presence: true
  validates :name,       presence: true
  validates :range,      presence: true
  validates :town,       presence: true
  validates :type,       presence: true

  def as_json options = {}
    {
      id:            id,
      name:          name,
      cost:          cost.to_s,
      abilities:     abilities.to_s,
      level:         level.to_s,
      max_health:    max_health.to_s,
      max_damage:    max_damage.to_s,
      min_damage:    min_damage.to_s,
      range:         range.to_s,    
      upgrade:       upgrade.to_s, 
      town_id:       town_id,
      troop_type_id: troop_type_id
    }
  end  
end
