class Map < ActiveRecord::Base
  before_save :set_objectives

  before_create :set_size
  before_create :set_max_player_count
  before_create :set_max_town_count
  after_create  :create_fields

  attr_accessor :is_random, :size, :victory_condition, :defeat_condition

  has_many :fields, dependent: :delete_all
  
  scope :by_name, -> { order( name: :asc ) }
  
  # validates :height, presence: true
  validates :name, presence: true
  validates :size, on: :create, presence: true
  # validates :town_count, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 8 }
  # validates :width,  presence: true

  def as_json options = {}
    {
      id:     id,
      name:   name,
      height: height,
      width:  width,
      victory: victory.to_s,
      defeat: defeat.to_s,
      max_player_count: max_player_count,
      town_count: town_count_json,
      towns:  towns_json,
      mines:  mines_json,
      fields: fields_json
    }
  end

  def randomize
    # randomize_fields
    randomize_towns
    randomize_mines
  end

  private

  def set_max_player_count
    self.max_player_count = size.to_i / 8
  end

  def set_max_town_count
    self.max_town_count = size.to_i / 4
  end  

  def fields_json
    json = []

    fields.includes( :ground ).all.each do |field|
      json << { x: field.x, y: field.y, texture_id: field.ground.sprite, cost: field.ground.cost, color: field.ground.color }
    end

    json
  end

  def mines_json
    json = []

    fields.where.not( mine_id: nil ).each do |field|
      if field.mine_id.to_i > 0
        json << { x: field.x, y: field.y, mine_id: field.mine_id }
      end
    end

    json
  end

  def towns_json
    json = []

    fields.where.not( town_id: nil ).each do |field|
      if field.town_id.to_i > 0
        json << { x: field.x, y: field.y, town_id: field.town_id }
      end
    end

    json
  end

  def town_count_json
    town_count = 0
    
    fields.where.not( town_id: nil ).each do |field|
      if field.town_id.to_i > 0
        town_count += 1
      end
    end

    town_count
  end

  def set_objectives
    self.victory = victory_condition unless victory_condition.blank?
    self.defeat  = defeat_condition  unless defeat_condition.blank?
  end

  def set_size
    self.height = self.width = size
  end

  def create_fields
    @fields = Hash.new

    ground = Ground.first

    (1..height).each_with_index do |y|
      (1..width).each_with_index do |x|
        @fields[[y, x]] = Field.create( x: x, y: y, ground_id: ground.id, map_id: id )
      end
    end
    
    randomize if is_random.to_i > 0
  end

  def randomize_fields
    fields.all.each do |f| 
      f.ground = Ground.random.first

      f.save 
    end
  end

  def randomize_towns 
    ( 1..max_town_count ).each do
      y = x = 0

      until can_build_town_at?( y, x )
        x = rand( 1..width )
        y = rand( 1..height )
      end

      build_town_at( y, x )
    end
  end

  def build_town_at( y, x )
    @fields[[y, x]].update( town: Town.random.first )
  end

  def can_build_town_at?( y, x )
    return false if y == 0 and x == 0

    permission = true
    
    radius = ( width / 5 ).to_i
    
    start_y = y - radius < 1 ? 1 : y - radius 
    end_y   = y + radius > width ? width : y + radius

    start_x = x - radius < 1 ? 1 : x - radius 
    end_x   = x + radius > width ? width : x + radius

    ( start_y .. end_y ).each do |y|
      ( start_x .. end_x ).each do |x|
        if @fields[[y, x]].town 
          permission = false

          break
        end
      end
    end

    permission
  end

  def randomize_mines
    radius = 2

    fields.where.not( town_id: nil ).each do |f|
      start_y = f.y - radius < 1 ? 1 : f.y - radius 
      end_y   = f.y + radius > width ? width : f.y + radius

      start_x = f.x - radius < 1 ? 1 : f.x - radius 
      end_x   = f.x + radius > width ? width : f.x + radius     

      Mine.all.each do |m|
        y = x = 0

        until can_build_mine_at?( y, x )
          x = rand( start_x..end_x )
          y = rand( start_y..end_y )
        end

        build_mine_at( m, y, x )
      end
    end
  end

  def can_build_mine_at?( y, x )
    return false if y == 0 and x == 0

    field = @fields[[y, x]]

    return if field.town or field.mine 

    return true
  end  

  def build_mine_at( mine, y, x )
    @fields[[y, x]].update( mine: mine )
  end  

end
