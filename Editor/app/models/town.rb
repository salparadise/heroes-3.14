class Town < ActiveRecord::Base
  belongs_to :ground

  has_many :heroes
  has_many :troops

  scope :by_name, -> { order( name: :asc ) }
  scope :random,  -> { order( 'random()' ) }
  
  validates :ground, presence: true
  validates :name,   presence: true

  def as_json options = {}
    {
      id:         id,
      texture_id: ground_id,
      name:       name,
      heroes:     heroes,
      sprite:     sprite.to_s,
      troops:     troops
    }
  end  
end
