class Hero < ActiveRecord::Base
  belongs_to :town

  scope :by_name, -> { order( name: :asc ) }
  
  validates :name,    presence: true
  validates :town_id, presence: true

  def as_json options = {}
    {
      id:      id,
      name:    name,
      town_id: town_id
    }
  end  
end
