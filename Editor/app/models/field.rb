class Field < ActiveRecord::Base
  belongs_to :ground
  belongs_to :mine
  belongs_to :town
  belongs_to :map
  
  validates :ground_id,  presence: true
  validates :map_id,     presence: true

  def as_json options = {}
    {
      id:         id,
      texture_id: ground_id,
      mine_id:    mine_id,
      town_id:    town_id
    }
  end  
end
