class TroopType < ActiveRecord::Base
  has_many :troops

  scope :by_name, -> { order( name: :asc ) }

  validates :name, presence: true
  
  def as_json options = {}
    {
      id:       id,
      name:     name,
      shooting: shooting.to_s,
      magic:    magic.to_s,
      flying:   flying.to_s
    }
  end  
end
