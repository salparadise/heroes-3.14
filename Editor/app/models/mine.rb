class Mine < ActiveRecord::Base
  scope :by_name, -> { order( name: :asc ) }
  
  validates :name, presence: true

  def as_json options = {}
    {
      id:     id,
      name:   name,
      sprite: sprite.to_s
    }
  end  
end
