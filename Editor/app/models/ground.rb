class Ground < ActiveRecord::Base  
  scope :by_name, -> { order( name: :asc ) }
  scope :random,  -> { order( 'random()' ) }
  
  validates :cost,        presence: true
  validates :name,        presence: true
  # validates :probability, presence: true

  def as_json options = {}
    {
      id:     id,
      name:   name,
      cost:   cost.to_s,
      color:  color,
      sprite: sprite.to_s
    }
  end  
end
