# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140126205508) do

  create_table "fields", force: true do |t|
    t.integer  "map_id"
    t.integer  "x"
    t.integer  "y"
    t.integer  "ground_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "town_id"
    t.integer  "mine_id"
  end

  create_table "grounds", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "color"
    t.integer  "cost"
    t.integer  "sprite",      default: 0
    t.decimal  "probability"
  end

  create_table "heros", force: true do |t|
    t.string   "name"
    t.integer  "town_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "maps", force: true do |t|
    t.string   "name"
    t.integer  "height"
    t.integer  "width"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "max_player_count"
    t.integer  "max_town_count"
    t.integer  "objective_id"
    t.string   "victory"
    t.string   "defeat"
  end

  create_table "mines", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sprite",     default: 0
  end

  create_table "objectives", force: true do |t|
    t.string   "name"
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "towns", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sprite",     default: 0
    t.integer  "ground_id"
  end

  create_table "troop_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "flying",     default: false
    t.boolean  "magic",      default: false
    t.boolean  "shooting",   default: false
  end

  create_table "troops", force: true do |t|
    t.integer  "abilities"
    t.integer  "max_health"
    t.integer  "max_damage"
    t.integer  "min_damage"
    t.integer  "range"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.integer  "town_id"
    t.integer  "troop_type_id"
    t.boolean  "upgrade",       default: false
    t.integer  "level"
    t.integer  "cost"
  end

end
