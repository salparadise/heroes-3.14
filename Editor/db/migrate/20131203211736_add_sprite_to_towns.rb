class AddSpriteToTowns < ActiveRecord::Migration
  def change
    add_column :towns, :sprite, :integer, default: 0
  end
end
