class AddSpriteToMines < ActiveRecord::Migration
  def change
    add_column :mines, :sprite, :integer, default: 0
  end
end
