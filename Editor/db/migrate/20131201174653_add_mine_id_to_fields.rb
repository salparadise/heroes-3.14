class AddMineIdToFields < ActiveRecord::Migration
  def change
    add_column :fields, :mine_id, :integer
  end
end
