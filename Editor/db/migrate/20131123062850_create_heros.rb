class CreateHeros < ActiveRecord::Migration
  def change
    create_table :heros do |t|
      t.string :name
      t.belongs_to :town

      t.timestamps
    end
  end
end
