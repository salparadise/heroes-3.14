class AddCostToTroops < ActiveRecord::Migration
  def change
    add_column :troops, :cost, :integer
  end
end
