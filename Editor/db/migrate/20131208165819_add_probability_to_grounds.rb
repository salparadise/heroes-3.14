class AddProbabilityToGrounds < ActiveRecord::Migration
  def change
    add_column :grounds, :probability, :decimal
  end
end
