class AddSpriteToGrounds < ActiveRecord::Migration
  def change
    add_column :grounds, :sprite, :integer, default: 0
  end
end
