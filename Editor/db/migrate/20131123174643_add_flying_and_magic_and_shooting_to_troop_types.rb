class AddFlyingAndMagicAndShootingToTroopTypes < ActiveRecord::Migration
  def change
    add_column :troop_types, :flying, :boolean, default: false
    add_column :troop_types, :magic, :boolean, default: false
    add_column :troop_types, :shooting, :boolean, default: false
  end
end
