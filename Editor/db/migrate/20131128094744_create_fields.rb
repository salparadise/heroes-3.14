class CreateFields < ActiveRecord::Migration
  def change
    create_table :fields do |t|
      t.integer :map_id
      t.integer :x
      t.integer :y
      t.integer :ground_id

      t.timestamps
    end
  end
end
