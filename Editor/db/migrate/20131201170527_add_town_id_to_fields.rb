class AddTownIdToFields < ActiveRecord::Migration
  def change
    add_column :fields, :town_id, :integer
  end
end
