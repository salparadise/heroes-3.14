class AddVictoryAndDefeatToMaps < ActiveRecord::Migration
  def change
    add_column :maps, :victory, :string
    add_column :maps, :defeat, :string
  end
end
