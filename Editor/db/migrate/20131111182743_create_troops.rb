class CreateTroops < ActiveRecord::Migration
  def change
    create_table :troops do |t|
      t.integer :abilities
      t.integer :max_health
      t.integer :max_damage
      t.integer :min_damage
      t.integer :range

      t.timestamps
    end
  end
end
