class CreateMines < ActiveRecord::Migration
  def change
    create_table :mines do |t|
      t.string :name

      t.timestamps
    end
  end
end
