class AddMaxTownCountAndMaxPlayerCountToMaps < ActiveRecord::Migration
  def change
    add_column :maps, :max_player_count, :integer
    add_column :maps, :max_town_count, :integer
  end
end
