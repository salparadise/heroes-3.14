class AddObjectiveIdToMaps < ActiveRecord::Migration
  def change
    add_column :maps, :objective_id, :integer
  end
end
