class AddNameAndTownIdTroopTypeIdToTroops < ActiveRecord::Migration
  def change
    add_column :troops, :name, :string
    add_column :troops, :town_id, :integer
    add_column :troops, :troop_type_id, :integer
  end
end
