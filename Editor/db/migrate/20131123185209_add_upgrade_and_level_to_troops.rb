class AddUpgradeAndLevelToTroops < ActiveRecord::Migration
  def change
    add_column :troops, :upgrade, :boolean, default: false
    add_column :troops, :level, :integer
  end
end
