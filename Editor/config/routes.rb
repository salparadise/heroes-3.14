Heroes::Application.routes.draw do

  root 'pages#index'
  
  resources :fields
  resources :grounds
  resources :heroes
  resources :maps do
    get 'fields',    on: :member
    get 'randomize', on: :member
  end
  resources :mines
  resources :towns  
  resources :troops
  resources :troop_types
    
end
