/*
 * FogOfWarLayer.hpp
 *
 *  Created on: 14 gru 2013
 *      Author: hawker
 */

#ifndef FOGOFWARLAYER_HPP_
#define FOGOFWARLAYER_HPP_
#include "Layer.hpp"

class FogOfWarLayer : public Layer {
public:
	FogOfWarLayer(std::shared_ptr<Game>);
	virtual bool processEvent(sf::Event&);
	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
	virtual void update(sf::Time dt);

protected:
	bool isMoving = false;
	std::shared_ptr<sf::RenderTexture> _drawBuffer;
};



#endif /* FOGOFWARLAYER_HPP_ */
