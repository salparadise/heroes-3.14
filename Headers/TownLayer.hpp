#ifndef TOWNLAYER_HPP_
#define TOWNLAYER_HPP_

#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include <memory>
#include "Layer.hpp"
#include "Player.hpp"
#include "Town.hpp"
#include "StringConverter.hpp"

/**
 * Klasa TownLayer, ktorej zadaniem jest rysowanie ekranu miasta.
 * Ekran miasta powinien przeslonic cala plansze (lacznie z Sidebarem i Topbarem).
 * W ekranie tym mozna budowac budynki dzieki ktorym mozliwe jest stworzenie nowej armii
 * (lub rozbudowanie istniejacej) oraz zwiekszenie garnizonu miejskiego.
 */
class TownLayer : public Layer {
public:
	TownLayer(std::shared_ptr<Game>);
	virtual ~TownLayer() {}

	/**
	 * Metoda processEvent przechwytuje odpowiednie zdarzenia.
	 * W przypadku warstwy TownLayer oznacza to wybranie interesujacego budynku oraz operacje na nim.
	 *
	 * @param sf::Event& Aktualnie przetwarzane zdarzenie.
	 */
	virtual bool processEvent(sf::Event&);

	/**
	 * Metoda draw wyrysowuje znajdujace sie w miescie budynki oraz okienka dialogowe.
	 *
	 * @param sf::RenderTarget& Okno docelowe po ktorym warstwa rysuje.
	 * @param sf::RenderStates Dodatkowe informacje.
	 */
	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

	/**
	 * Metoda sprawdza, czy potrzebne jest otwarcie lub aktualizacja ekranu miasta.
	 *
	 * @param sf::Time Czas od ostatniej aktualizacji stanu gry.
	 */
	virtual void update(sf::Time dt);

	/**
	 * Metoda zamykajaca ekran miasta i zwiazane z nim sfgui.
	 */
	void closeCity() {
		_isActive = false;
		_gui = nullptr;
		_town = nullptr;
	}

	/**
	 * Metoda pozwalajaca rozbudowac mury miejskie.
	 */
	void improveWalls();

	/**
	 * Metoda pozwalajaca zatwierdzic zakup jednostek.
	 */
	void buyTroops();

	/**
	 * Metoda pozwalajaca zakupic aktualny budynek.
	 */
	void buyBuilding();

	/**
	 * Metoda zamykajaca aktualne okienko dialogowe.
	 */
	void closeCurrent();

	/**
	 * Metoda ustawiajaca wybrane okienko na srodku ekranu.
	 * Zmodyfikowana, by nie uwzgledniac Sidebara.
	 *
	 * @param Obiekt sfg::Window, ktory chcemy przestawic.
	 */
	virtual void centerWindowTown(sfg::Window::Ptr);

protected:
	/**
	 * RenderTextura na ktorej rysowane sa wszystkie elementy.
	 * Dzieki rysowaniu w pamieci, zamiast bezposrednio na ekranie, operacje te sa szybsze.
	 */
	std::shared_ptr<sf::RenderTexture> _drawBuffer;

	/**
	 * Okienko dialogowe.
	 */
	sfg::Window::Ptr _gui = nullptr;

	/**
	 * Aktualnie otwarte miasto.
	 */
	std::shared_ptr<Town> _town = nullptr;

	/**
	 * Pole, w ktore wprowadzana jest ilosc kupowanych jednostek.
	 */
	sfg::SpinButton::Ptr _range;

	/**
	 * Tlo ekranu miasta.
	 */
	sfg::Image::Ptr _background;

	/**
	 * Biala plansza - dla wiekszych rozdzielczosci niz 800x600
	 */
	sfg::Window::Ptr _whiteBoard;
};


#endif /* TOWNLAYER_HPP_ */
