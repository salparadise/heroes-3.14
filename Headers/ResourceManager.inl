/**
 * ResourceManager.inl
 */

template <typename Resource>
void ResourceManager<Resource>::loadByJSON(const std::string& filename)
{
	std::string jsonFile = prepareJSONFile(filename);
	Json::Value root;
	Json::Reader reader;
	bool parsingSuccessful = reader.parse( jsonFile, root );
	if ( !parsingSuccessful )
		throw std::runtime_error("Failed to parse configuration\n" + reader.getFormattedErrorMessages());

	// Iteruje po calym pliku .json
	for(int i = 0; i < root.size(); ++i)
	{
		const int id = root[i]["id"].asInt();
		const std::string path = root[i]["path"].asString();
		if(!path.empty())
		{
			load(id, path);
		}
	}
}

template <typename Resource>
void ResourceManager<Resource>::load(const int id, const std::string& filename)
{
	// Zaalokuj pamiec i zaladuj zasob ze sciezki "filename"
	std::unique_ptr<Resource> resource(new Resource());
	if (!resource->loadFromFile(filename))
		throw std::runtime_error("ResourceManager::load - Failed to load " + filename);

	// Jesli ladowanie pliku zaokczy sie sukcesem, wsadz zasob do kontenera
	insertResource(id, std::move(resource));
}

template <typename Resource>
const Resource& ResourceManager<Resource>::get(const int id) const
{
	// Znajdz zadany element w kontenerze
	auto found = _resourcesMap.find(id);
	assert(found != _resourcesMap.end());

	// Jesli znaleziono plik
	return *found->second;
}

template <typename Resource>
const Resource& ResourceManager<Resource>::operator[](const int id) const
{
	return get(id);
}

template <typename Resource>
void ResourceManager<Resource>::insertResource(const int id, std::unique_ptr<Resource> resource)
{
	// Umiesc zasob w kontenerze
	auto inserted = _resourcesMap.insert(std::make_pair(id, std::move(resource)));
	assert(inserted.second);
}

template <typename Resource>
std::string ResourceManager<Resource>::prepareJSONFile(const std::string& filename)
{
	std::ifstream in(filename, std::ios::in);
	if (in)
	{
		std::string json;
		in.seekg(0, std::ios::end);
		json.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&json[0], json.size());
		in.close();
		return(json);
	}
	throw std::runtime_error("error in ResourceManager<Resource>::prepareJSONFile. cannot load " + filename);
}
