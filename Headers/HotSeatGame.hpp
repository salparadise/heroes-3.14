#ifndef HOT_SEAT_GAME_HPP_
#define HOT_SEAT_GAME_HPP_

#include <SFML/Graphics.hpp>
#include <memory>
#include <algorithm>
#include <string>
#include <iostream>
#include <tuple>
#include <cstdlib>
#include "Game.hpp"
#include "StringConverter.hpp"

class HotSeatGame : public Game{
	private:
		//const int _ileGraczy;

	public:
		HotSeatGame(int ileGraczy, std::shared_ptr<sf::RenderWindow> w, TextureHolder& textures);
/*
*@fn HotSeatGame( std::vector<std::shared_ptr<Player>> gracze, std::shared_ptr<sf::RenderWindow> w)
*@param gracze Wektor obiekto klasy player.
*
*
*/
		HotSeatGame(std::vector<std::shared_ptr<Player>> gracze, std::shared_ptr<sf::RenderWindow> w, TextureHolder& textures);
/*
*@fn HotSeatGame( std::vector<std::pair<std::string,std::string> >  gracze, std::shared_ptr<sf::RenderWindow> w)
*@param gracze Wektor par stringow- pierwsza imie, druga id rasy.
*
*/

		HotSeatGame( std::vector<std::pair<std::string,int> >  gracze, std::shared_ptr<sf::RenderWindow> w, TextureHolder& textures);
		
		/**
		 * Konstruktor HotSeatGame.
		 * @param gracze krotka (std::tuple<>) zawierajaca:
		 * 			std::string imie gracza,
		 * 			int nr id rasy,
		 * 			bool informacje o tym czy gracz jest kontrolowany przez cz�owieka czy komputer
		 * 			sf::Vector2i polozenie zamku, ktory zostal wybrany dla gracza
		 * @param w
		 * @param textures
		 */
		HotSeatGame(

					std::vector<std::tuple<std::string, int, bool, sf::Vector2i>> gracze,
					std::shared_ptr<sf::RenderWindow> w,
					TextureHolder& textures,
					std::string path);

};

#endif /* HOT_SEAT_GAME_HPP_ */
