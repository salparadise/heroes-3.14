#ifndef ARMY_HPP_
#define ARMY_HPP_

#include <vector>
#include <map>
#include <memory>
#include <array>
#include <SFML/Graphics.hpp>
#include <SFML/System/Vector2.hpp>
#include "Hero.hpp"
#include "ActiveObject.hpp"
#include "Creature.hpp"
#include "ScriptedStateMachine.hpp"

class Field;
class Player;

#ifdef Polish_dox
/**
 *   @class	Army
 *   Klasa reprezentujaca jednostke (a raczej zbior jednostek) na planszy.
 *   Tablica _creatures przechowuje liczbe jednostek. Do statystyk dobieramy sie przez "getCreature(int lv)".
 *
 *   Army dziedziczy rowniez po std::enable_shared_from_this<Army> aby wskaznik this na obiekty Army mogly byc pobierane
 *   jako std::shared_ptr<Army>. Taki wskaznik zwraca funkcja shared_from_this().
 *   Tylko w taki sposob mozna pozyskac wskaznik this typu std::shared_ptr<>.
 *   Konwersja static_cast<> dziala poprawnie dla polowy przypadkow, dla drugiej polowy wysypuje program
 *   (i pozniej szukaj sobe takiego bledu..).
 *
 *   _hero to zmienna przechowujaca bohatera. Bohaterowie nie sa zrobieni ale miejsce dla nich jest.
 *   _move to wartosc ile ruchu pozostalo armi do przejscia w tej rundzie.
 *   _moveD to wartosc ile ruchu moze przejsc armia w rundzie (maksymalnie)
 *
 */
#endif

class Army : public ActiveObject, public std::enable_shared_from_this<Army> {
	private:
		std::array<int, 10> _creatures={};
		std::shared_ptr<Hero> _hero;
		int _armySize;
		int _move;
		int _moveD;
		bool _selected = false;
		int _lineOfSight;


		// -------------------------------------------------------------------------FSM
		ScriptedStateMachine<Army>* _stateMachine;
	public:
		#ifdef Polish_dox
        /**
        *	Army(std::shared_ptr<Player> player = nullptr) przyjmuje za argument wlasciciela.
        *   Moze nie zostac podany, wtedy armia jest bezpanska (potrzebne to bylo tylko do testow,
        *   moze pozniej robiac neutralne potwory bedzie sie tworzyc bez wlasciciela).
        *
        */
		#endif
		Army(std::shared_ptr<Player> player, std::shared_ptr<Field> currentField, int hoursOfSleep = 0);
		Army(std::shared_ptr<Player> player, int move, std::shared_ptr<Field> currentField, int hoursOfSleep = 0);

		#ifdef Polish_dox
		/**
		 *   Konstruktor armii miejskiej. Armia taka nie musi byc sterowana w sztucznej inteligencji,
		 *   nie musi tak�e byc przywiazana do zadnego pola (jest zwiazana z miastem).
		 */
		#endif
		Army(std::shared_ptr<Player> = nullptr);

		void setHero(std::shared_ptr<Hero> hero) {_hero=hero; }
		Hero getHero() {return *_hero; }
		#ifdef Polish_dox
		/**
		 *@fn	update()
		 *Funkcja uaktualniajaca ruch, wywolywana co ture.
		 */
		#endif
		void update();
		#ifdef Polish_dox
		/**
		 *@fn	attack(std::shared_ptr<Army> enemy)
		 *Funkcja ataku na inna armie. Armie walcza dopoki ktoras nie zostanie calkowicie zniszczona.
		 *@param std::shared_ptr<Army> enemy wskaznik do przeciwnika
		 *@return Zwraca wskaznik do zwyciezcy.
		 */
		#endif
		std::shared_ptr<Army> attack(std::shared_ptr<Army> enemy);
		#ifdef Polish_dox
		/**
		 *@fn	getCreature(int lv)
		 *Funkcja ktora wg poziomu zwraca wskaznik do jednostki danego poziomu.
		 */
		#endif
		std::shared_ptr<Creature> getCreature(int lv) const;

		#ifdef Polish_dox
		/**
		 *@fn	updateCreature(int lv, int amount)
		 *Zmiena ilosc jednostek danego poziomu (lv) o podana wartosc (amount). Jesli chcemy zmniejszyc to nalezy dac ujemna ilosc.
		 */
		#endif

		void updateCreature(int lv, int amount);
		#ifdef Polish_dox
		/**
		 *@fn	getCreatureAmount(int lv)
		 *Podaje ilosc jednostek danego poziomu (lv).
		 */
		#endif
		int getCreatureAmount(int lv) const {return _creatures[lv-1]; }

		#ifdef Polish_dox
		/**
		 * Funkcja ��cz�ca oddzia�y armii. Nale�y z niej korzystac tylko gdy oddzialy
		 * sasiaduja na mapie. Do laczenia armii znajdujacych sie w wiekszej odleglosci
		 * od siebie sluzy funkcja mergeWithArmy().
		 * @param targetArmy Armia, z ktora chcemy polaczyc obecna armie.
		 */
		#endif
		void mergeArmy(const std::shared_ptr<Army>& targetArmy);
		#ifdef Polish_dox
		/**
		 * Funkcja sluzy do przejmowania budynkow. Nalezy z niej korzystac tylko gdy oddzial
		 * przejmujacy budynek jest obecny na polu z tym budynkiem. Do przejmowania budynkow
		 * gdy armia jest w pewnej odleglosci sluzy funkcja claimThatBuilding().
		 * @param targetBuilding Budynek, ktory obenca armia ma przejac.
		 */
		#endif
		void claimBuilding(const std::shared_ptr<Building>& targetBuilding);
		#ifdef Polish_dox
		/**
		 * Funkcja sluzy do przejmowania miast. Nalezy z niej korzystac tylko gdy oddzial
		 * przejmujacy miasto jest obecny na polu z tym miastem. Do przejmowania miast
		 * gdy armia jest w pewnej odleglosci sluzy funkcja claimThatTown().
		 * @param targetTown Miasto, ktore obenca armia ma przejac.
		 */
		#endif
		void claimTown(const std::shared_ptr<Town>& targetTown);
		int getMinRange() const;
		int getMoveD() const { return _moveD; }
		int getMove() const { return _move;}
		#ifdef Polish_dox
		/**
		 * Obecnie funkcja wuzglednia koszt ruchu. Jesli w tej turze gracz ma jeszcze
		 * odpowiednia ilosc 'punktow' na ten ruch to funkcja odejmuje koszt wykonywanego
		 * ruchu od ilosci 'punktow' gracza oraz uaktualnia pozycje Armii w wektorze Armii
		 * Player::_playersArmiesVector
		 * @param newField Pole Field, na ktore przesuwa sie armia
		 * @param cost Koszt przesuniecia sie na pole newField
		 */
		#endif
		void move(std::shared_ptr<Field> newField, int cost);

		#ifdef Polish_dox
		/**
		 * Funkcja, przelatujac po wektorach armii wszystkich graczy, sprawdza czy
		 * nie ma jakiejs wrogiej armii w zasiegu. Zasieg to zmienna Hero::_range.
		 * Przyjmuje, ze wszyscy gracze graja przeciwko wszystkim graczom.
		 * @return Zwraca najblizsza wroga armie jesli jest w zasiegu.
		 * Jesli nie ma wrogiej armii w zasiegu to zwraca nullptr.
		 */
		#endif
		std::shared_ptr<Army> enemyInRange() const;

		#ifdef Polish_dox
		/**
		 * Funkcja znajduje i zwraca najblizej polozona sojusznicza Armie. Funkcja zwraca nullptr
		 * gdy nie znajdzie zadnej sojuszniczej armii w zasiegu ruchu.
		 * @return Zwraca ajblizej polozona sojusznicza armie.
		 */
		#endif
		std::shared_ptr<Army> myArmyInMoveRange() const;

		#ifdef Polish_dox
		/**
		 * Funkcja, przelatujac po wektorach budynkow wszystkich graczy, sprawdza czy
		 * nie ma budynku przejetego przez innego gracza w zasiegu. Zasieg to zmienna Hero::_range.
		 * Przyjmuje, ze wszyscy gracze graja przeciwko wszystkim graczom.
		 * @return Zwraca najblizszy nie nalezacy do gracza budynek jesli jest w zasiegu.
		 * Jesli nie ma takowego to zwraca nullptr.
		 */
		#endif
		std::shared_ptr<Building> buildingInRange() const;

		#ifdef Polish_dox
		/**
		 * Funkcja zwraca wskaznik do miasta nie nalezacego do gracza, o ile znajduje sie ono
		 * w zasiegu ruchu armii.
		 * @return Zwraca najblizsze nie nalezace do gracza miasto jesli jest w zasiegu.
		 * Jesli nie ma takowego to zwraca nullptr.
		 */
		#endif
		std::shared_ptr<Town> townInRange() const;

		#ifdef Polish_dox
		/**
		 * Funkcja ocenia przekazywana w pierwszym argumencie armie jedynie na podstawie
		 * ilosci jednostek (Army::getCreatureAmount()).
		 * @param enemyArmy Oceniana armia
		 * @return Zwraca true jesli enemyArmy posiada wiecej jednostek.
		 * W przeciwnym wypadku zwraca false.
		 */
		#endif
		bool isEnemyStronger(const std::shared_ptr<Army>& enemyArmy) const;

		#ifdef Polish_dox
		/**
		 * Funkcja ustala najoptymalniejsza trase (za pomoca PathFinding().findPathAStar())
		 * do punktu (Field) przekazywanego w pierwszym argumencie. Gdy koszt dotarcia
		 * do celu jest wiekszy niz posiadana liczba 'punktow' ruchu, wykonywany jest maksymalny
		 * ruch na jaki pozwala liczba 'punktow' ruchu w kierunku celu.
		 * @param targetField Punkt (Field) docelowy
		 * @return Zwraca true gdy osiagnieto cel. W przeciwnym wypadku zwraca false
		 */
		#endif
		bool goTo(const std::shared_ptr<Field>& targetField);

		#ifdef Polish_dox
		/**
		 * Pobiera polozenie obiektu Object i uruchamia przeladowana funkcje
		 * goTo(const std::shared_ptr<Field>&)
		 * @param target Obiekt docelowy
		 * @return Zwraca true gdy osiagnieto cel. W przeciwnym wypadku zwraca false
		 */
		#endif
		bool goTo(const std::shared_ptr<Object>& target);

		#ifdef Polish_dox
		/**
		 * Funkcja przemieszczajaca armie na pole sasiadujace z targetField.
		 * @param targetField Pole sasiadujace z polem, na ktore armia ma sie udac.
		 * @return Zwraca true jesli osiagnieto pole sasiadujace z targetField.
		 * W przeciwnym wypadku zwraca false.
		 */
		#endif
		bool goToNeighbourFieldTo(const std::shared_ptr<Field>& targetField);

		#ifdef Polish_dox
		/**
		 * Funkcja ataku na wroga armie gdy ta jest w pewnym oddaleniu.
		 * Powoduje przesuniecie sie armii na pole sasiadujace z atakowana armia.
		 * Jesli zostanie osiagniete pole sasiadujace, z ktorego mozna przeprowadzic atak,
		 * toczona jest bitwa.
		 * @param enemyArmy Wroga armia - cel ataku.
		 */
		#endif
		void goAfter(const std::shared_ptr<Army>& enemyArmy);
		#ifdef Polish_dox
		/**
		 * Funkcja ucieczki od wrogiej armii. Ucieczka postepuje w kierunku 'od' wrogiej armii.
		 * Maksymalny ruch armii w tej funkcji to jedno pole (dlatego nalezy ja wywolywac
		 * dopoki silniejsza wroga armia znajduje sie w zasiegu wzroku).
		 * @param enemyArmy Wroga armia - obiekt, od ktorego postepuje ucieczka.
		 */
		#endif
		void flee(const std::shared_ptr<Army>& enemyArmy);

		std::shared_ptr<Town> selectClosestTown() const;

		bool mergeWithArmy(const std::shared_ptr<Army>& targetArmy);

		bool claimThatBuilding(const std::shared_ptr<Building>& targetBuilding);
		bool claimThatTown(const std::shared_ptr<Town>& targetTown);

		void setArmyToTheTown(const std::shared_ptr<Army>& army);
		void setArmyToTheSpottedBuilding(const std::shared_ptr<Army>& army);
		void resetArmyToTheTown();
		void resetArmyToTheSpottedBuilding();
		const std::shared_ptr<Army>& getArmyToTheTown() const;
		const std::shared_ptr<Army>& getArmyToTheSpottedBuilding() const;

		#ifdef Polish_dox
		/**
		 * Funkcja realizujaca losowy ruch armii. Przemieszcza armie na sasiadujace z obecnym pole.
		 */
		#endif
		void wander();

		#ifdef Polish_dox
		/**
		 * Funkcja pozwalajaca na zakup jednostek. Klasa jednostek wybierana jest losowo.
		 * Zostanie zakupiona maksymalna ilosc jednostek, na jakie gracz moze sobie w tej chwili pozwolic.
		 */
		#endif
		void buyTroops();

		int getNumberOfArmies() const;

		#ifdef Polish_dox
		/**
		 * Funkcja szacuje na podstawie liczby jednostek broniacych miasto czy jest ono silniejsze od armii.
		 * @param enemyTown Miasto, ktorego szacujemy sile.
		 * @return Jesli obrancy miasta sa liczniejsi od armii, zwraca true.
		 * W przeciwnym wypadku, zwraca false;
		 */
		#endif
		bool isTownDefencesStronger(const std::shared_ptr<Town>& enemyTown) const;

		void select() { _selected = true; }
		void deselect() { _selected = false; }
		bool isSelected() const { return _selected; }

		#ifdef Polish_dox
		/**
		 * Przeladowany operator porownuje obiekty Army na podstawie ich polozenia na mapie (Object::_currentField)
		 * oraz na podstawie identyfikatora nadawanego kazdemu obiektowi Object (Object::_id)
		 */
		#endif
		bool operator==(const Army&) const;

		// -------------------------------------------------------------------------FSM
		ScriptedStateMachine<Army>* getFSM() const
		{
			return (_stateMachine);
		}

		void updateMoveD(int cost) {
			_moveD -= cost;
		}

		void restoreMoveD() {
			_moveD = getMinRange();
		}

		int checkRefCount(const std::shared_ptr<Army>& p)
		{
			return p.use_count();
		}

		#ifdef Polish_dox
		/**
		 * Zwraca ilosc jednostek i-tego poziomu w armii.
		 * @param int i Poziom
		 * @return int Ilosc jednostek
		 */
		#endif
		int getCreaturesAmount(int i) {
			return _creatures[i - 1];
		}
};

#endif
