#ifndef SHAREDPTRCONVERTER_HPP_
#define SHAREDPTRCONVERTER_HPP_

#include <memory>

namespace luabind { namespace detail { namespace has_get_pointer_ {
    template<class T>
    T * get_pointer(std::shared_ptr<T> const& p) { return p.get(); }
}}}

namespace std { namespace tr1 {
    template<class T>
    T * get_pointer(shared_ptr<T> const& p) { return p.get(); }
}}

#endif
