#ifndef GAME_HPP_
#define GAME_HPP_

#define English_dox

extern "C"
{
  #include <lua.h>
  #include <lualib.h>
  #include <lauxlib.h>
}

#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include <memory>
#include <algorithm>
#include <string>
#include <ctime>

#include "StringConverter.hpp"
#include "ResourceManager.hpp"
#include "Field.hpp"
#include "StaticObject.hpp"
#include "Mill.hpp"
#include "GoldMine.hpp"
#include "OrePit.hpp"
#include "Sawmill.hpp"
#include "Army.hpp"
#include "Town.hpp"
#include "Farm.hpp"
#include "json.h"
#include "MusicPlayer.hpp"

class Game {
public:

	#ifdef Polish_dox /**
	 * Struktura wewnetrzna Config sluzaca do przechowywania danych wspoldzielonych pomiedzy roznymi czesciami gry.
	 * Najczesciej wykorzystywana przy przekazywaniu danych pomiedzy warstwami.
	 * Struktura powinna skladac sie tylko z typow prostych i obiektow klas zdefiniowanych w bibliotekach
	 * (czyli nie powinny znalezc sie w niej klasy napisane przez nas na potrzeby tej gry)
	 * Dzieki temu struktury Config mozna uzywac w dowolnej czesci programu, nie bojac sie o zaleznosci pomiedzy klasami.
	 */
	#endif
#ifdef English_dox
	 /**
	  * Internal structure, contains data shared within different areas of game.
	  */
#endif
	typedef struct Config {
		#ifdef Polish_dox /**
		 * Wskaznik do okna aplikacji.
		 */
		#endif
		#ifdef English_dox
		 /**
		  * Pointer to apps window
		  */
		#endif
		std::shared_ptr<sf::RenderWindow> window;

		#ifdef Polish_dox 
		/**
		 * Widok odpowiedzialny za rysowanie rzeczy zwiazanych z plansza.
		 * Ten widok bedzie przesuwany i skalowany.
		 */
		#endif
		#ifdef English_dox
		 /**
		  * View responsible for drawing things related to board.
		  */
		#endif
		sf::View boardView;

		#ifdef Polish_dox 
		/**
		 * Glowny widok aplikacji.
		 */
		#endif
		#ifdef English_dox
		 /**
		  * Main view of application.
		  */
		#endif
		sf::View mainView;

		#ifdef Polish_dox
		/**
		 * Wektor przechowujacy rozmiar planszy (w ilosci pol)
		 * Np. {50,50} oznacza 50 pol szerokosci i 50 pol wysokosci.
		 */
		#endif
		#ifdef English_dox
		 /**
		  * Cointains size of board measured in fields
		  */
		#endif
		sf::Vector2i boardSize;

		#ifdef Polish_dox 
		/**
		 * Wektor przechowujacy przesuniecie widoku boardView wzgledem (0,0)
		 */
		#endif
		#ifdef English_dox
		 /**
		  * Vector which holdes a move of view boardView in reference to (0,0)
		  */
		#endif
		sf::Vector2f boardMove;

		#ifdef Polish_dox 
		/**
		 * Rozmiar pojedynczego kafelka.
		 */
		#endif
		#ifdef English_dox
		 /**
		  * Size of single field
		  */
		#endif
		int fieldSize;

		#ifdef Polish_dox 
		/**
		 * Ilosc pol przypadajacych na jeden ekran.
		 */
		#endif
		#ifdef English_dox
		 /*
		  * Amount of fields on screen.
		  */
		#endif
		sf::Vector2i fieldsOnScreen;

		#ifdef Polish_dox 
		/**
		 * Poziom oddalenia mapy [0-15]. Wiekszy zoomLevel oznacza widocznosc wiekszego fragmentu mapy.
		 */
		#endif
		#ifdef English_dox
		 /**
		  * Level of zoom
		  */
		#endif
		int zoomLevel;

		#ifdef Polish_dox 
		/**
		 * Pole, ktore zostalo ostatnio zaznaczone.
		 */
		#endif
		#ifdef English_dox
		 /**
		  * clicked field
		  */
		#endif
		std::shared_ptr<Field> clickedField;

		#ifdef Polish_dox 
		/**
		 * Pole, ktorego armia zostala wybrana do ruchu.
		 */
		#endif
		#ifdef English_dox
		 /**
		  * army selected for movement
		  */
		#endif
		std::shared_ptr<Field> selectedArmy;

		#ifdef Polish_dox 
		/**
		 * Obiekt SFGUI sluzacy do rysowania okienek dialogowych z SFGUI.
		 */
		#endif
		#ifdef English_dox
		 /**
		  * SFGUI object needed to handle GUI
		  */
		#endif
		std::shared_ptr<sfg::SFGUI> sfgui;

		#ifdef Polish_dox 
		/**
		 * Pulpit umozliwiajacy ukladanie okienek dialogowych warstwowo (jedno na drugim).
		 */
		#endif
		#ifdef English_dox
		 /**
		  * desktop which allows for proper window handling
		  */
		#endif
		std::shared_ptr<sfg::Desktop> desktop;

		#ifdef Polish_dox 
		/**
		 * Wektor kolorow graczy.
		 */
		#endif
		#ifdef English_dox
		 /**
		  * vector of players color
		  */
		#endif
		std::vector<sf::Color> armyColor;

		#ifdef Polish_dox 
		/**
		 * Informacja dla warstw, czy wybrano nowego gracza.
		 */
		#endif
		#ifdef English_dox
		 /**
		  * Information if next player was picked
		  */
		#endif
		bool nextPlayer = false;
	} Config;

	#ifdef Polish_dox 
	/**
	 * Lista dostepnych poziomow trudnosci
	 */
	#endif
		#ifdef English_dox
	 	 /**
	 	  *List of aviable levels
	 	  */
		#endif
	enum DifficultyLevels {
		VERYEASY,//!< VERYEASY
		EASY,    //!< EASY
		NORMAL,  //!< NORMAL
		HARD,    //!< HARD
		VERYHARD //!< VERYHARD
	};
	#ifdef Polish_dox 
	/**
	 * aktualny poziom trudnosci
	 */
	#endif
	#ifdef English_dox
	 /**
	  * actual level
	  */

	#endif
	int difficulty = NORMAL;

	Game(std::shared_ptr<sf::RenderWindow>, TextureHolder&);
	Game(std::shared_ptr<sf::RenderWindow> w, TextureHolder& t,std::string fileName);
	virtual ~Game();

	void processKey(sf::Event &);
	virtual int getGameState();
	virtual void setGameState(int);
	virtual int isEnd();
	virtual void end(int player=99);

	virtual Game::Config& getConfig();
	virtual std::shared_ptr<Field> getField(int, int) const;

	void setDefaultCommandMap();

	#ifdef Polish_dox 
	/**
	 * @brief NIEPRZETESTOWANE
	 * @param Pole dla ktorego chcemy pobrac sasiadowotoczenie
	 * @return Wektor sasiadow podanego pola.
	 */
	#endif
		#ifdef English_dox
			/**
		 * @brief NOT TESTED
		 * @param selected field
		 * @return vector of neighbor fields
		 */
		#endif
	std::vector<std::shared_ptr<Field> > getFieldNeighbours(std::shared_ptr<Field>) const;

	#ifdef Polish_dox 
	/**
	 * @brief Wektor przechowuje sasiadow w kolejnosci innej niz bysmy sobie tego zyczyli, tj. v[0] = NW, v[1] = W, v[2] = SW, v[3] = N, v[4] = center,
	 * 		v[5] = S, v[6] = NE,  v[7] = E, v[8] = SE.
	 * @param Wspolrzedne (takie jak planszy tj. [0, 100), nie SFML'a [0, rozdzielczosc_ekranu)) pola dla ktorego chcemy dostac sasiadow
	 * @return Wektor sasiadow podanego pola.
	 */
	#endif
	#ifdef English_dox
	/**
	* @brief Holds neighboring fields in this way: v[0] = NW, v[1] = W, v[2] = SW, v[3] = N, v[4] = center,
	 * 		v[5] = S, v[6] = NE,  v[7] = E, v[8] = SE.
	 * @param Coordinates of field
	 * @return vector of neighboring fields
	 */
	#endif
	std::vector<std::shared_ptr<Field> > getFieldNeighbours(sf::Vector2i) const;

	#ifdef Polish_dox 
	/**
	 * Przeskakuje do nastepnego gracza. Gdy gracz nie posiada jednostek ani budynkow to go zabija.
	 * Przy okazji sprawdza warunki zwyciestwa (wywoluje funkcje checkIfWon( ) ).
	 * Gdy nie ma wiecej graczy na liscie wywoluje funkcje nextTurn( )
	 */
	#endif
	#ifdef English_dox 
	/**
	 * goes to next player
	 */
	 #endif
	void nextPlayer();

	#ifdef Polish_dox 
	/**
	 * Funkcja sprawdzajaca czy dany Player wygral gre.
	 * W razie potrzeby latwo mozna zmieniac/dodawac nowe warunki zwyciestwa.
	 * @param player wskaznik na aktualnego gracza
	 * @return true gdy wygral, false gdy nie
	 */
	 #endif
		#ifdef English_dox 
		/**
	 * Checks victory status
	 * @param player actual player
	 * @return true if player won
	 */
	#endif
	bool checkIfWon(std::shared_ptr<Player> player);

	#ifdef Polish_dox 
	/**
	 * Nowa tura - przechodzimy do pierwszego gracza i uaktualniamy zasoby oraz ruch jednostek.
	 */
	#endif
     #ifdef English_dox 
	 /**
	 * New round
	 */
	#endif
	void nextTurn();

	#ifdef Polish_dox 
	/**
	 *
	 * @return numer aktualnej tury
	 */
	#endif
	#ifdef English_dox 
	/**
	 *
	 * @return actual round number
	 */
	#endif
	int getTurn();

	#ifdef Polish_dox 
	/**
	 * Funkcja do otrzymywania aktualnego gracza.
	 * @return Wskaznik do gracza ktory jest aktualnie aktywny.
	 *
	 */
	#endif
	#ifdef English_dox 
	/**
	 * retrieves current player
	 * @returnpointer to current player
	 *
	 */
	#endif
	std::shared_ptr<Player> getCurrentPlayer(){return _players[_currentPlayer];}
	#ifdef Polish_dox
	/*
	 *@fn std::string Game::getFromJSON(const std::string JSONHandler, const int TownIdValue, const std::string& JSONKey)
	 *Magiczna funkcja wczytujaca stringa z JSONa wg podanego id.
	 *
	 */
	#endif
	Json::Value getFromJSON(const std::string JSONHandler, const int TownIdValue, const std::string& JSONKey);

	#ifdef Polish_dox 
	/**
	 * Funkcja wczytuje plik .json do pojedynczego stringa, ktorego nastepne zwraca.
	 * Wczytywanie powinno byc tak szybkie jak obsluga pliku w 'klasycznym C':
	 * http://insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html
	 * @param filename Scie�ka do pliku JSON
	 * @return Pojedynczy string, w kt�rym siedzi ca�y plik JSON
	 */
	#endif
	static std::string prepareJSONFile(const std::string& filename);
	#ifdef Polish_dox 
	/**
	 * Funkcja �aduje map� z pliku JSON, ktorego dostaje w postaci jednego stringa jako pierwszy argument
	 * Inicjalizuje pole _board i _config.boardSize.
	 * Uzupe�nia pole Field::_neighbourFields ka�dego z element�w kontenera _board wywo�uj�c funkcj� setNeighbourFields() dla ka�dego z nich.
	 * W cz�ci powiela funkcjonalnosc funkcji getFromJSON() - do zaniedbania albo pozniejszej poprawki.
	 * @param JSONHandler Plik JSON spakowany do pojedynczego stringa funkcj� prepareJSONFile().
	 */
	#endif
	void loadMapFromJson(const std::string& JSONHandler);
    #ifdef Polish_dox
	/*
	 *@fn void Game::racesInicialization()
	 *Inicjalizacja ras, wykonywana w konstrukorze klasy Game. Wczytuje wszystkie rasy i wstawia do wektora ras. Zaklada 10 jako maksymalne id rasy, 100 jako maksymalne id jednostki oraz 20 jako maksymalne id bohatera, można to zmienic gdyby byla szansa ze bedziemy miec wieksze id niz zalozone przezemnie.
	 *
	 */
	#endif
	#ifdef English_dox
	/*
	 *@fn void Game::racesInicialization()
	 * Initialisation of races
	 *
	 */
	#endif
	void racesInicialization();
	#ifdef Polish_dox
	/*
	 *@fn std::shared_ptr<Race> Game::getRace(int id)
	 *@param id
	 *Zwraca rase wg podanego id
	 */
	#endif
	#ifdef English_dox
	/*
	 *@fn std::shared_ptr<Race> Game::getRace(int id)
	 *@param id
	 *@return race of given id
	 */
	#endif
	std::shared_ptr<Race>  getRace(int id);

	#ifdef Polish_dox 
	/**
	 * Metoda zamykajaca aktualne okienko sfgui.
	 * Jest to tak naprawde zdublowanie funkcjonalnosci z Layer::closeCurrent()
	 * Niestety, nie mam dostepu do tamtej funkcji z Game.
	 */
	#endif
	#ifdef English_dox 
	/**
	 * closes current SFGUI window
	 */
	#endif
	void closeCurrent();

	#ifdef Polish_dox 
	/**
	 * Metoda pozwalajaca pobrac dane z pliku json
	 * @param path Nazwa pliku (wraz z katalogiem).
	 * @param key Nazwa pola w pliku.
	 * @return Json::Value Wartosc JSON, ktora nalezy zrzutowac na odpowiedni format.
	 */
	#endif
	#ifdef English_dox 
	/**
	 * Allows to get data from JSON file
	 * @param path name of the file, with catalogue
	 * @param key Name of the field in file.
	 * @return Json::Value Json value, needs to ge converted.
	 */
	#endif
	Json::Value getDataFromFile(std::string path, std::string key, int i = -1);

	/**
	 * Metoda pozwalajaca pobrac dane z std::string w formacie JSON
	 * @param path String z zawartoscia w formacie JSON
	 * @param key Nazwa pola w stringu.
	 * @return Json::Value Wartosc JSON, ktora nalezy zrzutowac na odpowiedni format.
	 */
	Json::Value getDataFromString(std::string &str, std::string key, int i = -1);

	void save(std::string fileName);

	TextureHolder& textures;

	const std::vector<std::shared_ptr<Player>>& getPlayers() const
	{
		return _players;
	}

	const std::vector<std::vector<std::shared_ptr<Field> > >& getBoard() const
	{
		return _board;
	}

	lua_State* getLuaState() const
	{
		return pLua;
	}

	const luabind::object& getLuabindStates() const
	{
		return states;
	}

	#ifdef Polish_dox 
	/**
	 * Metoda zwracajaca "neutralnego gracza".
	 */
	#endif
	#ifdef Polish_dox 
	/**
	 * @return neutral player
	 */
	#endif

	std::shared_ptr<Player> getNeutralPlayer() {
		return _neutralPlayer;
	}
	
	void setMusicPlayer(std::shared_ptr<MusicPlayer> musicPlayer){ _musicPlayer=musicPlayer;}

protected:
	unsigned int _currentPlayer = 0;
	int _gameState;
	std::shared_ptr<sf::RenderWindow> _window;
	sf::Vector2i _mousePosition;
	std::map<std::string,int> _commandMap;
	int _end = -1;
	Config _config;

	std::vector<std::vector<std::shared_ptr<Field>>> _board;

	int _turn;
	std::vector<std::shared_ptr<Player>> _players;
	std::vector<std::shared_ptr<Race>> _races;

	luabind::object states;

	#ifdef Polish_dox 
	/**
	 * Gracz neutralny - nie bierze udzialu w grze (tzn. nie ma swojej tury, zasobow itp.)
	 * Gracz ten jest wlascicielem "bezpanskich" budynkow i miast, dzieki temu nie bedzie problemu
	 * z ewentualnym nullptr przy getOwner().
	 */
	#endif
	#ifdef English_dox /**
	 * Neutral player
	 */
	#endif
	std::shared_ptr<Player> _neutralPlayer;
	std::shared_ptr<MusicPlayer> _musicPlayer;
	std::string _mapPath;

private:
	//std::shared_ptr<lua_State> pLua;
	lua_State* pLua;

	void runLuaScript(lua_State* L, const char* script_name);
	void registerScriptedStateMachineWithLua(lua_State* pLua);
	void registerObjectWithLua(lua_State* pLua);
	void registerActiveObjectWithLua(lua_State* pLua);
	void registerArmyWithLua(lua_State* pLua);

	std::shared_ptr<Player> getPlayer(std::string);
	
};

#endif /* GAME_HPP_ */
