#ifndef RACE_
#define RACE_

#include <vector>
#include <map>
#include <memory>
#include <string>
#include <fstream>
#include "Creature.hpp"
#include "Hero.hpp"
/*
*@class	Race
*Klasa reprezentujaca rase.
*_creatures wektor jednostek danej rasy. (zakladamy ze bedzie 5 jednostek, wazne jest ze przy dodawaniu nie sprawdzamy czy nie ma juz jednostki danego poziomu, wiec jesli JSON bedzie zle zrobiony bedziemy miec dwie jednostki poziomu 1, w takim przypadku i tak brana pod uwage bedzie pierwsza jednostka)
*_name nazwa rasy
*_id numer identyfikacyjny rasy
* Mozna by tu dodawac grafiki konkretnych ras, dzwiekow czy co tam bedzie do nich przypisane
*



*/

class Race {
	private:
		std::vector<std::shared_ptr<Creature>> _creatures;
		std::string _name;
		int _id;
		std::vector<std::shared_ptr<Hero>> _heroes;
		int _textureId;
	///\	std::string _fileName;
	///\	std::fstream _file;

	public:

/*
*@fn Race(std::string name, int id)
*Konstruktor, pobiera nazwe i id danej rasy.
*/
		Race(std::string name, int id);

/*
*@fn setCreature(std::shared_ptr<Creature> creature)
*Dodaje jednostke do wektora.
*/
		std::shared_ptr<Creature> getCreature(int lv);
/*
*@fn getCreature(int lv)
*Zwraca jednostke danego poziomu.
*/
		void setCreature(std::shared_ptr<Creature> creature);
		void printRace(); //tekstowa reprezentacja rasy
		void printRaceName(){}
		int getId(){return _id;}
		std::string getName() const { return _name; }

		/**
		 * Ustawia numer tekstury miasta.
		 * @param Nowe id tekstury.
		 */
		void setTextureId(int id) { _textureId = id; }

		/**
		 * Zwraca numer tekstury miasta.
		 * @return Numer tekstury
		 */
		int getTextureId() const { return _textureId; }

		void setHero(std::shared_ptr<Hero> hero);
};

#endif
