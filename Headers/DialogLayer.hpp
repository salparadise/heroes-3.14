#ifndef DIALOGLAYER_HPP_
#define DIALOGLAYER_HPP_

#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include <memory>
#include "Layer.hpp"
#include "Field.hpp"
#include "Town.hpp"
#include "TownLayer.hpp"
#include "StringConverter.hpp"

#ifdef Polish_dox /**
 * Klasa DialogLayer odpowiadajaca za obsluge okienek dialogowych i opcji jednostek / budynkow.
 */
#endif
#ifdef  English_dox
	/**
	 * Handles Dialog windows
	 */
#endif
class DialogLayer : public Layer {
public:
	DialogLayer(std::shared_ptr<Game>);
	virtual ~DialogLayer() {}

	#ifdef Polish_dox /**
	 * Metoda processEvent przechwytuje odpowiednie zdarzenia.
	 * W przypadku warstwy DialogLayer oznacza to klikniecie na ktorys z elementow GUI.
	 *
	 * @param sf::Event& Aktualnie przetwarzane zdarzenie.
	 */
	#endif
	#ifdef English_dox /**
	 * Process given event
	 *
	 * @param sf::Event& given vent to handle
	 */
	#endif
	virtual bool processEvent(sf::Event&);

	#ifdef Polish_dox /**
	 * Metoda draw wyswietla wszystkie aktywne okienka dialogowe.
	 *
	 * @param sf::RenderTarget& Okno docelowe po ktorym warstwa rysuje.
	 * @param sf::RenderStates Dodatkowe informacje.
	 */
	#endif
	#ifdef English_dox /**
	 * Draws all active windows
	 *
	 * @param sf::RenderTarget& Target window to be printed on
	 * @param sf::RenderStates Additional data
	 */
	#endif
	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

	#ifdef Polish_dox /**
	 * Metoda aktualizuje stan okienek:
	 * - jesli wydano rozkaz tworzenia nowego okienka, wywoluje metode createPopup()
	 * - dba o wyswietlanie okienek w odpowiednim miejscu.
	 *
	 * @param sf::Time Czas od ostatniej aktualizacji stanu gry.
	 */
	#endif
	#ifdef English_dox /**
	 * Updates state of the windows
	 *
	 * @param sf::Time time from last update of the state of the game
	 */
	#endif
	virtual void update(sf::Time dt);

	virtual void closeCurrent();

	#ifdef Polish_dox /**
	 * Metoda wyswietlajaca ilosc jednostek w wybranej armii.
	 */
	#endif
	#ifdef Polish_dox /**
	 * Views number of units in selected army
	 */
	#endif
	void numberOfTroops();

	#ifdef Polish_dox /**
	 * Metoda usuwajaca aktualnie wybrany budynek.
	 * Jest umieszczona w DialogLayer, poniewaz tylko stad mamy dostep
	 * zarowno do Field, Building jak i Player.
	 */
	#endif
	#ifdef English_dox /**
	 * Deletes current chosen building.
	 */
	#endif
	void destroyBuilding();

protected:
	#ifdef Polish_dox /**
	 * Metoda pozwala stworzyc okienko dialogowe (pop-up) dla budynkow i jednostek.
	 * Korzysta ona z informacji zawartych wewnatrz Game::Config.
	 */
	#endif
	#ifdef English_dox /**
	 * Allows to create popup for buildings and unit.
	 * Uses data from Game::Config.
	 */
	#endif
	void createPopup();

	#ifdef Polish_dox /**
	 * Wskaznik do pop-upa.
	 */
	#endif
	#ifdef English_dox /**
	 * Pointer to popup
	 */
	#endif

	sfg::Window::Ptr _gui;
};

#endif /* DIALOGLAYER_HPP_ */
