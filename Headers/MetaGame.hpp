#ifndef METAGAME_HPP_
#define METAGAME_HPP_

#include <memory>
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Audio.hpp>
#include <tuple>
#include <list>
#include <dirent.h>
#include <string>
#include <set>
#include <iostream>
#include "Game.hpp"
#include "HotSeatGame.hpp"
#include "Layer.hpp"
#include "TopLayer.hpp"
#include "DialogLayer.hpp"
#include "SidebarLayer.hpp"
#include "FogOfWarLayer.hpp"
#include "GroundLayer.hpp"
#include "MetaLayer.hpp"
#include "NetworkGame.hpp"
#include "SinglePlayerGame.hpp"
#include "TownLayer.hpp"
#include "StringConverter.hpp"
#include "MusicPlayer.hpp"

class MetaGame {
public:
	MetaGame();
	void gameLoop();
	void metaLoop();



	int state = MAINMENU;
	enum GameState {
		META,
		MAINMENU,
		HOTSEAT,
		GAME,
		QGAME,
		EXIT,
		SAVE,
		LOAD,
		HELP,
		QUIT,
		RESOLUTION,
		NETWORK
	};


private:
	#ifdef Polish_dox /**
	 * Obsluguje event klawiaturowy w menu glownym
	 *
	 */
	#endif
	#ifdef English_dox /**
	 * Handles key event
	 *
	 */
	#endif
	bool handleKeyEvent(sf::Event &e);
	#ifdef Polish_dox /**
	 * Tworzy gui menu glownego
	 */
	#endif
	#ifdef English_dox /**
	 * Creates main menu GUI
	 */
	#endif
	void createMenu();
	#ifdef Polish_dox /**
	 * czysci gui
	 */
	#endif
	#ifdef English_dox /**
	 * clears gui
	 */
	#endif
	void clearMenu();
	#ifdef Polish_dox /**
	 * tworzy okno do wyboru imion nowego playera
	 *
	 * @param Informacja o tym, czy wlaczamy gre singleplayer (true) czy hotseat (false - domyslnie).
	 */
	#endif
	#ifdef English_dox /**
	 * Creates window for choosing players
	 *
	 * @param Information about game mode (singleplayer = true, hotseat = false - default)
	 */
	#endif
	void createSelectPlayers(bool single = false,bool ca = false);

	#ifdef Polish_dox /**
	 * zmienia stan tego obiektu na QUIT
	 */
	#endif
	#ifdef English_dox /**
	 * Changes actual states to  QUIT
	 */
	#endif

	void exit(){ state=QUIT;}
	#ifdef Polish_dox /**
		 * zmienia stan tego obiektu na HOTSEAT
		 */
	#endif
	#ifdef English_dox /**
		 * Changes actual states to Hotseat
		 */
	#endif
	void hotseat(){ state=HOTSEAT;}
	#ifdef Polish_dox /**
		 * zmienia stan tego obiektu na QGAME
		 */
	#endif
	#ifdef English_dox /**
		 * Changes actual states to QGAME
		 */
	#endif
	void qgame(){ state=QGAME;}
	#ifdef Polish_dox /**
		 * zmienia stan tego obiektu na  MAINEMNU
		 */
	#endif
	#ifdef English_dox /**
		 * Changes actual states to MAINMENU
		 */
	#endif
	void mainmenu() {state=MAINMENU;}
	#ifdef Polish_dox /**
		 * zmienia stan tego obiektu na  LOAD
		 */
	#endif
	#ifdef English_dox /**
		 * Changes actual states to LOAD
		 */
	#endif
	void load() {state=LOAD;}
	
	
	#ifdef Polish_dox /**
	 * Wektor z polami w ktore s� wprowadzone nicki graczy
	 */
	#endif
	#ifdef English_dox /**
	 * Vector of players nicknames
	 */
	#endif

	void OnComboSelect();
	#ifdef English_dox /**
	 * Views options window
	 */
	#endif
	void options();
	#ifdef English_dox /**
	 * Sets resolution
	 */
	#endif
	void setResolution();
	#ifdef English_dox /**
	 * Loads map
	 */
	#endif
	void loadMap();

	#ifdef English_dox /**
	 * Gets maximum number of players from json file in string format
	 * @argument const std::string & JSON file with map in it
	 */
	#endif

	int getMaximumNumberOfPlayersFromJSON(const std::string &);
	#ifdef English_dox /**
	 * Gets list of aviable races
	 */
	#endif
	void getRacesList();


	void owieleLepszePobieranieRasNizWGameIWOgole();

	void getCfg();
	void updateCfg();


	#ifdef English_dox /**
	 * Function responsible for listing files from directory.\n
	 * Sample usage: listFiles("./Saves/")
	 * @param directory name of directory for which files should be listed
	 * @return std::set <std::string> containing of file names (and directories if exists) with extensions
	 */
	#endif

	std::set <std::string> listFiles(std::string directory);

	static TextureHolder textures;
	static FontHolder fonts;
private:
	void loadResources();

	std::shared_ptr<sf::RenderWindow> _window;
	std::list<std::shared_ptr<Layer>> _layers;
	sfg::SFGUI _sfgui;
	//std::shared_ptr<MainMenu> _mainMenu;
	std::shared_ptr<sfg::Window> _gui;
	sfg::Desktop _desktop;
	std::shared_ptr<Game> _game;
	std::string _rozdzielczosc;
	sfg::ComboBox::Ptr _resolution;
	sfg::Entry::Ptr _mapName;
	sfg::Label::Ptr _mapLoadEffect;
	std::vector<std::tuple<sfg::Entry::Ptr,sfg::ComboBox::Ptr,sfg::CheckButton::Ptr> > _playerGui;
	std::string mapa;
	std::vector<std::pair<int,std::string>> _playerRaces;
		std::vector<sfg::Box::Ptr> _raceBoxes;
	std::shared_ptr<MusicPlayer> _musicPlayer;

	sfg::ResourceManager _sfgResourceManager;

	bool campa;
	int _winner;
	int _lastWonCampaign;

	#ifdef Polish_dox /**
	 * ComboBox pozwalajacy wybrac mape z listy dostepnych.
	 * Wszystkie mapy powinny byc wrzucone do katalogu Resources/maps/
	 */
	#endif
	#ifdef English_dox /**
	 * Allows to choose map from aviable list
	 */
	#endif

	/**
	 * Tlo menu.
	 */
	sf::Sprite _background;

	#ifdef Polish_dox
	/**
	 * MusicPlayer do odtwarzania muzyki i efektow dzwiekowych
	 */
	#endif
	std::shared_ptr<MusicPlayer> _metaMusicPlayer;
	
	sfg::ComboBox::Ptr _availableMaps;

	#ifdef Polish_dox /**
	 * Metoda, ktora tworzy tabele startowa do gry (wybor graczy i ras).
	 *
	 * @param Informacja o tym, czy wlaczamy gre singleplayer (true) czy hotseat (false - domyslnie).
	 */
	#endif
	#ifdef English_dox /**
	 * Creates starting table (players and races)
	 * @param Game mode (false = singleplayer)
	 */
	#endif
	void createNewGameTable(bool single = false);

	#ifdef Polish_dox /**
	 * Metoda pozwalajaca pobrac dane mapy.
	 * @return Json::Value Wartosc JSON, ktora nalezy zrzutowac na odpowiedni format.
	 */
	#endif
	#ifdef English_dox /**
	 * Gets map
	 * @return Json::Value JSON value, needs to be properlu converted
	 */
	#endif
	Json::Value getMapData(std::string path, std::string key);

	#ifdef Polish_dox /**
	 * Metoda wyswietlajaca menu z mozliwoscia wyboru rodzaju gry sieciowej
	 * (jako serwer lub jako klient).
	 */
	#endif
	#ifdef English_dox /**
	 * creates menu with network options
	 */
	#endif
	void networkMenu();


	#ifdef Polish_dox /**
	 * Metoda realizuj�ca zadanie serwera - akceptowanie graczy.
	 */
	#endif
	#ifdef English_dox /**
	 * Network game with server mode
	 */
	#endif
	void serverGame();

	#ifdef Polish_dox /**
	 * Metoda clienta sluzaca do polaczenia z serwerem.
	 */
	#endif
	#ifdef English_dox /**
	 * Network game with client mode
	 */
	#endif
	void clientGame();

	#ifdef Polish_dox /**
	 * Metoda pomocnicza, ustawia pozycje okienka na srodek ekranu.
	 */
	#endif
	#ifdef English_dox /**
	 * Sets position of window into center of the screen
	 */
	#endif
	void guiSetPosition();

	#ifdef Polish_dox /**
	 * Metoda serwera czekajaca na przylaczenie sie graczy do gry.
	 */
	#endif
	#ifdef English_dox /**
	  * Network game waits for player to connect
	 */
	#endif
	void serverConnectPlayers();
};

#endif /* METAGAME_HPP_ */
