#ifndef NETWORK_HPP_
#define NETWORK_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <memory>
#include <algorithm>
#include <string>
#include "Game.hpp"

class NetworkGame : public Game {
	private:
		/**
		 * Adres IP serwera.
		 */
		sf::IpAddress _ipAddress;

		/**
		 * Port, pod ktorym dostepny jest serwer.
		 */
		int _port;

		/**
		 * Tylko dla gry jako serwer. Socket, na ktorym nasluchujemy nowych klientow i wiadomosci.
		 */
		sf::TcpListener _listener;

	public:
		/**
		 * Konstruktor klasy NetworkGame. Zasadniczo nie powinien byc uzywany.
		 * (Zostal stworzony dla zachowania konwencji).
		 *
		 * @param std::shared_ptr<sf::RenderWindow) Obiekt RenderWindow, w ktorym rysowana bedzie gra.
		 */
		NetworkGame(std::shared_ptr<sf::RenderWindow> w);

		/**
		 * Konstruktor klasy NetworkGame przyjmujacy jako argumenty port i adres IP gry sieciowej.
		 * Gra siecowa realizwana jest za pomoca pakietow TCP, dzieki czemu jedyna sytuacja krytyczna jest
		 * utrata polaczenia na linii serwer - klient.
		 * Pierwszym podlaczonym graczem powinien byc serwer. Nastepnie, z adresem IP serwera probuja sie laczyc
		 * klienci. Serwer oczekuje na klientow dopoki nie uplynie okreslony czas oczekiwania lub liczba graczy
		 * osiagnie maksymalna - 1 (-1, bo jednym z graczy jest serwer).
		 *
		 * @param std::shared_ptr<sf::RenderWindow> Obiekt RenderWindow, w ktorym rysowana bedzie gra.
		 * @param int Port, ktory pozwala na komunikacje z serwerem.
		 * @param sf::IpAddress Adres IP serwera w sieci lokalnej (nie planujemy obslugi gry przez Internet).
		 */
		NetworkGame(std::shared_ptr<sf::RenderWindow> w, int port, sf::IpAddress ip = "");

		
}; 

#endif /* NETWORK_HPP_ */
