/**
 * @file PathFinding.hpp
 *
 * @date 8 gru 2013
 * @author Wojtek
 * @version 1.0
 *
 */

#ifndef PATHFINDING_HPP_
#define PATHFINDING_HPP_
#include "Field.hpp"
#include <map>
#include <vector>
#include <memory>
#include <algorithm>

#ifdef English_dox
/**
 * Class used to calculate unit possible movement and/or path to destination field from selected field.
 */
#endif
class PathFinding {

	public:
		PathFinding();

		virtual ~PathFinding();

#ifdef English_dox
		/**
		 * Struct representing node of single field by which route may lead. It consists of:\n
		 * three int numbers : 	g - cost of movement to this field\n
		 * 						h - heuristic cost of movement to destination field\n
		 * 	 					f - sum of g and h\n
		 * std::shared_ptr<Field> - pointer to this field\n
		 * two pointers to parent and child node (field)
		 */
#endif
		struct Node {
			int f_;
			int g_;
			int h_;


			std::shared_ptr<Field> field_;

			std::shared_ptr<Node> parent_;

			Node() : f_(0), g_(0), h_(0), field_(nullptr), parent_(nullptr) {}
			Node(int f, const std::shared_ptr<Field> field) : f_(f), g_(0), h_(0), field_(field), parent_(nullptr) {}
			Node(const std::shared_ptr<Field> field) : f_(0), g_(0), h_(0), field_(field), parent_(nullptr) {}
			Node(int f, int g, int h, const std::shared_ptr<Field> field, const std::shared_ptr<Node> parent)
												: f_(f), g_(g), h_(h), field_(field), parent_(parent) {}
		};

#ifdef English_dox
		/**
		 * @brief struct (functor) used to compare two nodes for smallest movement cost function f
		 */
#endif
		struct NodeCompare {
			bool operator() ( const Node x, const Node y ) const {
				return x.f_ < y.f_;
			}
		 };

#ifdef English_dox
		/**
		 * @brief struct (functor) used for searching if Node x has pointer to Field field_ inside
		 */
#endif
		struct HasField {
			std::shared_ptr<Field> field_;
			bool operator() (const Node x) const {
				return field_ == x.field_;
			}
		};


#ifdef English_dox
		/**
		 * 	Showing fields which are reachable by division in this turn.
		 *
		 * @param startField Field where army stands
		 * @param range Range of i-th division of Army
		 * @return std::vector consisting of pointers to fields which division can reach
		 */
#endif
		std::vector<std::shared_ptr<Field>> showPossibleMoves(const std::shared_ptr<Field> startField, int range);

#ifdef English_dox
		/**
		 *  Heuristic method calculating shortest path using manhattan metric
		 * 	TO DO: omijanie niedostepnych pol
		 *
		 * @param currentField field from which heuristic movement cost is calculated
		 * @param destinationField destination field of heuristic function
		 * @return int h value of heuristic function
		 */
#endif
		int calculateHeuristic(const std::shared_ptr<Field> currentField, const std::shared_ptr<Field> destinationField);

#ifdef English_dox
		/**
		 *  Implementation of A* algorithm searching for shortest path for army.
		 * 	Main method of PathFinding class responsible for searching for shortest path.
		 *
		 * @param startField field from which shortest path is calculated
		 * @param destinationField destination field to which shortest path is calculated
		 * @param range range of army for which path is calculated
		 * @return std::map consisting of pointers to fields denoting shortest path (key), and
		 *  		cost of moving there (mapped value)
		 */
#endif
		std::map<int, std::shared_ptr<Field>> findPathAStar(const std::shared_ptr<Field> startField, const std::shared_ptr<Field> destinationField, const int range);

#ifdef English_dox
		/**
		 * @brief Method responsible for reconstructing route from destinationField to startField using pointers to parent node
		 *
		 * @param closedSet std::multiset consisting of all seen Fields
		 * @param destinationField pointer to destination field
		 * @return std::map<int, std::shared_ptr<Field>> mapped value is pointer to field, and key is cost of moving to this field
		 */
#endif
		std::map<int, std::shared_ptr<Field>> reconstructPath(const std::multiset<Node, NodeCompare> closedSet, const std::shared_ptr<Field> destinationField);

};

#endif /* PATHFINDING_HPP_ */
