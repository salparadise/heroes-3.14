#ifndef GOLD_MINE_HPP_
#define GOLD_MINE_HPP_

#include <SFML/Graphics.hpp>
#include "Building.hpp"

class GoldMine : public Building{
public:
	GoldMine();
	virtual ~GoldMine();
	virtual void updateResources();
		
}; 

#endif /* GOLD_MINE_HPP_ */

