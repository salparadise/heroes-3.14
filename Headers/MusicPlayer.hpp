#ifndef MUSIC_PLAYER_HPP_
#define MUSIC_PLAYER_HPP_

#include <SFML/Audio.hpp>
#include <map>
#include <string>
#include <list>
#include <memory>
#include <assert.h>
#include <map>

/**
* Dwie klasy do otwarzania muzyki, MusicPlayer do muzyki, SoundPlayer do dzwiekow w grze
*
*
*
*/


	class MusicPlayer
	{
		
		public:
			enum ID
			{
				Menu,
				Map,
			};
		
		
			MusicPlayer();
			void play(ID sound);
			void playSound(std::string);
			void stop();
			void setVolume(float volume){_volume=volume; _music.setVolume(_volume);}
			void setPaused(bool paused);
			 std::map<std::string,  std::shared_ptr<sf::Music> > _sounds;
		
		private:
			sf::Music _music;
			std::map<ID, std::string> _themes;
			float _volume;

			
			
	};

	
	
	
	
	
#endif
