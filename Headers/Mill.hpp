#ifndef MILL_HPP_
#define MILL_HPP_

#include <SFML/Graphics.hpp>
#include "Building.hpp"

class Mill : public Building{
public:
	Mill();
	virtual ~Mill();
	virtual void updateResources();
		
}; 

#endif /* MILL_HPP_ */

