#ifndef FARM_HPP_
#define FARM_HPP_

#include <SFML/Graphics.hpp>
#include "Building.hpp"

class Farm : public Building{
public:
	Farm();
	virtual ~Farm();
	virtual void updateResources();
}; 

#endif /* FARM_HPP_ */

