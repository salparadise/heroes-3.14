/*
 * Meta.hpp
 *
 *  Created on: 25 lis 2013
 *      Author: hawker
 */

#ifndef META_HPP_
#define META_HPP_
#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include <memory>
#include "MetaGame.hpp"
#include "Layer.hpp"
#ifdef English_dox
	/**
	 *Layer of in-game menu
	 */
#endif
class MetaLayer : public Layer {
public:
	sf::IntRect rect;
	MetaLayer(std::shared_ptr<Game>);
	virtual ~MetaLayer() {}
	bool processEvent(sf::Event& e);
	virtual void draw(sf::RenderTarget& window, sf::RenderStates states) const;
	 virtual void update(sf::Time dt); //jak na razie tego nie potrzeba
protected:
	mutable int _wHeight;
	mutable int _wWidth;
	void createMessage();
	void clickedMenu();
	void clickedOk();
	void clickedCancel();
	std::shared_ptr<sfg::Window> _gui;
	sfg::Desktop _desktop;
};



#endif /* META_HPP_ */
