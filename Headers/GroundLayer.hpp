#ifndef GROUNDLAYER_HPP_
#define GROUNDLAYER_HPP_

#include <SFML/Graphics.hpp>
#include <memory>
#include "Layer.hpp"
#include "DialogLayer.hpp"
#include "Player.hpp"
#include "StringConverter.hpp"
#include "PathFinding.hpp"
#include "Town.hpp"
#include "MusicPlayer.hpp"

#ifdef Polish_dox /**
 * Klasa GroundLayer, ktorej zadaniem jest narysowanie w oknie gry aktualnej planszy.
 */
#endif
#ifdef English_dox /**
 * Prints current board
 */
#endif
class GroundLayer : public Layer {
public:
	GroundLayer(std::shared_ptr<Game>);
	virtual ~GroundLayer() {}

	#ifdef Polish_dox /**
	 * Metoda processEvent przechwytuje odpowiednie zdarzenia.
	 * W przypadku warstwy GroundLayer oznacza to przesuwanie jednostek po planszy,
	 * przewijanie planszy i jej przyblizanie.
	 *
	 * @param sf::Event& Aktualnie przetwarzane zdarzenie.
	 */

	#endif
	virtual bool processEvent(sf::Event&);

	#ifdef Polish_dox /**
	 * Metoda draw wyrysowuje rzezbe terenu, budynki i jednostki na planszy.
	 * W przypadku zaznaczenia jednostki, dorysowuje jej zasieg.
	 *
	 * @param sf::RenderTarget& Okno docelowe po ktorym warstwa rysuje.
	 * @param sf::RenderStates Dodatkowe informacje.
	 */
	#endif
	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

	#ifdef Polish_dox /**
	 * Metoda sprawdza, czy potrzebne jest wyszukanie mozliwych ruchow dla ktorejs z jednostek.
	 * Ponadto przewija plansze.
	 *
	 * @param sf::Time Czas od ostatniej aktualizacji stanu gry.
	 */
	#endif
	virtual void update(sf::Time dt);

	#ifdef Polish_dox /**
	 * Zatwierdzenie ruchu.
	 */
	#endif
	#ifdef English_dox /**
	 * Accepts move
	 */
	#endif
	void acceptMove();

	#ifdef Polish_dox /**
	 * Odrzucanie mozliwosci ruchu.
	 */
	#endif
	#ifdef English_dox /**
	 * cancels move
	 */
	#endif
	void cancelMove();
	
	

protected:
	#ifdef Polish_dox /**
	 * Struktura przechowujaca informacje o poruszajacej sie armii.
	 */
	#endif
	#ifdef English_dox /**
	 * Contains information about moving army
	 */
	#endif
	typedef struct MovingArmy {
		#ifdef Polish_dox /**
		 * Pole, z ktorego armia zaczynala marsz.
		 */
		#endif
		#ifdef English_dox /**
		 * Starting field of army
		 */
		#endif
		std::shared_ptr<Field> startField;

		#ifdef Polish_dox /**
		 * Pole, na ktore skierowano armie.
		 */
		#endif
		#ifdef English_dox /**
		 * Starting field of army
		 */
		#endif
		std::shared_ptr<Field> endField;

		#ifdef Polish_dox /**
		 * Sciezka, po ktorej odbywa sie ruch.
		 */
		#endif
		#ifdef English_dox /**
		 * Path of the movement
		 */
		#endif
		std::map<int, std::shared_ptr<Field>> path;

		#ifdef Polish_dox /**
		 * Aktualna pozycja armii.
		 */
		#endif
		#ifdef English_dox /**
		 * Actual position of army
		 */
		#endif
		sf::Vector2i current;

		#ifdef Polish_dox /**
		 * Przesuwana armia.
		 */
		#endif
		#ifdef English_dox /**
		 * Moved army
		 */
		#endif
		std::shared_ptr<Army> army;

		#ifdef Polish_dox /**
		 * Czy ruch zostal juz zatwierdzony?
		 */
		#endif
		#ifdef English_dox /**
		 * If move is accepted
		 */
		#endif
		bool makeMove;
	} MovingArmy;

	#ifdef Polish_dox /**
	 * Informacja o aktualnie poruszajacych sie po planszy jednostkach.
	 */
	#endif
	#ifdef English_dox /**
	 * Information about currently mooving units
	 */
	#endif
	bool isMoving = false;

	#ifdef Polish_dox /**
	 * RenderTextura na ktorej rysowane sa wszystkie elementy.
	 * Dzieki rysowaniu w pamieci, zamiast bezposrednio na ekranie, operacje te sa szybsze.
	 */
	#endif
	#ifdef English_dox /**
	 * RenderTexture on which elements ar printed.
	 */
	#endif
	std::shared_ptr<sf::RenderTexture> _drawBuffer;

	#ifdef Polish_dox /**
	 * Aktualnie podswietlana sciezka pomiedzy polami.
	 */
	#endif
	#ifdef English_dox /**
	 * Actually lightened path
	 */
	#endif
	std::map<int, std::shared_ptr<Field>> _path;

	#ifdef Polish_dox /**
	 * Aktualnie pokazywany zasieg jednostki.
	 */
	#endif
	#ifdef English_dox /**
	 * Actually showed range of unit
	 */
	#endif
	std::vector<std::shared_ptr<Field>> _possibleMoves;

	#ifdef Polish_dox /**
	 * Aktualnie przesuwana armia.
	 */
	#endif
	#ifdef English_dox /**
	 * Actually moved army
	 */
	#endif
	MovingArmy _movingArmy;

	#ifdef Polish_dox /**
	 * Nast�pne pole, na ktore przejdzie armia.
	 */
	#endif
	#ifdef Polish_dox /**
	 * field to be taken by army
	 */
	#endif
	std::shared_ptr<Field> _endField = nullptr;

	#ifdef Polish_dox /**
	 * Okienko akceptowania ruchu.
	 */
	#endif
	#ifdef Polish_dox /**
	 * Accept movement window
	 */
	#endif
	sfg::Window::Ptr _gui = nullptr;
	

	std::shared_ptr<MusicPlayer> _groundMusicPlayer;

};


#endif /* GROUNDLAYER_HPP_ */
