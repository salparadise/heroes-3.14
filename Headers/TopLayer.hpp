#ifndef TOPLAYER_HPP_
#define TOPLAYER_HPP_

#include "Layer.hpp"
#include "SidebarLayer.hpp"
#ifdef English_dox
	/**
	 *the highest layer
	 */
#endif
class TopLayer : public Layer {
public:
	TopLayer(std::shared_ptr<Game>);
	virtual ~TopLayer() {}
	virtual bool processEvent(sf::Event&);
	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
	virtual void update(sf::Time dt);
};



#endif /* TOPLAYER_HPP_ */
