/**
 * @file Player.hpp
 *
 * @date 10 lis 2013
 * @version 1.0
 *
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include <cmath>
#include <algorithm>
#include <SFML/Graphics.hpp>
#include "Race.hpp"
#include "ScriptedStateMachine.hpp"


//F: Forward declaration
//http://stackoverflow.com/questions/553682/when-to-use-forward-declaration
class Town;
class Building;
class Army;
class Field;

#ifdef Polish_dox
/*
*@class	Player
*Klasa gracza. Przechowuje zasoby, jego nazwe oraz rase. Posiada rowniez wartosc "isAlive" ktora mowi czy gracz jest jeszce w grze
*
*@fn funkcje update, zmieniaja ilosc danego surowca o podana wartosc
*/
#endif

class Player{
private:
	unsigned int _boardSizeX;
	unsigned int _boardSizeY;
	std::vector<std::vector<bool> > _fog;

	std::shared_ptr<Race> _race;
	std::string _name;
	int _sight;
	int _gold = 1000;
	int _wood = 1000;
	int _stone = 1000;
	int _food = 1000;
	bool _isAlive = true;
	const bool _humanPlayer;
	std::map<sf::Keyboard::Key, std::string> mKeyBinding;

	std::vector<std::shared_ptr<Army> > _playersArmiesVector;
	std::vector<std::shared_ptr<Building> > _playersBuildingsVector;
	std::vector<std::shared_ptr<Town> > _playersTownsVector;
	const std::vector<std::shared_ptr<Player> >& _players;
	std::shared_ptr<Army> _armyOnItsWayToTheTown;
	std::shared_ptr<Army> _armyOnItsWayToTheSpottedBuilding;

	const std::shared_ptr<Player>& _neutralPlayer;

	const luabind::object& _states;

public:
	Player(std::string, std::shared_ptr<Race>, const std::vector<std::shared_ptr<Player> >& players, bool humanPlayer, const std::shared_ptr<Player>& neutralPlayer, luabind::object& states, int gold = 500, int wood = 20, int stone = 20, int food = 100);

	~Player();

	bool alive() {return _isAlive;}
	void dead() {_isAlive = false;}

	bool isHumanPlayer() const {return _humanPlayer;}

	int getGold() {return _gold;}
	int getWood() {return _wood;}
	int getStone() {return _stone;}
	int getFood() {return _food;}
	int getSight() {return _sight;};

	void setGold(int gold) {_gold = gold;}
	void setWood(int wood) {_wood = wood;}
	void setStone(int stone) {_stone = stone;}
	void setFood(int food) {_food = food;}
	void setSight(int sight) {_sight=sight;};

	void updateGold(int dGold) {_gold += dGold;}
	void updateWood(int dWood) {_wood += dWood;}
	void updateStone(int dStone) {_stone += dStone;}
	void updateFood(int dFood) {_food += dFood;}

	void update();
	void updateFogOfWar(unsigned int,unsigned int,bool c);
	void setFog(int a, int b, bool c);

	std::string getName(){return _name;}
	void print();

	std::string handleInput(sf::Event &);
	void setDefaultKeys();
	void assignKey(std::string action,sf::Keyboard key);
	std::shared_ptr<Race> getRace(){return _race;}
	#ifdef Polish_dox
	/**
	 * Inicjacja mgly wojny.
	 * @param wspolrzedna x
	 * @param wspolrzedna y
	 */
	#endif
	void initiateFog(int,int);
	#ifdef Polish_dox
	/**
	 * Funckcja zwracajaca informacje czy na podanej pozycji jest obecna mgla wojny.
	 * @param wspolrzedna x
	 * @param wspolrzedna y
	 * @return Jesli teren jest zakryty mgla wojny, zwraca true.
	 * W przeciwnym wypadku, zwraca false.
	 */
	#endif
	bool getFog(int , int );
	#ifdef Polish_dox
	/**
	 * Funkcja zwraca wektor przechowujacy wszystkich graczy.
	 */
	#endif
	const std::vector<std::shared_ptr<Player> >& getPlayers() const;

	#ifdef Polish_dox
	/**
	 * Funkcja pozwalajaca na przypisanie armii graczowi (Player)
	 * Umieszcza podana w pierwszym argumencie armie w wektorze armii _playersArmiesVector
	 */
	#endif
	void addArmy(std::shared_ptr<Army> army);
	#ifdef Polish_dox
	/**
	 * Funkcja usuwajaca armie z wektora armii _playersArmiesVector gracza (Player)
	 */
	#endif
	void removeArmy(std::shared_ptr<Army> army);
	#ifdef Polish_dox
	/**
	 * Funkcja uaktualnia pozycje Armii army w wektorze Armii _playersArmiesVector
	 * @param army Armia, kotej pozycje zmieniamy
	 * @param newPosition Nowa pozycja
	 */
	#endif
	void updateArmyPosition(const std::shared_ptr<Army>& army, const std::shared_ptr<Field>& newPosition);
	const std::vector<std::shared_ptr<Army> >& getArmiesVector() const
	{
		return (_playersArmiesVector);
	}

	#ifdef Polish_dox
	/**
	 * Funkcja pozwalajaca na przypisanie budynku graczowi (Player)
	 * Umieszcza podany w pierwszym argumencie budynek w wektorze budynkow _playersBuildingsVector
	 */
	#endif
	void addBuilding(std::shared_ptr<Building> building);
	#ifdef Polish_dox
	/**
	 * Funkcja usuwajaca budynek z wektora budynkow _playersBuildingsVector gracza (Player)
	 */
	#endif
	void removeBuilding(std::shared_ptr<Building> building);
	const std::vector<std::shared_ptr<Building> >& getBuildingsVector() const
	{
		return (_playersBuildingsVector);
	}
	std::vector<std::shared_ptr<Building> >& getBuildingsVector()
	{
		return (_playersBuildingsVector);
	}

	#ifdef Polish_dox
	/**
	 * Funkcja pozwalajaca na przypisanie miasta graczowi (Player)
	 * Umieszcza podane w pierwszym argumencie miasto w wektorze budynkow _playersTownsVector
	 */
	#endif
	void addTown(std::shared_ptr<Town> town);
	#ifdef Polish_dox
	/**
	 * Funkcja usuwajaca miasto z wektora miast _playersTownsVector gracza (Player)
	 */
	#endif
	void removeTown(std::shared_ptr<Town> town);
	const std::vector<std::shared_ptr<Town> >& getTownsVector() const
	{
		return (_playersTownsVector);
	}
	std::vector<std::shared_ptr<Town> >& getTownsVector()
	{
		return (_playersTownsVector);
	}

	sf::Vector2i getBoardSize() const
	{
		return (sf::Vector2i(_boardSizeX, _boardSizeY) );
	}

	void setArmyToTheTown(const std::shared_ptr<Army>& army)
	{
		_armyOnItsWayToTheTown = army;
	}

	void resetArmyToTheTown()
	{
		_armyOnItsWayToTheTown = nullptr;
	}

	void setArmyToTheSpottedBuilding(const std::shared_ptr<Army>& army)
	{
		_armyOnItsWayToTheSpottedBuilding = army;
	}

	void resetArmyToTheSpottedBuilding()
	{
		_armyOnItsWayToTheSpottedBuilding = nullptr;
	}

	const std::shared_ptr<Army>& getArmyToTheTown() const
	{
		return (_armyOnItsWayToTheTown);
	}

	const std::shared_ptr<Army>& getArmyToTheSpottedBuilding() const
	{
		return (_armyOnItsWayToTheSpottedBuilding);
	}

	const luabind::object& getLuabindStates() const
	{
		return (_states);
	}

	#ifdef Polish_dox
	/**
	 * Funkcja zwraca wskaznik do gracza neutralnego.
	 * Nie figuruje on w wektorze wszystkich graczy, ktory mozna pobrac metoda getPlayers().
	 */
	#endif
	const std::shared_ptr<Player>& getNeutralPlayer() const
	{
		return (_neutralPlayer);
	}

};

#endif /* PLAYER_HPP_ */
