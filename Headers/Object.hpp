#ifndef OBJECT_HPP_
#define OBJECT_HPP_

#include <SFML/Graphics.hpp>
#include "ResourceManager.hpp"

class Field;
#ifdef English_dox
	/**
	 * Encapsulates objects on fields
	 */
#endif
class Object{
	protected:
#ifdef English_dox
	/**
	 *
	 */
#endif
		int _textureId;
#ifdef English_dox
	/**
	 *
	 */
#endif
		static int _numberOfObjects;
#ifdef English_dox
	/**
	 *
	 */
#endif
		std::shared_ptr<Field> _currentField;
#ifdef English_dox
	/**
	 *
	 */
#endif

		static const int VISIBILITY_RADIUS = 4;
	public:
		Object();

		virtual ~Object();
#ifdef English_dox
	/**
	 *
	 */
#endif
		virtual bool isActiveObject() const = 0;
#ifdef English_dox
	/**
	 *
	 */
#endif
		int getTextureId() const
		{
			return _textureId;
		}
#ifdef English_dox
	/**
	 *
	 */
#endif
		void setTextureId(int textureId)
		{
			_textureId = textureId;
		}
#ifdef English_dox
	/**
	 *
	 */
#endif
		int getId() const
		{
			return _id;
		}
#ifdef English_dox
	/**
	 *
	 */
#endif
		void setCurrentField(const std::shared_ptr<Field>& field)
		{
			_currentField = field;
		}
#ifdef English_dox
	/**
	 *
	 */
#endif
		std::shared_ptr<Field>& getCurrentField()
		{
			return _currentField;
		}
#ifdef English_dox
	/**
	 *
	 */
#endif
		const std::shared_ptr<Field>& getCurrentField() const
		{
			return _currentField;
		}
#ifdef English_dox
	/**
	 *
	 */
#endif
		virtual std::string getType() const {
			return "Object";
		}
#ifdef English_dox
	/**
	 *
	 */
#endif
		int _id;
};

#endif /* OBJECT_HPP_ */
