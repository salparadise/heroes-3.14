#ifndef SINGLE_PLAYER_GAME_HPP_
#define SINGLE_PLAYER_GAME_HPP_

#include <iostream>
#include <SFML/Graphics.hpp>
#include <memory>
#include <algorithm>
#include <string>
#include <tuple>
#include "Game.hpp"
#include "Player.hpp"
#include "Town.hpp"
#include "StringConverter.hpp"

#ifdef English_dox
/**
 * Class responsible for handling Single Player Game.
 */
#endif
class SinglePlayerGame : public Game {
	public:
#ifdef English_dox
		/**
		 * Primary constructor of SinglePlayerGame.
		 * @param std::tuple<> containing:\n
		 * 			std::string player name,\n
		 * 			int race id,\n
		 * 			bool information if player is AI\n
		 * 			sf::Vector2i coordinates of castle, which has been chosen for player\n
		 * @param w
		 * @param textures
		 * @param path name of file with map
		 */
#endif
#ifdef Polish_dox
		/**
		 * Podstawowy konstruktor SinglePlayerGame.
		 * @param gracze krotka (std::tuple<>) zawierajaca:\n
		 * 			std::string imie gracza,\n
		 * 			int nr id rasy,\n
		 * 			bool informacje o tym czy gracz jest kontrolowany przez cz�owieka czy komputer\n
		 * 			sf::Vector2i polozenie zamku, ktory zostal wybrany dla gracza
		 * @param w
		 * @param textures
		 * @param path Nazwa pliku z mapa
		 */
#endif
		SinglePlayerGame( std::vector<std::tuple<std::string, int, bool, sf::Vector2i>> players,
						  std::shared_ptr<sf::RenderWindow> w,
						  TextureHolder& textures,
						  std::string path);
		
}; 

#endif /* SINGLE_PLAYER_GAME_HPP_ */
