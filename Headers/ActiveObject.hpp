#ifndef ACTIVE_OBJECT_HPP_
#define ACTIVE_OBJECT_HPP_

#include <iostream>
#include <SFML/Graphics.hpp>
#include <memory>
#include "Object.hpp"
#include "Player.hpp"
#ifdef English_dox
	/**
	 *Encapsulates objects which are acitve (can make actions, has owner)
	 */
#endif
class ActiveObject : public Object{
protected:
#ifdef English_dox
	/**
	 *
	 */
#endif
	std::shared_ptr<Player> _owner;
	int _income = 0;

public:
#ifdef English_dox
	/**
	 *
	 */
#endif
	ActiveObject() { }
	virtual ~ActiveObject() {}
#ifdef English_dox
	/**
	 *
	 */
#endif
	std::shared_ptr<Player> getOwner(){ return _owner; };
#ifdef English_dox
	/**
	 *
	 */
#endif
	void changeOwner(std::shared_ptr<Player> newOwner) { _owner = newOwner; }
#ifdef English_dox
	/**
	 *
	 */
#endif
	virtual bool isActiveObject() const { return true; }

};

#endif /* ACTIVE_OBJECT_HPP_ */
