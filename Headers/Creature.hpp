
#ifndef CREATURE_HPP_
#define CREATURE_HPP_

#include <SFML/Graphics.hpp>
#include <string>
#include <memory>
#include <cstdlib>
#include <ctime>
#ifdef Polish_dox /**
*@class	Creature
* Klasa przechowujaca statystyki jednej jednostki.
* _maxHealth- wytrzymalosc jednostki
* _maxDamage- maksymalne obrazenia zadawane przez jednostke
* _minDamage- minimalne obrazenia zadawane przez jednostke
* _upgrade- czy dana jednostka jest ulepszona
* _range- zasieg ruchu jednostki
* _level- poziom jednostki (nie uwzgledniamy +5
* _cost - koszt zakupu jednostki
*

*/
#endif
#ifdef English_dox /**
*@class	Creature
* Contains of statistics of single unit
* _maxHealth
* _maxDamage
* _minDamage
* _upgrade
* _range
* _level
* _cost
*

*/
#endif



class Creature {
	private:
		std::string _name;
		int _abilities;
		int _maxHealth;
		int _maxDamage;
		int _minDamage;
		bool _upgrade;
		int _range;
		int _level;
		int _cost = 10;

	public:
		//Creature( std::string race, int range) : _race(race), _range(range) {}
#ifdef Polish_dox /**
*@fn Creature(std::string name, int maxHealth, int maxDamage, int minDamage, int range, int lv)
* Konstruktor przyjmujacy nazwe, zycie, maksymalne obrazenia, minimalne obrazenia, zasieg ruchu, poziom.
*/
#endif
#ifdef English_dox /**
*@fn Creature(std::string name, int maxHealth, int maxDamage, int minDamage, int range, int lv)
* Constructs this class by name, health, maximum damage, minimal damage, rang of movement, and level.
*/
#endif
		Creature(std::string name, int maxHealth, int maxDamage, int minDamage, int range, int lv){ _name= name; _maxHealth=maxHealth; _maxDamage=maxDamage; _minDamage=minDamage; _range=range; _level=lv;}
		Creature(int maxHealth, int maxDamage, int minDamage){_maxHealth=maxHealth; _maxDamage=maxDamage; _minDamage=minDamage;}

		//std::shared_ptr<Creature> getCreatures(int n);

		//int* getType() {return _type; } //nie bedzie potrzebne

		std::string getName() {return _name;}
		void setName(std::string name) {_name=name;}

		//int getAbilities(){return _abilities;}
		//void setAbilities(int abilities) {_abilities=abilities;}

		int getMaxHealth(){return _maxHealth;}
		void setMaxHealth(int maxHealth) {_maxHealth=maxHealth;}

		int getMaxDamage(){return _maxDamage;}
		void setMaxDamage(int maxDamage) {_maxDamage=maxDamage;}

		int getMinDamage(){return _minDamage;}
		void setMinDamage(int minDamage) {_minDamage=minDamage;}

		int getRange(){return _range;}
		void setRange(int range) {_range=range;}


		int getCost() const { return _cost; }
		void setCost(int c) { _cost = c; }

		int getLevel(){return _level;}

		bool getUpgrade() {return _upgrade; }
		void setUpgrade(bool upgrade) {_upgrade=upgrade; }

		//sf::IntRect getGraphics(){return _graphics;}
		//void setGraphics(sf::IntRect graphics) {_graphics=graphics;}
#ifdef Polish_dox /**
*@fn getDamage()
*Zwraca wylosowane obrazenia od danego potwora.
*/
#endif
		int getDamage();

		void printCreature(){}
};

#endif

