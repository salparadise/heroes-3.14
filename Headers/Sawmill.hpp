#ifndef SAWMILL_HPP_
#define SAWMILL_HPP_

#include <SFML/Graphics.hpp>
#include "Building.hpp"

class Sawmill : public Building{
public:
	Sawmill();
	virtual ~Sawmill();
	virtual void updateResources();

}; 

#endif /* SAWMILL_HPP_ */

