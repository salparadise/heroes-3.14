#ifndef TOWN_HPP_
#define TOWN_HPP_

#include <SFML/Graphics.hpp>
#include "Building.hpp"
#include "Creature.hpp"
#include "Hero.hpp"
#include "Army.hpp"
#include <memory>
#include <vector>
#include <string>

class Town : public Building{
	public:
		/**
		 * Typ budynku na ekranie miasta.
		 */
		enum BuildingType {
			BARRACKS,
			STONEMASON,
			TOWNHALL,
			BANK,
			MILL,
			CLOSE
		};

		/**
		 * Struktura reprezentujaca budowle w ekranie miasta.
		 */
		typedef struct Part {
			/**
			 * Obszar zajmowany przez budynek.
			 * Dwie pierwsze wspolrzedne (left, top) to polozenie budynku na ekranie.
			 * Dwie kolejne (width, height) to rozmiary.
			 */
			sf::IntRect area;

			/**
			 * Typ budynku.
			 */
			BuildingType type;

			/**
			 * Id budynku.
			 * Jest o 1 wieksze od indeksu w wektorze (czyli przymuje wartosci [1; 10]).
			 */
			int id;

			/**
			 * Informacja o tym, czy budynek zostal juz odblokowany.
			 */
			bool active;

			/**
			 * Koszt budowy - zloto.
			 */
			int costGold;

			/**
			 * Koszt budowy - drewno.
			 */
			int costWood;

			/**
			 * Koszt budowy - kamie�.
			 */
			int costStone;
		} Part;


	private:
		std::map<std::string, std::shared_ptr<Creature>> _creature;

		/**
		 * Zmienna oznaczajaca, czy wyswietlony jest aktualnie ekran tego miasta.
		 */
		bool _selected;

		/**
		 * Wektor budynkow na ekranie miasta.
		 */
		std::vector<std::shared_ptr<Part>> _buildings;

		/**
		 * Aktualnie zaznaczony budynek.
		 */
		std::shared_ptr<Part> _building;

		/**
		 * Armia broniaca miasta.
		 * Mozna ja rozbudowywac z poziomu ekranu miasta. Nie mozna nia sterowac.
		 */
		std::shared_ptr<Army> _army;

		/**
		 * Przesuniecie obszaru klikniec wzgledem (0, 0).
		 * Wymagane do obslugi pelnej rozdzielczosci.
		 */
		sf::Vector2i _offset;

		int _foodIncome;
	public:
		Town();
		~Town();
		std::shared_ptr<Hero> newHero(int type);
		std::shared_ptr<Creature> newCreature(int type);
		virtual void updateResources();
		virtual std::string getType() const {
			return "Town";
		}

		/**
		 * Wybierz budynek i wyswietl dla niego ekran miasta.
		 */
		void select() {
			_selected = true;
		}

		/**
		 * Odznacz budynek.
		 */
		void deselect() {
			_selected = false;
		}

		/**
		 * Sprawdz, czy dla tego budynku poproszono o wyswietlenie ekranu miasta.
		 */
		bool isSelected() const {
			return _selected;
		}

		/**
		 * Pobierz liste budynkow w miescie.
		 */
		std::vector<std::shared_ptr<Part>> getBuildings() {
			return _buildings;
		}

		/**
		 * Pobierz aktualnie zaznaczony budynek.
		 */
		std::shared_ptr<Part> getActiveBuilding() {
			return _building;
		}

		/**
		 * Zaznacza budynek.
		 */
		void setActiveBuilding(std::shared_ptr<Part> b) {
			_building = b;
		}

		/**
		 * Zwraca armie obroncow miasta.
		 */
		std::shared_ptr<Army> getArmy() {
			return _army;
		}

		/**
		 * Metoda zmienajaca wlasciciela miasta.
		 * @param newOwner Wskaznik do nowego wlasciciela.
		 */
		void changeOwner(std::shared_ptr<Player> newOwner);

		/**
		 * Metoda inicjalizuje obrone miasta.
		 * Nie moze to zostac wykonane w kostruktorze, poniewaz nie sa wtedy dostepne informacje o wlascicielu.
		 */
		void initDefense();

		/**
		 * Zmienia _offset.
		 */
		void setOffset(sf::Vector2i offset);

		/**
		 * Zwraca informacje o istnieniu w miescie magazynu.
		 */
		bool hasWarehouse() const {
			return _buildings[8]->active;
		}

		/**
		 * Zwraca informacje o istnieniu w miescie ratusza.
		 */
		bool hasTownHall() const {
			return _buildings[5]->active;
		}

		/**
		 * Zwraca informacje o istnieniu w miescie banku.
		 */
		bool hasBank() const {
			return _buildings[7]->active;
		}
};

#endif  /* TOWN_HPP_ */

