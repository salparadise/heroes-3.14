#ifndef SCRIPTEDSTATEMACHINE_HPP_
#define SCRIPTEDSTATEMACHINE_HPP_

extern "C"
{
	#include <lua.h>
}
#include <stack>
#include <luabind/luabind.hpp>
#include <cassert>

template <typename entity_type>
class ScriptedStateMachine
{
private:
	//pointer to the agent that owns this instance
	entity_type* _owner;

	//the current state is a lua table of lua functions. A table may be
	//represented in C++ using a luabind::object
	luabind::object _currentState;

	//stos stanow. na jego szczycie znajduje sie aktywny stan
	std::stack<luabind::object> _stateStack;

public:
	ScriptedStateMachine(entity_type* owner): _owner(owner)
	{}

	#ifdef Polish_dox
	/**
	 * Funkcja uaktualnia FSM, tj. dzialania Army.
	 * Robi to poprzez pobranie elementu ze szczytu stosu stanow _stateStack - stan na szczycie stosu to stan aktywny.
	 * Nastepnie wykonuje dla niego (tego stanu) akcje execute.
	 */
	#endif
	void update()
	{
		_currentState = getCurrentState();
		//trzeba sie upewnic czy aktywny stan 'is valid' przed uruchomieniem akcji execute
		if (_currentState.is_valid()) //this could also be written as 'if(_currentState)'
			_currentState["execute"](_owner);
		else
			throw std::runtime_error("ScriptedStateMachine::update(): !_currentState.is_valid()\n");
	}

	#ifdef Polish_dox
	/**
	 * Funkcja wyrzuca z gory stosu (aktywny) stan.
	 * Zamin zrzuci stary stan wykonuje jego akcje exit (logiczny 'destruktor' akcji).
	 * Po zrzuceniu uruchamia jego akcje enter ('konstruktor').
	 */
	#endif
	void popState()
	{
		_currentState = getCurrentState();

		_currentState["exit"](_owner);

		_stateStack.pop();

		_currentState = getCurrentState();

		_currentState["enter"](_owner);
	}

	#ifdef Polish_dox
	/**
	 * Funkcja wrzuca na gore stosu nowy stan (co rowna sie zatrzymaniem aktywnego stanu).
	 * Zanim wrzuci nowy stan (ktory stanie sie aktywnym), wykonuje akcje exit dla -jeszcze- obecnego aktywnego stanu.
	 * Po wrzuceniu nowego stanu uruchamia jego akcje enter.
	 * @param new_state Nowy stan, ktory ma zostac wrzucony na szczyt stosu.
	 */
	#endif
	void pushState(const luabind::object& new_state)
	{
		if(new_state != getCurrentState())
		{
			_currentState = getCurrentState();

			_currentState["exit"](_owner);

			_stateStack.push(new_state);

			_currentState = getCurrentState();

			_currentState["enter"](_owner);
		}
	}

	#ifdef Polish_dox
	/**
	 * Pobiera (nie usuwa!) aktywny stan, tj. stan ze szczytu stosu.
	 */
	#endif
	const luabind::object& getCurrentState() const
	{
		if(!_stateStack.empty())
			return _stateStack.top();
		else
			throw std::runtime_error("Probowano pobrac element ze stosu stanow _stateStack podczas gdy byl pusty.\n");
	}

	#ifdef Polish_dox
	/**
	 * Wrzuca element na PUSTY stos. Sluzy jedynie ustawienia poczatkowego (pierwszego i nieusuwalnego) stanu.
	 * Do wrzucania kolejnych stanow sluzy ScriptedStateMachine::pushState()
	 * @param new_state Stan poczatkowy, ktory pozostaje na dnie stosu przez cala gre.
	 */
	#endif
	void setStartState(const luabind::object& new_state)
	{
		if(_stateStack.empty())
			_stateStack.push(new_state);
		else
			throw std::runtime_error("Probowano niepoprawnie wepchnac stan na stos. Uzyj do tego funkcji ScriptedStateMachine::pushState()\n");
	}
};

#endif
