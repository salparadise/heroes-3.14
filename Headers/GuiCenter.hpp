#ifndef GUICENTER_HPP_
#define GUICENTER_HPP_

#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include <memory>

class GuiCenter {
public:
	virtual ~GuiCenter() {}

	virtual void centerWindow(sfg::Window::Ptr gui, std::shared_ptr<sf::RenderWindow> window) {
		gui->SetPosition(
			sf::Vector2f(
				window->getSize().x / 2.f - gui->GetAllocation().width / 2.f - 100,
				window->getSize().y / 2.f - gui->GetAllocation().height / 2.f
			)
		);
	}
};

#endif
