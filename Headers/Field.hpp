#ifndef FIELD_HPP_
#define FIELD_HPP_

#include <SFML/Graphics.hpp>
#include <memory>
#include <vector>
#include <algorithm>
#include <map>
#include <set>
#include <utility>
#include <cmath>
#include "Object.hpp"
#include "StaticObject.hpp"

class Building;
class Army;
class Field;


//////////////////////////////

class Field : public sf::Drawable, public sf::Transformable {
	private:
		sf::Vector2i _position;
		int _cost;

		sf::Color _groundColor;
		std::shared_ptr<StaticObject> _ground;
		std::shared_ptr<Building> _building;
		std::shared_ptr<Army> _army;

		#ifdef English_dox /**
		 * @var _neighbourFields
		 * @brief Convention of neighbours: v[0] = NW, v[1] = N, v[2] = NE, v[3] = W, v[4] = center,
		 * 		v[5] = E, v[6] = SW,  v[7] = S, v[8] = SE.
		 * 		http://cdn.tutsplus.com/gamedev/uploads/legacy/032_tileMask/local_range_in_1d.png
		 */
		#endif
		std::vector<std::shared_ptr<Field> > _neighbourFields;

		#ifdef Polish_dox
		/**
		 * @var _textureId zmienna okresla identyfikator tekstury.
		 * 		Dzieki niemu mozna podbrac odpowiednia teksture z obiektu 'ResourceManager<sf::Texture> textures' (przyklad: textures[_textureId])
		 */
		#endif
		#ifdef English_dox
		/**
		 * @var _textureId id of texture;
		 */
		#endif
		int _textureId;
		TextureHolder& textures;

	public:
		Field(sf::Vector2i position, TextureHolder& textures, int cost = 0, std::shared_ptr<StaticObject> ground = nullptr, std::shared_ptr<Building> building = nullptr, std::shared_ptr<Army> army = nullptr);

		void init();

		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

		void setGround(std::shared_ptr<StaticObject>);
		#ifdef English_dox /**
		*
		*/
		#endif
		void setTextureId(int);
		#ifdef English_dox /**
		 *
		*/
		#endif
		void setBuilding(std::shared_ptr<Building>);
		#ifdef English_dox /**
		*
		*/
		#endif
		void setPosition(sf::Vector2i);
		#ifdef English_dox /**
		*
		*/
		#endif
		void setObject(const std::shared_ptr<Object>);
		#ifdef English_dox /**
		*
		*/
		#endif
		void addArmy(const std::shared_ptr<Army>& army);
		#ifdef English_dox /**
		*
		*/
		#endif
		std::shared_ptr<Army> getArmy();
		#ifdef English_dox /**
		*
		*/
		#endif
		void removeArmy();

		#ifdef English_dox /**
		*
		*/
		#endif
		void moveArmy(std::shared_ptr<Field> startField, std::shared_ptr<Field> destinationField);
		#ifdef English_dox /**
		*
		*/
		#endif

		void removeBuilding();
		#ifdef English_dox /**
		*
		*/
		#endif
		void setNeighbourFields(std::vector<std::shared_ptr<Field> > neighbourFields) {_neighbourFields = neighbourFields; }
		#ifdef English_dox /**
		*
		*/
		#endif
		const std::vector<std::shared_ptr<Field> >& getNeighbourFields() const {return _neighbourFields; }
		#ifdef English_dox /**
		*
		*/
		#endif
		void setCost(int cost) {_cost = cost; }
		#ifdef English_dox /**
		*
		*/
		#endif
		int getCost() const {return _cost; }
		#ifdef English_dox /**
		*
		*/
		#endif
		void setGroundColor(const std::string& hexColor);
		sf::Color getGroundColor() const;
		#ifdef English_dox /**
		*
		*/
		#endif

		sf::Vector2i getPosition() const;
		#ifdef English_dox /**
		*
		*/
		#endif
		bool isFree() const;
		#ifdef English_dox /**
		*
		*/
		#endif
		bool isFreeBuilding() const;
		#ifdef English_dox /**
		*
		*/
		#endif
		bool isFreeArmy() const;
		#ifdef English_dox /**
		*
		*/
		#endif

		//std::shared_ptr<Army> getArmy(){return _army;#ifdef English_dox /** * */ #endif}
		#ifdef English_dox /**
		*
		*/
		#endif
		std::shared_ptr<Building> getBuilding(){return _building;}

		#ifdef Polish_dox /**
		 * @brief Operator porownuje obiekty typu Field jedynie pod wzgledem polozenia na mapie
		 */
		#endif
		#ifdef English_dox /**
		 * @brief Compares coordinates of field
		 */
		#endif
		bool operator==(const Field& rhs) const
		{
			return (_position == rhs._position);
		}

};


#endif /* FIELD_HPP_ */
