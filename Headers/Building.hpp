/**
 * @file Building.hpp
 *
 * @date 11 lis 2013
 * @author Wojtek
 * @version 1.0
 *
 */

#ifndef BUILDING_HPP_
#define BUILDING_HPP_

#include <SFML/Graphics.hpp>
#include "ActiveObject.hpp"
#ifdef English_dox /**
	 * Class which represents building
	 */
	#endif
class Building : public ActiveObject{
	private:
		unsigned _buildingId;

	protected:
		int _income = 0;

	public:
		Building(){
			_id = _numberOfObjects-1;
		}

		virtual ~Building() {}
		virtual void updateResources() = 0;
		#ifdef Polish_dox
		/**
		 * Przeladowany operator porownuje obiekty Building na podstawie ich polozenia na mapie (Object::_place)
		 * oraz na podstawie identyfikatora nadawanego kazdemu obiektowi Object (Object::_id)
		 */
		#endif
		#ifdef English_dox /**
		 * Compares coordinates of building
		 */
		#endif
		bool operator==(const Building& rhsBuilding);
		#ifdef English_dox /**
		 * Set buildings id
		 * @param buildingId id of building
		 */
		#endif
		void setBuildingId(const unsigned buildingId)
		{
			_buildingId = buildingId;
		}
		#ifdef English_dox /**
		 * Get buildings id
		 * @return unsigned int buildingId
		 */
		#endif
		unsigned getBuildingId() const
		{
			return _buildingId;
		}
		#ifdef English_dox /**
		 * Get buildings type
		 * @return type of building
		 */
		#endif
		virtual std::string getType() const {
			return "Building";
		}
};

#endif /* BUILDING_HPP_ */
