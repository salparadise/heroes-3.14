/**
 * ResourceManager.hpp
 */

#ifndef RESOURCEMANAGER_HPP_
#define RESOURCEMANAGER_HPP_

#include <unordered_map>
#include <memory>
#include <fstream>
#include <stdexcept>
#include <cstdio>
#include <cassert>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Graphics/Font.hpp>

#include "json.h"

template <typename Resource>
class ResourceManager
{
public:
	#ifdef Polish_dox
	/**
	 * Funkcja laduje do kontenera _resourcesMap dane zadane w konkretnym pliku .json.
	 * Pojedynczy obiekt w pliku .json musi KONIECZNIE:
	 *	- znajdowal sie� w 'tablicy',
	 *	- skladal sie� z pola "id" (ktoremu przypisujemy wartosc int, kotra jest kluczem w kontenerze _resourcesMap)
	 *		oraz pola "path" (ktore okresla polozenie zasobu na dysku).
	 * Najprostrzy przyklad pliku .json (dla tekstur): [{"id":1,"path":"Resources/grass.png"}, {"id":2,"path":"Resources/water.png"}]
	 * @param filename Sciezka do pliku .json
	 */
	#endif
	void loadByJSON(const std::string& filename);
	// Resource& get(int id);
	#ifdef Polish_dox
	/**
	 * @brief Funkcja zwraca zasob, ktorego nazwa podawana jest jako pierwszy argument
	 */
	#endif
	const Resource& get(const int id) const;
	const Resource& operator[](const int id) const;
private:
	#ifdef Polish_dox
	/**
	 * @brief Funkcja tworzy miejsce w pamieci i laduje z pliku zasob
	 * @param id Okresla identyfikator pod jakim zostanie zapisany zasob w mapie _resourcesMap
	 * @param filename Sciezka do ladowanego zasobu
	 */
	#endif
	void load(const int id, const std::string& filename);
	#ifdef Polish_dox
	/**
	 * @brief Funkcja umieszcza zaladowany do pamieci zasob w kontenerze _resoucesMap
	 * @param id (oznacza to samo co w funkcja load) Okresla nazwe pod jaka zostanie zapisany zasob
	 * 	w mapie _resourcesMap
	 * @param resource Wskaznik do ladowanego zasobu
	 */
	#endif
	void insertResource(const int id, std::unique_ptr<Resource> resource);
	#ifdef Polish_dox
	/**
	 * Funkcja wczytuje plik .json do pojedynczego stringa, ktorego nastepne zwraca.
	 * Wczytywanie powinno byc tak szybkie jak obsluga pliku w 'klasycznym C':
	 * http://insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html
	 * @param filename Sciezka do pliku JSON
	 * @return Pojedynczy string, w ktorym siedzi caly plik JSON
	 */
	#endif
	std::string prepareJSONFile(const std::string& filename);
private:
	//http://stackoverflow.com/questions/8372579/c-1m-look-ups-in-unordered-map-with-string-key-works-much-slower-than-net-c
	//http://blog.dubbelboer.com/2012/12/04/lru-cache.html
	std::unordered_map<int, std::unique_ptr<Resource>> _resourcesMap;
};

//ResourceManager nie obsluguje sf::Music. patrz: podrecznik, s.45
typedef ResourceManager<sf::Texture> TextureHolder;
typedef ResourceManager<sf::Font> FontHolder;
typedef ResourceManager<sf::SoundBuffer> SoundHolder;
//Klasie Game czy kazdej fabryce obiektow (np. jednostek) bedzie przekazywany wskaznik/referencja do odpowiednich Holder'ow

#include "ResourceManager.inl"

#endif
