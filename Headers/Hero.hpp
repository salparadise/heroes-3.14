#ifndef HERO_HPP_
#define HERO_HPP_

#include <memory>
#include <SFML/Graphics.hpp>

class Hero{
private:
	std::string _name;
	int _abilities;
	int _range;

public:
	int getAbilities() {return _abilities; }
	void setAbilities(int abilities) {_abilities=abilities; }
	int getRange() {return _range; }
	void setRange(int range) {_range=range; }
	Hero(std::string name) : _name(name)
	{}
	/*
	 *@fn printHer()
	 *Wypisuje bohatera.
	 */
	void printHero(){}
};
#endif
