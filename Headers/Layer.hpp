#ifndef LAYER_HPP_
#define LAYER_HPP_

#include <SFML/Graphics.hpp>
#include <memory>
#include "Game.hpp"
#include "GuiCenter.hpp"

#ifdef Polish_dox /**
 * Klasa Layer - warstwa.
 * Poszczegolne warstwy maja za zadanie przejmowac odpowiednie zdazenia (lub przesylac je dalej),
 * rysowac odpowiednie elementy na okno i aktualizowac stan gry.
 */
#endif
#ifdef English_dox /**
 * Layer class - a layer
 * Each layers has a task to capture states or events (or send them further)
 * paint elements onto window and update state of the game.
 */
#endif
class Layer : public sf::Drawable, public sf::Transformable, public GuiCenter {
public:

	#ifdef Polish_dox /**
	 * Konstruktor klasy Layer. Przyjmuje jako parametr aktualna rozgrywke.
	 *
	 * @param std::shared_ptr<Game> Wskaznik do aktualnej rozgrywki.
	 */
	#endif
	#ifdef English_dox /**
	 * @param std::shared_ptr<Game> Pointer to current game.
	 */
	#endif
	Layer(std::shared_ptr<Game> g):_game(g) { }
	virtual ~Layer() {}

	#ifdef Polish_dox /**
	 * Funkcja processEvent przetwarzajaca zdarzenia (np. klikniecie mysza, nacisniecie klawisza).
	 *
	 * @param sf::Event& Aktualne zdarzenie.
	 * @return bool Zwraca true, jesli kolejne warstwy tez powinny obsluzyc zdarzenie.
	 */
	#endif
#ifdef English_dox /**
		* process event
	 * @param sf::Event& event to process
	 * @return bool true if other layers should handle event
	 */
	#endif
	virtual bool processEvent(sf::Event&) = 0;

	#ifdef Polish_dox /**
	 * Przeladowana metoda dziedziczona z sf::Drawable.
	 * Funkcja wyrysowuje w oknie przypisane jej elementy.
	 *
	 * @param sf::RenderTarget& Okno docelowe po ktorym warstwa rysuje.
	 * @param sf::RenderStates Dodatkowe informacje.
	 */
	#endif
#ifdef English_dox /**
	 * Overriden function of sf::Drawable
	 * draws elements onto window
	 *
	 * @param sf::RenderTarget& Target window
	 * @param sf::RenderStates Additional data
	 */
	#endif
	virtual void draw(sf::RenderTarget&, sf::RenderStates) const = 0;

	#ifdef Polish_dox /**
	 * Funkcja sluzaca aktualizacji stanu gry.
	 *
	 * @param sf::Time Czas od ostatniej aktualizacji stanu gry.
	 */
	#endif
	#ifdef English_dox /**
	 * updates state of the game
	 *
	 * @param sf::Time Time from last update
	 */
	#endif
	virtual void update(sf::Time dt) = 0;

	#ifdef Polish_dox /**
	 * Przeciążanie operatora bool. Zwraca informacje o tym, czy warstwa jest aktywna.
	 *
	 * @param bool Informacja o aktywnosci warstwy.
	 */
	#endif
#ifdef English_dox /**
	 * Bool operator overriden.
	 *
	 * @param bool true if layer is active
	 */
	#endif
	virtual operator bool() const { return _isActive; }

	#ifdef Polish_dox /**
	 * Przeciążanie operatora !. Zwraca informacje o tym, czy warstwa jest aktywna.
	 *
	 * @param bool Informacja o aktywnosci warstwy (zaprzeczenie aktywnosci).
	 */
	#endif
	#ifdef English_dox /**
	 * Overriden ! operator.
	 *
	 * @param bool false if this layer is active
	 */
	#endif
	virtual bool operator!() const { return !_isActive; }


	void SetIsActive(bool t){ _isActive=t;}

	#ifdef Polish_dox /**
	 * Metoda zamykająca aktualne okno z SFGUI.
	 */
	#endif
#ifdef English_dox /**
	 * Closes gui
	 */
	#endif
	virtual void closeCurrent();
	
	 #ifdef Polish_dox /**
	 * Ustawienie musicPlayera
	 */
	#endif
#ifdef English_dox /**
	 * settings of music player
	 */
	#endif
	void setMusicPlayer(std::shared_ptr<MusicPlayer> musicPlayer){ _musicPlayer=musicPlayer;}

protected:
	#ifdef Polish_dox /**
	 * Zmienna sprawdzajaca, czy warstwa powinna byc rysowana.
	 */
	#endif
	#ifdef English_dox /**
	 * true if layer is active (should be painted, or handle events)
	 */
	#endif
	bool _isActive = false;

	#ifdef Polish_dox /**
	 * Wskaznik do aktualnej rozgrywki.
	 */
	#endif
#ifdef English_dox /**
	 * Pointer to current game
	 */
	#endif
	std::shared_ptr<Game> _game = nullptr;
	
	#ifdef Polish_dox #ifdef Polish_dox /**
	 * MusicPlayer
	 */
	#endif
#ifdef English_dox #ifdef Polish_dox /**
	 * MusicPlayer
	 */
	#endif
	std::shared_ptr<MusicPlayer> _musicPlayer;
};

#endif
