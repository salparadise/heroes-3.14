#ifndef ORE_PIT_HPP_
#define ORE_PIT_HPP_

#include <SFML/Graphics.hpp>
#include "Building.hpp"

class OrePit : public Building {
public:
	OrePit();
	virtual ~OrePit();
	virtual void updateResources();
		
}; 

#endif /* ORE_PIT_HPP_ */

