#ifndef SIDEBARLAYER_HPP_
#define SIDEBARLAYER_HPP_

#include <SFGUI/SFGUI.hpp>
#include "Layer.hpp"
#include "GroundLayer.hpp"

class SidebarLayer : public Layer {
public:
	SidebarLayer(std::shared_ptr<Game>);
	virtual ~SidebarLayer() { }
	virtual bool processEvent(sf::Event&);
	virtual void draw(sf::RenderTarget &, sf::RenderStates) const;
	virtual void update(sf::Time dt);

protected:

private:
	void viewMove() const;

	/**
	 * Funkcja odpala sie przy wybraniu jednej z dostepnych armii z ComboBoxa armii na sidebarze.
	 */
	void selectAndCenterArmy() const;
	void selectCastle() const;

	/**
	 * Funkcja uaktualniajaca etykiety na topBar'rze
	 */
	void updateResources();

	//"C++11 Non-static member initializers are implemented from gcc 4.7"
	//https://blogs.oracle.com/pcarlini/entry/c_11_tidbits_non_static
	const unsigned _sidebarWidth = 200, _topbarHeight = 40;

	sf::FloatRect _guiPositionAndSize;
	sf::FloatRect _topbarPositionAndSize;
	sf::View minimap;
	sf::Image minimapImage;

	//------------------------------------------------------------ kontrolki SFGUI
	std::shared_ptr<sfg::SFGUI> _sfgui;

	std::shared_ptr<sfg::Window> _sidebar;
	std::shared_ptr<sfg::Window> _topbar;

	std::shared_ptr<sfg::Canvas> sfml_canvas;

	std::shared_ptr<sfg::Table> table;
	std::shared_ptr<sfg::Table> _topbarTable;

	std::shared_ptr<sfg::Button> button2;
	std::shared_ptr<sfg::Button> button3;

	std::shared_ptr<sfg::Label> _topbarLabel1, _topbarLabel2, _topbarLabel3, _topbarLabel4;
	std::shared_ptr<sfg::Image> _topbarImage1, _topbarImage2, _topbarImage3, _topbarImage4;

	std::shared_ptr<sfg::ComboBox> _sidebarComboBoxCastle, _sidebarComboBoxArmy;
};

#endif /* SIDEBARLAYER_HPP_ */
