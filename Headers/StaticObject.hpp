#ifndef STATICOBJECT_HPP_
#define STATICOBJECT_HPP_

#include <SFML/Graphics.hpp>
#include "Object.hpp"
#ifdef English_dox
	/**
	 *encapsulates inactive objects (no owner, no actions)
	 */
#endif
class StaticObject : public Object {
protected:
	int _cost;

public:
	StaticObject();
	virtual ~StaticObject();
#ifdef English_dox
	/**
	 *
	 */
#endif
	int getCost() const { return _cost; }
#ifdef English_dox
	/**
	 *
	 */
#endif
	virtual bool isActiveObject() const { return false; }
};


#endif /* STATICOBJECT_HPP_ */
